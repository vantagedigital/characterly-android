package com.mucaroo.adapters;

/**
 * Created by ahsan on 8/10/2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.ViewModel.LessonVM;
import com.mucaroo.activities.lessons.LessonDetail;
import com.mucaroo.application.App;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.models.Pillar;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {

    private List<LessonVM> mData = Collections.emptyList();
    private LayoutInflater mInflater;

    private Context context;

    public LessonAdapter(Context context, List<LessonVM> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_lesson, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LessonVM lessonVM = mData.get(position);
        holder.mNameText.setText(lessonVM.getName());
        holder.mSubjectText.setText("Subject: " + lessonVM.getSubject());
        holder.mGradeText.setText("Grade: " + lessonVM.getGrade());

        String imageUrl = lessonVM.getImageUrl();
//        if (position == 3) {
//            imageUrl = "https://characterlystorage.blob.core.windows.net/contentthumbnail/a84b7596-7d72-4805-9340-08d52e46e116/image/20171121_160016.jpg";
//            imageUrl = "http://s3.amazonaws.com/digitaltrends-uploads-prod/2016/09/group-of-school-kids-with-teacher-sitting-in-classroom-and-raising-hands.jpg";
//        }
        ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.mImageView, imageUrl);


        holder.mPillar1.setVisibility(View.INVISIBLE);
        holder.mPillar2.setVisibility(View.INVISIBLE);
        holder.mPillar3.setVisibility(View.INVISIBLE);
        holder.mPillar4.setVisibility(View.INVISIBLE);
        holder.mPillar5.setVisibility(View.INVISIBLE);
        holder.mPillar6.setVisibility(View.INVISIBLE);
        holder.mPillar7.setVisibility(View.INVISIBLE);
        holder.mPillar8.setVisibility(View.INVISIBLE);
        holder.mPillar9.setVisibility(View.INVISIBLE);

        for (int i = 0; i < lessonVM.getPillars().size(); i++) {
            Pillar pillar = lessonVM.getPillars().get(i);
            CircleImageView pillarView = getPillarForRow(holder, i);
            pillarView.setVisibility(View.VISIBLE);
            try {
                pillarView.setImageDrawable(new ColorDrawable(Color.parseColor(pillar.colorCode)));
                if (pillar.colorCode.contains("ffffff")) {
                    pillarView.setBorderColor(Color.BLACK);
                    pillarView.setBorderWidth(context.getResources().getDimensionPixelOffset(R.dimen.dp_05));
                } else {
                    pillarView.setBorderColor(Color.WHITE);
                    pillarView.setBorderWidth(context.getResources().getDimensionPixelOffset(R.dimen.dp_0));
                }
            } catch (Exception e) {

            }
        }
    }

    private CircleImageView getPillarForRow(ViewHolder holder, int i) {
        if (i == 0) {
            return holder.mPillar1;
        } else if (i == 1) {
            return holder.mPillar2;
        } else if (i == 2) {
            return holder.mPillar3;
        } else if (i == 3) {
            return holder.mPillar4;
        } else if (i == 4) {
            return holder.mPillar5;
        } else if (i == 5) {
            return holder.mPillar6;
        } else if (i == 6) {
            return holder.mPillar7;
        } else if (i == 7) {
            return holder.mPillar8;
        } else {
            return holder.mPillar9;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mNameText;
        TextView mSubjectText;
        TextView mGradeText;
        CircleImageView mPillar1, mPillar2, mPillar3, mPillar4, mPillar5, mPillar6, mPillar7, mPillar8, mPillar9;
        ImageView mImageView;

        ViewHolder(View itemView) {
            super(itemView);
            mNameText = itemView.findViewById(R.id.tv_name);
            mSubjectText = itemView.findViewById(R.id.subject_text);
            mGradeText = itemView.findViewById(R.id.grade_text);
            mPillar1 = itemView.findViewById(R.id.pillar_1);
            mPillar2 = itemView.findViewById(R.id.pillar_2);
            mPillar3 = itemView.findViewById(R.id.pillar_3);
            mPillar4 = itemView.findViewById(R.id.pillar_4);
            mPillar5 = itemView.findViewById(R.id.pillar_5);
            mPillar6 = itemView.findViewById(R.id.pillar_6);
            mPillar7 = itemView.findViewById(R.id.pillar_7);
            mPillar8 = itemView.findViewById(R.id.pillar_8);
            mPillar9 = itemView.findViewById(R.id.pillar_9);
            mImageView = itemView.findViewById(R.id.image_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            context.startActivity(LessonDetail.newIntent(context, getItem(position).getData()));
        }

    }

    public LessonVM getItem(int id) {
        return mData.get(id);
    }

}