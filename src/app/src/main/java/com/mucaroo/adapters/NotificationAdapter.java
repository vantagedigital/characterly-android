package com.mucaroo.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.classroom.ClassTimeline;
import com.mucaroo.activities.classroom.Classrooms;
import com.mucaroo.activities.evaluation.EvaluationMain;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.BadgeAllDialog;
import com.mucaroo.helper.DateHelper;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.helper.SystemNotificationType;
import com.mucaroo.models.Notification;
import com.mucaroo.models.NotificationModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ahsan on 8/14/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private List<Notification> mData = new ArrayList<>();
    private Context context;

    public NotificationAdapter(Context context, List<Notification> data) {
        this.context = context;
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Notification data = mData.get(position);
        holder.mTitle.setText(data.message);
        Calendar date = DateHelper.getCalendarDateFromFullFormattedString(DateHelper.getLocalDateFromUTCDate(data.creationTime));
        holder.mTime.setText(DateHelper.getTimeElapsedString(date));

        holder.mBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        holder.mDivider.setBackgroundColor(context.getResources().getColor(R.color.white_alt));
        if (!data.isRead) {
            holder.mBackground.setBackgroundColor(context.getResources().getColor(R.color.pale_lilac));
            holder.mDivider.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTitle, mTime;
        View mBackground, mDivider;

        ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.tv_title);
            mTime = itemView.findViewById(R.id.tv_time);
            mBackground = itemView.findViewById(R.id.background);
            mDivider = itemView.findViewById(R.id.divider);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Notification data = mData.get(position);
            data.isRead = true;
            //save locally
            NotificationModel model = new NotificationModel();
            model.result = mData;
            UserCache.getInstance().setNotifications(model);

            markNotificationAsRead(data.id);

            notifyDataSetChanged();

            if (data.type == SystemNotificationType.BadgeAssignation.getValue()) {
                BadgeAllDialog dialog = new BadgeAllDialog(context);
                dialog.show();
            } else if (data.type == SystemNotificationType.ClassroomAssignation.getValue()) {
                // go to classrooms
                Intent myIntent = new Intent(context, Classrooms.class);
                context.startActivity(myIntent);
            } else if (data.type == SystemNotificationType.ContentSubmission.getValue()) {
                // go to content
                context.startActivity(LessonMain.newIntent(context, UserCache.getInstance().getContentInfo()));
            } else if (data.type == SystemNotificationType.ContentApproval.getValue()) {
                //  go to content
                context.startActivity(LessonMain.newIntent(context, UserCache.getInstance().getContentInfo()));
            } else if (data.type == SystemNotificationType.ContentScheduleAssignation.getValue()) {
                // go to content
                context.startActivity(LessonMain.newIntent(context, UserCache.getInstance().getContentInfo()));
            } else if (data.type == SystemNotificationType.EvaluationAssignation.getValue()) {
                // : go to evaluation by type id?
                context.startActivity(EvaluationMain.newIntent(context));
            } else if (data.type == SystemNotificationType.UserEvaluationSubmission.getValue()) {
                // : go to evaluations
                context.startActivity(EvaluationMain.newIntent(context));
            } else if (data.type == SystemNotificationType.UserEvaluationReview.getValue()) {
                // : go to evaluations
                context.startActivity(EvaluationMain.newIntent(context));
            } else if (data.type == SystemNotificationType.InvitationUserToCMS.getValue()) {
                // TODO:Not sure?
            } else if (data.type == SystemNotificationType.InvitationUserToContentManagement.getValue()) {
                // TODO:Not sure?
            } else if (data.type == SystemNotificationType.InvitationUserToBranch.getValue()) {
                // TODO:Not sure?
            } else if (data.type == SystemNotificationType.InvitationUserToClassroom.getValue()) {
                // TODO:Go to classroom via type id?
                Intent myIntent = new Intent(context, Classrooms.class);
                context.startActivity(myIntent);
            } else if (data.type == SystemNotificationType.InvitationParentToStudent.getValue()) {
                // TODO:Go to classroom via type id?
            } else if (data.type == SystemNotificationType.TimelineComment.getValue()) {
                // TODO:Go to TimelineComment
                context.startActivity(ClassTimeline.newIntent(context, data.typeId));
            } else if (data.type == SystemNotificationType.UserAccount.getValue()) {
                // TODO:Not sure?
            }
        }
    }

    private void markNotificationAsRead(String id) {
        NetworkHelper networkHelper = new NetworkHelper(context);
        Map<String, Object> map = new HashMap<>();
        networkHelper.postRequest(NetworkHelper.NotificationMarkRead + id, map, new Object(), new NetworkHelper.Listener() {
            @Override
            public void onResponse(Object model) {
            }

            @Override
            public void onFailure(String msg) {
            }
        });
    }

    public Notification getItem(int id) {
        return mData.get(id);
    }

}