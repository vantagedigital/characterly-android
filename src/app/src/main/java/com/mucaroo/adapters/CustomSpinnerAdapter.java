package com.mucaroo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.helper.Common;

import java.util.ArrayList;

/**
 * Created by ahsan on 8/17/2017.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    private ArrayList<String> objects;
    private int selectedIndex = -1;
    private Context context;

    public CustomSpinnerAdapter(Context context, int resourceId,
                                ArrayList<String> objects) {
        super(context, resourceId, objects);
        this.objects = objects;
        this.context = context;

    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    private View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = LayoutInflater.from(context).inflate(R.layout.item_spinner, parent, false);
        final TextView label = row.findViewById(R.id.spinner_textView);
        label.setText(objects.get(position));

        if (position == selectedIndex) {
            label.setBackgroundColor(Common.getColorWrapper(context, R.color.off_blue));
            label.setTextColor(Common.getColorWrapper(context, R.color.white));
        }

        return row;
    }


    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;

    }

}
