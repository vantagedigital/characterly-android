package com.mucaroo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.models.TimelineComment;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class TimelineCommentAdapter extends RecyclerView.Adapter<TimelineCommentAdapter.ViewHolder> {

    private List<TimelineComment> mData = Collections.emptyList();
    private LayoutInflater mInflater;

    public TimelineCommentAdapter(Context context, List<TimelineComment> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TimelineComment timelineComment = mData.get(position);
        holder.tv_username.setText(timelineComment.user.fullName);
        holder.date_text.setText("");
        holder.comment_text.setText(timelineComment.commentText);
        try {
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.profile_imageview, timelineComment.user.profilePicture.path);
        } catch (Exception e) {
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_username)
        TextView tv_username;
        @BindView(R.id.date_text)
        TextView date_text;
        @BindView(R.id.comment_text)
        TextView comment_text;
        @BindView(R.id.profile_imageview)
        CircleImageView profile_imageview;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}