package com.mucaroo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomModel;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.UserInfoModel;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mucaroo.helper.NetworkHelper.FileAttachmentGetByEntityId;

/**
 * Created by ahsan on 8/27/2017.
 */

public class ClassroomsAdapter extends RecyclerView.Adapter<ClassroomsAdapter.ViewHolder> {

    private final NetworkHelper networkHelper;
    private List<Classroom> mData;
    private LayoutInflater mInflater;
    private Context context;
    private ListItemSelectedListener mItemSelectedListener;

    public ClassroomsAdapter(Context context, List<Classroom> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        networkHelper = new NetworkHelper(this.context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_classrooms, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Classroom classroom = mData.get(position);
        holder.mClassName.setText(classroom.name);
        holder.mClassCode.setText(classroom.classCode);
        if (classroom.grade != null) {
            holder.mGradeText.setText(classroom.grade.name);
        } else {
            holder.mGradeText.setText("");
        }
        holder.mStudentsText.setText("");

        updateClassroomImage(position, holder);
    }

    private void updateClassroomImage(final int position, final ViewHolder holder) {
        Classroom classroom = mData.get(position);
            try {
                if (TextUtils.isEmpty(classroom.imagePath)) {
                    networkHelper.getRequest(FileAttachmentGetByEntityId + classroom.id, new FileAttachmentModel(), new NetworkHelper.Listener<FileAttachmentModel>() {
                        @Override
                        public void onResponse(FileAttachmentModel model) {
                            try {
                                mData.get(position).imagePath = model.result.get(model.result.size() - 1).path;

                                ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.mImageView, mData.get(position).imagePath);
                            } catch (Exception e) {
                            }
                        }

                        @Override
                        public void onFailure(String msg) {
                        }
                    });
                } else {
                    ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.mImageView, mData.get(position).imagePath);
                }
            } catch (Exception e) {
            }
    }

    public void setItemSelectedListener(ListItemSelectedListener mItemSelectedListener) {
        this.mItemSelectedListener = mItemSelectedListener;
    }

    @Override
    public int getItemCount() {
        return  mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.class_code)
        TextView mClassCode;

        @BindView(R.id.class_name)
        TextView mClassName;

        @BindView(R.id.grade_text)
        TextView mGradeText;

        @BindView(R.id.students_text)
        TextView mStudentsText;

        @BindView(R.id.image_view)
        ImageView mImageView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (mItemSelectedListener != null) {
                mItemSelectedListener.onListItemSelected(position, view);
            }
        }

    }

}