package com.mucaroo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.UserEvaluation;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ahsan on 8/14/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private List<? extends Object> mData;
    private LayoutInflater mInflater;
    private ListItemSelectedListener listener;
    // TODO: implement unread/read

    private Context context;

    public SearchAdapter(Context context, List<? extends Object> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Evaluation evaluation = null;
        if (mData.get(position) instanceof Evaluation) {
            evaluation = (Evaluation) mData.get(position);
        } else if (mData.get(position) instanceof UserEvaluation) {
            evaluation =  ((UserEvaluation) mData.get(position)).evaluation;
        }
        holder.mTitleText.setText(evaluation.name);
        holder.mStudentName.setText("");
//        holder.mDateText.setText("");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTitleText, mDateText, mStudentName;
        CircleImageView mStudentImage;

        ViewHolder(View itemView) {
            super(itemView);
            mTitleText = itemView.findViewById(R.id.title_text);
            mDateText = itemView.findViewById(R.id.date_text);
            mStudentName = itemView.findViewById(R.id.student_name);
            mStudentImage = itemView.findViewById(R.id.student_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (listener != null) {
                listener.onListItemSelected(position, view);
            }
        }

    }

    public void setListener(ListItemSelectedListener listener) {
        this.listener = listener;
    }

    public Object getItem(int id) {
        return mData.get(id);
    }

}