package com.mucaroo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.dialogs.TimelineCommentDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.DateHelper;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.models.Timeline;
import com.mucaroo.models.TimelineComment;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ahsan on 8/23/2017.
 */

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {

    private List<TimelineComment> mData = Collections.emptyList();
    private LayoutInflater mInflater;

    private Context context;
    private String mClassroomId;

    public TimelineAdapter(Context context, List<TimelineComment> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_timeline, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.lay_content_audio.setVisibility(View.GONE);
        holder.lay_content_pic.setVisibility(View.GONE);
        holder.iv_image.setVisibility(View.GONE);
        holder.btn_play.setVisibility(View.GONE);
        holder.audio_btn.setVisibility(View.GONE);


        final TimelineComment timelineComment = mData.get(position);

        holder.btn_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimelineCommentDialog dialog = new TimelineCommentDialog(context, timelineComment, mClassroomId);
                dialog.show();
            }
        });
        try {
            holder.tv_username.setText(timelineComment.user.fullName);
        } catch (Exception e) {
        }
        try {
            String formatSuffix = "";
            String prefix = "";
            if (isToday(timelineComment.creationTime)) {
                prefix = "Today -";
            } else if (isYesterday(timelineComment.creationTime)) {
                prefix = "Yesterday -";
            } else if (!TextUtils.isEmpty(timelineComment.creationTime)) {
                formatSuffix = "MMM dd, yyyy";
            }
            Calendar localDate = DateHelper.getCalendarDateFromFullFormattedString(DateHelper.getUTCFromLocalDate(DateHelper.getDateFromFormattedDate(timelineComment.creationTime).getTime()));
            String suffixText = DateHelper.getFormattedDate(localDate.getTime(), formatSuffix + " @ hh:mm a");
            holder.tv_date.setText(prefix + suffixText);
        } catch (Exception e) {
        }

        try {
            Calendar timeToCheck = DateHelper.getDateFromFormattedDate(timelineComment.creationTime);
            holder.date_text.setText(DateHelper.getTimeElapsedString(timeToCheck));
        } catch (Exception e) {

        }

        try {
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.profile_imageview, timelineComment.user.profilePicture.path);
        } catch (Exception e) {
        }
        holder.content_text.setText(timelineComment.commentText);

        holder.lay_date.setVisibility(View.VISIBLE);
        holder.lay_content_text.setVisibility(View.VISIBLE);

        if (timelineComment.attachments != null && timelineComment.attachments.size() > 0) {
            holder.lay_content_text.setVisibility(View.GONE);
            holder.lay_content_pic.setVisibility(View.VISIBLE);
            holder.iv_image.setVisibility(View.VISIBLE);
            final String path = timelineComment.attachments.get(timelineComment.attachments.size() - 1).path;
            if (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png")) {
                ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.iv_image, timelineComment.attachments.get(timelineComment.attachments.size() - 1).path);
            } else if (path.contains(".mp3") || path.contains(".ogg") || path.contains(".mkv")) {
                holder.audio_btn.setVisibility(View.VISIBLE);
                holder.iv_image.setBackgroundColor(context.getResources().getColor(R.color.white_four));
                holder.audio_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // launch audio with another application

                        if (!TextUtils.isEmpty(path)) {
                            Uri uri = Uri.parse(path);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            try {
                                context.startActivity(intent);
                            } catch (Exception e) {
                                Common.showToast("Error trying to play audio link");
                            }
                        } else {
                            Common.showToast("No Audio link available");
                        }
                    }
                });
            } else {
                holder.btn_play.setVisibility(View.VISIBLE);
                holder.iv_image.setBackgroundColor(Color.BLACK);
                holder.btn_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // launch video with another application

                        if (!TextUtils.isEmpty(path)) {
                            Uri uri = Uri.parse(path);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            try {
                                context.startActivity(intent);
                            } catch (Exception e) {
                                Common.showToast("Error trying to play video link");
                            }
                        } else {
                            Common.showToast("No Video link available");
                        }
                    }
                });
            }
        }

        // TODO: decide when to show audio or image in attachment
//        if (position == 0) { // Show textual post
//
//        } else if (position % 2 == 0) { // Show Picture/Video post
//            holder.lay_content_pic.setVisibility(View.VISIBLE);
//
//        } else { // Show Audio post
//            holder.lay_content_audio.setVisibility(View.VISIBLE);
//        }


    }

    private boolean isToday(String creationTime) {
        String todayUTC = DateHelper.getFormattedDate(Calendar.getInstance().getTime());
        if (creationTime != null) {
            creationTime = creationTime.split("T")[0];
        }
        if (todayUTC != null) {
            todayUTC = todayUTC.split("T")[0];
        }
        return todayUTC.equalsIgnoreCase(creationTime);
    }

    private boolean isYesterday(String creationTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        String todayUTC = DateHelper.getFormattedDate(calendar.getTime());
        if (creationTime != null) {
            creationTime = creationTime.split("T")[0];
        }
        if (todayUTC != null) {
            todayUTC = todayUTC.split("T")[0];
        }
        return todayUTC.equalsIgnoreCase(creationTime);
    }

    public TimelineComment getItem(int id) {
        return mData.get(id);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setClassroomId(String id) {
        mClassroomId = id;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tv_username;

        @BindView(R.id.date_text)
        TextView date_text;

        @BindView(R.id.content_text)
        TextView content_text;

        @BindView(R.id.tv_date)
        TextView tv_date;

        @BindView(R.id.iv_image)
        ImageView iv_image;

        @BindView(R.id.btn_play)
        View btn_play;

        @BindView(R.id.audio_btn)
        View audio_btn;

        @BindView(R.id.lay_content_text)
        LinearLayout lay_content_text;

        @BindView(R.id.lay_content_audio)
        LinearLayout lay_content_audio;

        @BindView(R.id.lay_content_pic)
        FrameLayout lay_content_pic;

        @BindView(R.id.lay_date)
        LinearLayout lay_date;

        @BindView(R.id.profile_imageview)
        CircleImageView profile_imageview;

        @BindView(R.id.btn_comments)
        View btn_comments;

        @OnClick(R.id.btn_arrow)
        void btn_arrow(ImageButton btn) {
            Common.rotateView(btn);
        }

        @OnClick(R.id.btn_like)
        void btn_like() {

        }

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}