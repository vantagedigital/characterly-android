package com.mucaroo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.Student;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.mucaroo.helper.NetworkHelper.FileAttachmentGetByEntityId;

/**
 * Created by ahsan on 8/22/2017.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {

    private final NetworkHelper networkHelper;
    private List<Student> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private List<String> selectedUserIds = new ArrayList<>();
    private ListItemSelectedListener listener;


    public StudentAdapter(Context context, List<Student> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        networkHelper = new NetworkHelper(this.context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_student, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_name.setText(mData.get(position).name);
        holder.tv_lastname.setText(mData.get(position).surname);

        String user_id = mData.get(position).id;
        if (selectedUserIds.contains(user_id)) {
            holder.ic_checkmark.setVisibility(View.VISIBLE);
        } else {
            holder.ic_checkmark.setVisibility(View.INVISIBLE);
        }

        updateStudentImage(position, holder);
    }

    private void updateStudentImage(final int position, final ViewHolder holder) {
        Student student = mData.get(position);
        try {
            String profilePicPath = student.profilePicture == null ? null : student.profilePicture.path;
            if (TextUtils.isEmpty(student.imagePath) && TextUtils.isEmpty(profilePicPath)) {
                networkHelper.getRequest(FileAttachmentGetByEntityId + student.id, new FileAttachmentModel(), new NetworkHelper.Listener<FileAttachmentModel>() {
                    @Override
                    public void onResponse(FileAttachmentModel model) {
                        Common.logOutput("response:" + model);
                        try {
                            mData.get(position).imagePath = model.result.get(model.result.size() - 1).path;

                            updateProfilePic(holder, position);
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onFailure(String msg) {
                        Common.logOutput("response:" + msg);
                    }
                });
            } else {
                updateProfilePic(holder, position);
            }
        } catch (Exception e) {
        }
    }

    private void updateProfilePic(ViewHolder holder, int position) {
        String profilePicPath = mData.get(position).profilePicture == null ? null : mData.get(position).profilePicture.path;
        String imagePath = TextUtils.isEmpty(profilePicPath) ? mData.get(position).imagePath : profilePicPath;
        ImageLoaderUtil.loadImageIntoView(App.getInstance(), holder.img_user, imagePath);
    }

    public JSONArray getSelectedIds() {
        return new JSONArray(selectedUserIds);
    }

    public void addToSelectedIds(List<String> selectedUserIds) {
        this.selectedUserIds.addAll(selectedUserIds);
    }

    public void setListener(ListItemSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_name)
        TextView tv_name;

        @BindView(R.id.tv_lastname)
        TextView tv_lastname;

        @BindView(R.id.ic_checkmark)
        ImageView ic_checkmark;

        @BindView(R.id.img_user)
        ImageView img_user;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            String user_id = mData.get(position).id;
            if (listener == null) {
                if (selectedUserIds.contains(user_id)) {
                    selectedUserIds.remove(selectedUserIds.indexOf(user_id));
                    ic_checkmark.setVisibility(View.INVISIBLE);
                } else {
                    selectedUserIds.add(user_id);
                    ic_checkmark.setVisibility(View.VISIBLE);
                }
            } else {
                listener.onListItemSelected(position, view);
            }

        }
    }

}