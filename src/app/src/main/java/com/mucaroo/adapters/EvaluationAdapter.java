package com.mucaroo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.interfaces.CompletionListener;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Answer;
import com.mucaroo.models.Question;
import com.mucaroo.models.QuestionOption;
import com.mucaroo.models.SelectedQuestionOption;
import com.mucaroo.models.UserEvaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahsan on 9/6/2017.
 */

public class EvaluationAdapter extends RecyclerView.Adapter<EvaluationAdapter.ViewHolder> {

    private List<Answer> mAnswers = Collections.emptyList();
    private List<Question> mQuestions = Collections.emptyList();
    private LayoutInflater mInflater;
    private boolean isEvaluated;
    private HashMap<Integer, Boolean> mScoredAnswers = new HashMap<>();
    private CompletionListener mDataChangedListener;

    Context context;

    public EvaluationAdapter(Context context, List<Answer> answers, List<Question> questions, Boolean isEvaluated) {
        this.mInflater = LayoutInflater.from(context);
        this.mAnswers = answers;
        this.mQuestions = questions;
        this.isEvaluated = isEvaluated;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_evaluation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (isEvaluated) {
            holder.lay_action.setVisibility(View.VISIBLE);
        }
        updateUI(position, holder);

        holder.mCorrectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mScoredAnswers.put(position, true);
                updateUI(position, holder);
                dataChanged();
            }
        });

        holder.mIncorrectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mScoredAnswers.put(position, false);
                updateUI(position, holder);
                dataChanged();
            }
        });
    }

    private void dataChanged() {
        if (mDataChangedListener != null) {
            mDataChangedListener.onCompleted();
        }
    }

    public void setDataChangedListener(CompletionListener dataChangedListener) {
        this.mDataChangedListener = dataChangedListener;
    }

    private void updateUI(int position, ViewHolder holder) {
        Question question = mQuestions.get(position);

        holder.cb_number.setText((position + 1) + ".");
        holder.cb_question.setText(question.description);

        configureForType(question, position, holder.lay_questions);

        updateBottomUI(position, holder);
    }

    private void updateBottomUI(int position, ViewHolder holder) {
        holder.mCorrectButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.box_white));
        holder.mIncorrectButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.box_white));
        if (mScoredAnswers.containsKey(position) && mScoredAnswers.get(position) == false) {
            holder.mIncorrectButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_light_peach));
        } else if (mScoredAnswers.containsKey(position) && mScoredAnswers.get(position) == true) {
            holder.mCorrectButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_very_light_green));
        }
    }

    private void configureForType(Question question, int position, LinearLayout mLayQuestions) {
        if (mLayQuestions.getChildCount() > 0) {
            return;
        }
        int type = question.questionType;
        switch (type) {
            case 0: {//OpenQuestion
                final View view = View.inflate(context, R.layout.item_evalutation_edittext, null);
                final EditText open_edittext = view.findViewById(R.id.open_edittext);
                String answer = mAnswers.size() > 0 ? mAnswers.get(position).answerText : "";
                open_edittext.setText(answer);
                mLayQuestions.addView(view);

                break;
            }
            case 1: {//TrueFalse
                final View view = View.inflate(context, R.layout.item_evalutation_edittext, null);
                final EditText open_edittext = view.findViewById(R.id.open_edittext);
                String answer = mAnswers.size() > 0 ? mAnswers.get(position).answerText : "";
                open_edittext.setText(answer);
                mLayQuestions.addView(view);
                //TODO:? need design
                break;
            }
            case 2: {//SingleChoice
                List<QuestionOption> data = new ArrayList<>();
                for (QuestionOption option : question.questionOptions) {
                    data.add(option);
                }
                final LinearLayout subView = new LinearLayout(context);
                subView.setOrientation(LinearLayout.VERTICAL);

                List<Integer> selectedAnswers = new ArrayList<>();
                if (mAnswers.size() > 0) {
                    ArrayList<String> selectedQIds = new ArrayList<>();
                    for (SelectedQuestionOption selectedQOption : mAnswers.get(position).selectedOptions) {
                        selectedQIds.add(selectedQOption.evaluationQuestionOptionId);
                    }
                    for (int i = 0; i < question.questionOptions.size(); i++) {
                        if (selectedQIds.contains(question.questionOptions.get(i).id)) {
                            selectedAnswers.add(i);
                        }
                    }
                }
                int selectedAnswer = -1;
                if (selectedAnswers.size() > 0) {
                    selectedAnswer = selectedAnswers.get(0);
                }
                if (selectedAnswer >= 0) {
                    View selectedView = subView.getChildAt(selectedAnswer);
                    selectPosition(selectedView, subView);
                }
                Common.addViewFromLayout(context, R.layout.item_evalutation_radio, subView, data, null);
                mLayQuestions.addView(subView);
                break;
            }
            case 3: {//MultipleChoice
                List<QuestionOption> data1 = new ArrayList<>();
                for (QuestionOption option : question.questionOptions) {
                    data1.add(option);
                }
                List<Integer> selectedAnswers = new ArrayList<>();
                if (mAnswers.size() > 0) {
                    ArrayList<String> selectedQIds = new ArrayList<>();
                    for (SelectedQuestionOption selectedQOption : mAnswers.get(position).selectedOptions) {
                        selectedQIds.add(selectedQOption.evaluationQuestionOptionId);
                    }
                    for (int i = 0; i < question.questionOptions.size(); i++) {
                        if (selectedQIds.contains(question.questionOptions.get(i).id)) {
                            selectedAnswers.add(i);
                        }
                    }
                }
                Common.addViewFromLayout(context, R.layout.item_evalutation_checkbox, mLayQuestions, data1, null, selectedAnswers);
                break;
            }
            case 4://SingleImageChoice
            {
                final View view2 = View.inflate(context, R.layout.item_evalutation_image_edittext, null);
                final TextView open_img_edittext = view2.findViewById(R.id.open_img_edittext);
                final ImageView eva_imageview = view2.findViewById(R.id.eva_imageview);
                String answer = mAnswers.size() > 0 ? mAnswers.get(position).answerText : "";
                open_img_edittext.setText(answer);
                ImageLoaderUtil.loadImageIntoView(App.getInstance(), eva_imageview, question.fileAttachment.path);
                mLayQuestions.addView(view2);
                break;
            }
            case 5: {//MultipleImageChoice
                List<QuestionOption> data2 = new ArrayList<>();
                for (QuestionOption option : question.questionOptions) {
                    data2.add(option);
                }
                List<Integer> selectedAnswers = new ArrayList<>();
                if (mAnswers.size() > 0) {
                    ArrayList<String> selectedQIds = new ArrayList<>();
                    for (SelectedQuestionOption selectedQOption : mAnswers.get(position).selectedOptions) {
                        selectedQIds.add(selectedQOption.evaluationQuestionOptionId);
                    }
                    for (int i = 0; i < question.questionOptions.size(); i++) {
                        if (selectedQIds.contains(question.questionOptions.get(i).id)) {
                            selectedAnswers.add(i);
                        }
                    }
                }
                Common.addViewFromLayout(context, R.layout.item_evalutation_img_checkbox, mLayQuestions, data2, null, selectedAnswers);
                break;
            }
            default:
                break;
        }
    }

    private void selectPosition(View view, LinearLayout subView) {
        for (int i = 0; i < subView.getChildCount(); i++) {
            View child = subView.getChildAt(i);
            View container = child.findViewById(R.id.container);
            Button cbButton = child.findViewById(R.id.btn_cb);
            TextView cbText = child.findViewById(R.id.cb_text);
            container.setBackgroundColor(context.getResources().getColor(R.color.white));
            cbButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.box_white));
            cbText.setTextColor(context.getResources().getColor(R.color.charcoal_grey));
        }

        View container = view.findViewById(R.id.container);
        container.setBackgroundColor(context.getResources().getColor(R.color.off_blue));
        Button cbButton = view.findViewById(R.id.btn_cb);
        TextView cbText = view.findViewById(R.id.cb_text);
        cbButton.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.box_blue));
        cbText.setTextColor(context.getResources().getColor(R.color.white));
    }

    public HashMap<Integer, Boolean> getScoredAnswers() {
        return mScoredAnswers;
    }

    @Override
    public int getItemCount() {
        return mQuestions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cb_number)
        TextView cb_number;

        @BindView(R.id.cb_question)
        TextView cb_question;

        @BindView(R.id.lay_questions)
        LinearLayout lay_questions;

        @BindView(R.id.lay_action)
        LinearLayout lay_action;

        @BindView(R.id.correct_button)
        ImageButton mCorrectButton;

        @BindView(R.id.incorrect_button)
        ImageButton mIncorrectButton;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}