package com.mucaroo.ViewModel;

import com.mucaroo.models.FileAttachment;
import com.mucaroo.models.MyContentResult;
import com.mucaroo.models.Pillar;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.List;

public class LessonVM {

    private String mName;
    private String mSubject;
    private String mGrade;
    private List<Pillar> mPillars = new ArrayList<Pillar>();
    private String mImageUrl;

    private MyContentResult mData;

    public LessonVM(MyContentResult data) {
        this.mData = data;
        try {
            this.mName = data.contentSchedule.content.title;
        } catch (Exception e) {
        }
        try {
            List<FileAttachment> attachments = mData.contentSchedule.content.fileAttachments == null ? mData.contentSchedule.content.attachments : mData.contentSchedule.content.fileAttachments;
            this.mImageUrl = attachments.get(attachments.size() - 1).path;
        } catch (Exception e) {
        }
        try {
            this.mSubject = data.contentSchedule.content.subjects.get(0).name;
        } catch (Exception e) {
        }
        try {
            this.mGrade = data.contentSchedule.content.grades.get(0).name;
        } catch (Exception e) {
        }

        try {
            this.mPillars = data.contentSchedule.content.pillars;
        } catch (Exception e) {
        }
    }

    public MyContentResult getData() {
        return mData;
    }

    public List<Pillar> getPillars() {
        return mPillars;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String mSubject) {
        this.mSubject = mSubject;
    }

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String mGrade) {
        this.mGrade = mGrade;
    }

    public String getImageUrl() {
        return mImageUrl;
    }
}
