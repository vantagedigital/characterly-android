package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.CompoundButton;

import com.mucaroo.R;
import com.tolstykh.textviewrichdrawable.CheckBoxRichDrawable;

/**
 * Created by ahsan onImage 9/30/2017.
 */

public class CustomCheckbox extends CheckBoxRichDrawable {

    @DrawableRes
    int onImage;
    @DrawableRes
    int offImage;

    private boolean checked = false;
    private boolean isRight = false;

    public CustomCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);

        setButtonDrawable(new StateListDrawable());

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomImageButton, 0, 0);
        try {

            onImage = ta.getResourceId(R.styleable.CustomImageButton_onImage, 0);
            offImage = ta.getResourceId(R.styleable.CustomImageButton_offImage, 0);
            checked = ta.getBoolean(R.styleable.CustomImageButton_isChecked, false);
            isRight = ta.getBoolean(R.styleable.CustomImageButton_isRight, false);
            setDrawable();

        } finally {
            ta.recycle();
        }

    }

    @Override
    public void setOnCheckedChangeListener(@Nullable final OnCheckedChangeListener listener) {
        super.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                listener.onCheckedChanged(compoundButton, isChecked);
                checked = isChecked;
                setDrawable();
            }
        });
    }

    private void setDrawable() {
        int drawable = checked ? onImage : offImage;

        if (drawable != 0)
            if (!isRight)
                setDrawableStartVectorId(drawable);
            else
                setDrawableEndVectorId(drawable);
    }

}
