package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mucaroo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahsan onImage 9/15/2017.
 */

public class CustomNotificationBadge extends FrameLayout {

    @BindView(R.id.counter)
    TextView counter;

    public CustomNotificationBadge(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.elem_notification_badge, this);
        ButterKnife.bind(this);

    }

    public void setCounter(int value) {
        counter.setText(String.valueOf(value));
        setVisibility(value <= 0 ? GONE : VISIBLE);
    }
}
