package com.mucaroo.helper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import com.squareup.picasso.Transformation;

public class CircleTransformation implements Transformation {

    @Override
    public Bitmap transform(Bitmap source) {
        return getCircleImage(source, true);
    }

    @Override
    public String key() {
        return "circle";
    }

    /**
     * @param recycle - only recycle if the source won't be used again an will be replaced by the
     *                new bitmap.
     */
    public static Bitmap getCircleImage(Bitmap source, boolean recycle) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        // get square version first, just incase this image is already a circle. This prevents the bug were it tries to clip a circle and the bitmap ends up being distorted.
        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);

        Bitmap circleBitmap = Bitmap.createBitmap(squaredBitmap.getWidth(),
                squaredBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(circleBitmap);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, squaredBitmap.getWidth(),
                squaredBitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);

        canvas.drawCircle(squaredBitmap.getWidth() / 2,
                squaredBitmap.getHeight() / 2, squaredBitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(squaredBitmap, rect, rect, paint);

        if (recycle) {
            source.recycle();
        }

        return circleBitmap;
    }

}
