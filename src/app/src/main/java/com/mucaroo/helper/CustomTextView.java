package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahsan onImage 8/16/2017.
 */

public class CustomTextView extends LinearLayout {

    @BindView(R.id.label)
    TextView label;

    @BindView(R.id.value)
    TextView value;

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.elem_textview, this);
        ButterKnife.bind(this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
        try {
            String text = ta.getString(R.styleable.CustomEditText_label);
            label.setText(text);

        } finally {
            ta.recycle();
        }
    }

    public TextView getLabel() {
        return label;
    }

    public void setText(String text) {
        this.value.setText(text);
    }
}