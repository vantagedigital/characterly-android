package com.mucaroo.helper;

public enum UploadCategories {
    UserProfile (1),
    ClassroomThumbnail (2),
    ClassroomHeader (3),
    TimelineComment (4),
    EvaluationQuestion (5),
    EvaluationQuestionOption (6),
    ContentThumbnail (7),
    ContentAttachment (8),
    PillarThumbnail (9),;

    private int mValue;

    UploadCategories(int value) {
        this.mValue = value;
    }

    public int getValue() {
        return mValue;
    }
}
