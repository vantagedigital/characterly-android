package com.mucaroo.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.QuestionOption;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vantage onImage 8/2/2017.
 */

public class Common {

    public static ArrayList<String> getDummyList() {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            String prefix = i < 10 ? "0" : "";
            strings.add("tag");
        }
        return strings;
    }

    public static void setRowDivider(Context context, RecyclerView recyclerView) {
        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(context, R.drawable.divider_1));
        recyclerView.addItemDecoration(divider);
    }

    public static void addViewFromLayout(Context context, int layout, ViewGroup view, int repeat) {
        for (int i = 0; i < repeat; i++) {
            View child = View.inflate(context, layout, null);
            view.addView(child);
        }

    }

    public static void addViewFromLayout(Context context, int layout, ViewGroup view, List<QuestionOption> data, final ListItemSelectedListener listener) {
        addViewFromLayout(context, layout, view, data, listener, null);
    }

    public static void addViewFromLayout(Context context, int layout, ViewGroup view, List<QuestionOption> data, final ListItemSelectedListener listener, List<Integer> selected) {
        for (int i = 0; i < data.size(); i++) {
            final View child = View.inflate(context, layout, null);
            if (layout == R.layout.item_evalutation_checkbox
                    || layout == R.layout.item_evalutation_img_checkbox
                    || layout == R.layout.item_evalutation_radio) {
                final CustomCheckbox cb = child.findViewById(R.id.cb);
                final Button cbButton = child.findViewById(R.id.btn_cb);
                final TextView cbText = child.findViewById(R.id.cb_text);
                final ImageView imgview = child.findViewById(R.id.image_view);
                if (imgview != null) {
                    ImageLoaderUtil.loadImageIntoView(App.getInstance(), imgview, data.get(i).fileAttachment.path);
                }
                if (cbButton != null) {
                    cbButton.setText(getLetter(i));
                }
                if (cbText != null) {
                    cbText.setText(data.get(i).description);
                    final int finalI = i;
                    child.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (listener != null) {
                                listener.onListItemSelected(finalI, child);
                            }
                        }
                    });
                }
                if (cb != null) {
                    if (selected != null) {
                        cb.setEnabled(false);
                        if (selected.contains(i)) {
                            cb.setChecked(true);
                        }
                    }
                    cb.setText(data.get(i).description);
                    final int finalI = i;
                    cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (listener != null) {
                                listener.onListItemSelected(finalI, cb);
                            }
                        }
                    });
                }
            }
            view.addView(child);
        }

    }

    private static String getLetter(int i) {
        if (i == 0) {
            return "A";
        } else if (i == 1) {
            return "B";
        } else if (i == 2) {
            return "C";
        } else if (i == 3) {
            return "D";
        } else if (i == 4) {
            return "E";
        } else {
            return "F";
        }
    }

    public static void addViewFromLayout(Context context, @IdRes int resId, int layout, ViewGroup view, int repeat) {
        for (int i = 0; i < repeat; i++) {
            View child = View.inflate(context, layout, null);
            View specificView = child.findViewById(resId);
            specificView.setTag(i);
            view.addView(specificView.getRootView());
        }
    }

    public static void scaleInlineDrawables(TextView btn, double fitFactor) {
        Drawable[] drawables = btn.getCompoundDrawables();

        for (int i = 0; i < drawables.length; i++) {
            if (drawables[i] != null) {
                int imgWidth = drawables[i].getIntrinsicWidth();
                int imgHeight = drawables[i].getIntrinsicHeight();
                if ((imgHeight > 0) && (imgWidth > 0)) {    //might be -1
                    float scale;
                    if ((i == 0) || (i == 2)) { //left or right -> scale height
                        scale = (float) (btn.getHeight() * fitFactor) / imgHeight;
                    } else { //top or bottom -> scale width
                        scale = (float) (btn.getWidth() * fitFactor) / imgWidth;
                    }
                    if (scale < 1.0) {
                        Rect rect = drawables[i].getBounds();
                        int newWidth = (int) (imgWidth * scale);
                        int newHeight = (int) (imgHeight * scale);
                        rect.left = rect.left + (int) (0.5 * (imgWidth - newWidth));
                        rect.top = rect.top + (int) (0.5 * (imgHeight - newHeight));
                        rect.right = rect.left + newWidth;
                        rect.bottom = rect.top + newHeight;
                        drawables[i].setBounds(rect);
                    }
                }
            }
        }
    }

    public static void goTo(Context context, Class target) {
        Intent intent = new Intent(context, target);
        context.startActivity(intent);
    }

    public static int getColorWrapper(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static Drawable getDrawableWrapper(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getDrawable(id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

    public static Boolean isEmpty(String string) {
        return ((string != null ? string.length() : 0) == 0);
    }

    public static void rotateView(View view) {
        float deg = (view.getRotation() == 180F) ? 0F : 180F;
        view.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public static void setInverseVisibility(View view) {
        int visibility = (view.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        view.setVisibility(visibility);
    }

    static void hideAllLayouts(LinearLayout[] layouts) {
        for (LinearLayout aMessagesArray : layouts) aMessagesArray.setVisibility(View.GONE);
    }

    public AlertDialog showAlertDialog(Context context, String title, String message) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showToast(String message) {
        Toast.makeText(App.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    public static void logOutput(String data) {
        System.out.println("DATA :: " + data);
        System.out.println("============================================================");
    }


    public static int getGridColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / 180);
    }

}
