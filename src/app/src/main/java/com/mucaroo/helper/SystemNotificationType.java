package com.mucaroo.helper;

public enum SystemNotificationType {
    BadgeAssignation(1),
    ClassroomAssignation(2),
    ContentSubmission(3),
    ContentApproval(4),
    ContentScheduleAssignation(5),
    EvaluationAssignation(6),
    UserEvaluationSubmission(7),
    UserEvaluationReview(8),
    InvitationUserToCMS(9),
    InvitationUserToContentManagement(10),
    InvitationUserToBranch(11),
    InvitationUserToClassroom(12),
    InvitationParentToStudent(13),
    TimelineComment(14),
    UserAccount(15),;

    private int mValue;

    SystemNotificationType(int value) {
        this.mValue = value;
    }

    public int getValue() {
        return mValue;
    }
}
