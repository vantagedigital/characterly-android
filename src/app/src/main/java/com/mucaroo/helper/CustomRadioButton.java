package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mucaroo.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ahsan onImage 8/13/2017.
 */

public class CustomRadioButton extends LinearLayout {

    private listener onSegmentSelectedListener;
    private CustomRadioButton segmentView;
    private int selectedBgColor;
    private int unselectedBgColor;
    private int selectedTextColor;
    private int unSelectedTextColor;
    private int segmentColorSelected;
    private int selectedIndex = 0;
    private View[] contents;

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context, attrs);
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context, attrs);
    }

    private void initLayout(Context mContext, AttributeSet attrs) {
        segmentView = this;

        List<String> titlesOfSegment = new ArrayList<>();

        TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.CustomRadioButtons);
        int middlePadding = typedArray.getInt(R.styleable.CustomRadioButtons_middleMargin, 2);
        int numberOfSegments = typedArray.getInt(R.styleable.CustomRadioButtons_numberOfSegment, 0);
        String titleWithComma = typedArray.getString(R.styleable.CustomRadioButtons_titlesOfSegment);
        selectedTextColor = typedArray.getColor(R.styleable.CustomRadioButtons_selectedTextColor, Color.WHITE);
        unSelectedTextColor = typedArray.getColor(R.styleable.CustomRadioButtons_unSelectedTextColor, Color.parseColor("#c13d1f"));
        segmentColorSelected = typedArray.getColor(R.styleable.CustomRadioButtons_segmentColor, 0);
        selectedBgColor = typedArray.getResourceId(R.styleable.CustomRadioButtons_selectedBgColor, R.drawable.bg_tab_active);
        unselectedBgColor = typedArray.getResourceId(R.styleable.CustomRadioButtons_unSelectedBgColor, R.drawable.bg_tab_inactive);

        int unitsTextSize = typedArray.getDimensionPixelSize(R.styleable.CustomRadioButtons_textSize, 0);

        if (titleWithComma != null && !titleWithComma.isEmpty()) {
            String[] arrayOfTitles = titleWithComma.split(",");
            Collections.addAll(titlesOfSegment, arrayOfTitles);
        }
        typedArray.recycle();

        segmentView.setOrientation(HORIZONTAL);

        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Semibold.ttf");

        for (int i = 0; i < numberOfSegments; i++) {

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
            param.leftMargin = middlePadding;
            Button btn = new Button(mContext);
            btn.setTypeface(font);

            if (titlesOfSegment.size() == numberOfSegments)
                btn.setText(titlesOfSegment.get(i));

            if (unitsTextSize != 0) {
                btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, unitsTextSize);
            } else
                btn.setTextSize(14.0f);

            if (i == 0) {
                param.rightMargin = middlePadding;
                btn.setBackgroundResource(selectedBgColor);
                btn.setTextColor(selectedTextColor);
                updateBorderAndSolidColor(btn);

            } else {
                btn.setBackgroundResource(unselectedBgColor);
                btn.setTextColor(unSelectedTextColor);
                btn.setLayoutParams(param);
                if (segmentColorSelected != 0) {
                    changeStrokeColor(btn, segmentColorSelected);
                }
            }

            final int x = i;
            btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    performTabClick(x);

                }
            });
            segmentView.addView(btn, param);
        }
    }

    public void performTabClick(int x) {
        if (onSegmentSelectedListener != null)
            onSegmentSelectedListener.onSegmentSelected(x);
        handleClick(x);
    }

    private void updateBorderAndSolidColor(Button btn) {
        if (segmentColorSelected != 0) {
            changeStrokeColor(btn, segmentColorSelected);
        }
        if (segmentColorSelected != 0) {
            changeSolidColor(btn, segmentColorSelected);
        }
    }

    public interface listener {
        void onSegmentSelected(int position);
    }

    public void onSelectedListner(listener onSegmentSelectedListener) {
        this.onSegmentSelectedListener = onSegmentSelectedListener;
    }


    private void handleClick(int position) {
        selectedIndex = position;

        for (int iter = 0; iter < segmentView.getChildCount(); iter++) {
            Button btnView = (Button) segmentView.getChildAt(iter);
            if (iter == position) {
                btnView.setBackgroundResource(selectedBgColor);
                btnView.setTextColor(selectedTextColor);
                updateBorderAndSolidColor(btnView);
            } else {
                btnView.setBackgroundResource(unselectedBgColor);
                btnView.setTextColor(unSelectedTextColor);
                if (segmentColorSelected != 0)
                    changeStrokeColor(btnView, segmentColorSelected);
            }
        }

        if (contents != null) {
            for (int i = 0; i < contents.length; i++)
                contents[i].setVisibility(position == i ? VISIBLE : GONE);
        }

    }

    private void changeStrokeColor(Button view, int colorCode) {
        GradientDrawable drawable = (GradientDrawable) view.getBackground();
        drawable.setStroke(3, colorCode);
    }

    private void changeSolidColor(Button view, int colorCode) {
        GradientDrawable drawable = (GradientDrawable) view.getBackground();
        drawable.setColor(colorCode);
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setupVisibility(View[] contents) {
        this.contents = contents;
    }

}