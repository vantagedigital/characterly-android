package com.mucaroo.helper;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;

public class ImageLoaderUtil {

    public static final String TAG = ImageLoaderUtil.class.getSimpleName();

    public static void loadImageIntoView(Context context, ImageView imageView, String url) {
        loadImageIntoView(context, imageView, null, url);
    }

    public static void loadImageIntoView(Context context, ImageView imageView, Uri uri) {
        loadImageIntoView(context, imageView, null, uri);
    }

    public static void loadImageIntoView(final Context context, final ImageView imageView,
                                         final Drawable placeHolder,
                                         String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        final String finalUrl = url;
        Picasso picasso = Picasso.with(context);
        RequestCreator requestCreator = null;
        if (CameraHelper.isPrivateFile(url) || CameraHelper.isFile(url)) {
            File file = new File(url);
            requestCreator = picasso.load(file);
        } else if (CameraHelper.isImageUri(url)) {
            Uri imageUri = Uri.parse(url);
            requestCreator = picasso.load(imageUri);
        } else {
            requestCreator = picasso.load(url);
        }
        requestCreator.placeholder(placeHolder).into(imageView, new Callback() {
            @Override
            public void onError() {
                Log.e(ImageLoaderUtil.class.getSimpleName(),"Error transforming image url:" + finalUrl);
                if (placeHolder != null) {
                    imageView.setImageDrawable(placeHolder);
                }
            }

            @Override
            public void onSuccess() {
            }
        });
    }

    public static void loadImageIntoView(final Context context, final ImageView imageView,
                                         final Drawable placeHolder,
                                         Uri uri) {

        final Uri finalUri = uri;
        Picasso.with(context).load(uri).placeholder(placeHolder).into(imageView, new Callback() {
            @Override
            public void onError() {
               Log.e(ImageLoaderUtil.class.getSimpleName(), "Error transforming image url:" + finalUri);
                if (placeHolder != null) {
                    imageView.setImageDrawable(placeHolder);
                }
            }

            @Override
            public void onSuccess() {
            }
        });
    }

    public static void loadImageIntoViewResized(final Context context, final ImageView imageView,
                                                final Drawable placeHolder, int width, int height, String url) {

        Picasso.with(context).load(url).placeholder(placeHolder)
                .resize(width, height).into(
                imageView, new Callback() {
                    @Override
                    public void onError() {
                        Log.e(ImageLoaderUtil.class.getSimpleName(),"Error transforming image");
                        imageView.setImageDrawable(placeHolder);
                    }

                    @Override
                    public void onSuccess() {
                    }
                }
        );

    }

//    public static void loadBitmapFromByteArray(final ImageView imageView, final byte[] imageByteArray) {
//        if (imageByteArray == null) {
//            Log.e(ImageLoaderUtil.class.getSimpleName(),"Error imageByteArray is null");
//            return;
//        }
//        new SafeAsyncTask<Bitmap>() {
//
//            @Override
//            protected Bitmap safeDoInBackground(Void... params) {
//                return BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);
//            }
//
//            @Override
//            protected void safeOnPostExecute(Bitmap bitmap) {
//                if (imageView != null) {
//                    imageView.setImageBitmap(bitmap);
//                }
//
//            }
//        }.execute();
//    }

    public static void loadUsingPicasso(Picasso picasso, ImageView imageView, String imageUrl) {
        loadUsingPicasso(picasso, imageView, imageUrl, false);
    }

    public static void loadUsingPicasso(Picasso picasso, ImageView imageView, String imageUrl, boolean circle) {
        RequestCreator requestCreator = null;
        if (CameraHelper.isPrivateFile(imageUrl)) {
            File file = new File(imageUrl);
            requestCreator = picasso.load(file);
        } else if (CameraHelper.isImageUri(imageUrl)) {
            Uri imageUri = Uri.parse(imageUrl);
            requestCreator = picasso.load(imageUri);
        } else {
            requestCreator = picasso.load(imageUrl);
        }

        if (circle && requestCreator != null) {
            requestCreator = requestCreator.transform(new CircleTransformation());
        }

        if (requestCreator != null) {
            requestCreator.into(imageView);
        }
    }

}
