package com.mucaroo.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;

import java.security.InvalidParameterException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vantage onImage 8/2/2017.
 */

public class CustomMessage extends LinearLayout {

    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int WARNING = 2;
    public static final int INFO = 3;

    int[] titles = {R.string.msg_success, R.string.msg_error, R.string.msg_info, R.string.msg_warning};
    int[] text_colors = {R.color.nasty_green, R.color.tomato_two, R.color.dusty_orange_two, R.color.slate};
    int[] lay_bgs = {R.drawable.bg_msg_success, R.drawable.bg_msg_error, R.drawable.bg_msg_info, R.drawable.bg_msg_warning};

    private Context context;

    @BindView(R.id.ico_msg)
    ImageView ico_msg;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_detail)
    TextView tv_detail;

    @OnClick(R.id.btn_close)
    void close() {
        setVisibility(GONE);
    }

    public CustomMessage(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.elem_msg, this);
        ButterKnife.bind(this);
        hideMessage();
    }

    public void hideMessage() {
        setVisibility(GONE);
    }

    public void showMessage(int type, String title, String msg) throws InvalidParameterException {

        if (type < 0 || type > 3)
            throw new InvalidParameterException();
        else {
            int text_color = Common.getColorWrapper(context, text_colors[type]);
            setBackgroundResource(lay_bgs[type]);
            tv_title.setText(title);
            tv_detail.setText(msg);
            tv_title.setTextColor(text_color);
            tv_detail.setTextColor(text_color);
            ico_msg.setImageLevel(type);
            setVisibility(VISIBLE);
        }
    }
}