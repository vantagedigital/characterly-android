package com.mucaroo.helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.mucaroo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahsan on 10/16/2017.
 */

public class CustomProgressDialog extends AlertDialog {

    TextView tv_msg;

    public CustomProgressDialog(@NonNull Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.elem_progress, null);
        setView(dialogView);
        setCancelable(false);
        tv_msg = ButterKnife.findById(dialogView, R.id.tv_msg);

    }

    public void setMessage(String msg) {
        tv_msg.setText(msg);
    }

}
