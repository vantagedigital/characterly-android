package com.mucaroo.helper;

import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.FileAttachmentModelSingle;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FileUploadHelper {

    private void fileupload() {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "Camera" + File.separator + "20180107_140232.jpg";
        final File file = new File(path);
        if (file.exists()) {
            Log.e("LESSON DET", "FILE EXISTS");
        } else {
            Log.e("LESSON DET", "FILE DOES NOT EXISTS");
            Log.e("LESSON DET", "path:" + path);

        }

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                uploadFile(file);
//            }
//        }).start();
    }

    public static void uploadFile(final String path, final String mimType, final String category, final String entityId, final NetworkHelper.Listener<FileAttachmentModelSingle> listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String json = getFileUploadResponse(path, mimType, category, entityId);
                if (TextUtils.isEmpty(json)) {
                    if (listener != null) {
                        listener.onFailure("Failure uploading");
                    }
                } else {
                    Gson mGson = new Gson();
                    FileAttachmentModelSingle response = mGson.fromJson(json, FileAttachmentModelSingle.class);
                    if (listener != null) {
                        listener.onResponse(response);
                    }
                }
                Log.e("LESSON DET", "DONE uploading:" + json);
            }
        }).start();
    }

    private static String getFileUploadResponse(String path, String mimType, String category, String entityId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("category", category);
        params.put("entityId", entityId);

        return multipartRequest(NetworkHelper.URL + "services/app/FileAttachment/Upload", params, path, "file", mimType);
    }

    // sample
    private void uploadFile(File file) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("category", "1");
        params.put("entityId", "4");

        String result = multipartRequest(NetworkHelper.URL + "services/app/FileAttachment/Upload", params, file.getAbsolutePath(), "file", "image/jpg");
        Log.e("LESSON DET", "DONE uploading:" + result);
    }

    private static String multipartRequest(String urlTo, Map<String, String> parmas, String filepath, String filefield, String fileMimeType) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;

        Log.e("FIL","params:"+ parmas);
        Log.e("FIL","filepath:"+ filepath);
        Log.e("FIL","urlTo:"+ urlTo);

        String twoHyphens = "--";
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String lineEnd = "\r\n";

        String result = "";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        String[] q = filepath.split("/");
        int idx = q.length - 1;

        try {
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL(urlTo);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);

            // Upload POST Data
            Iterator<String> keys = parmas.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = parmas.get(key);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


            if (200 != connection.getResponseCode()) {
                Log.e("LESSON DET", "Failed to upload code" + connection.getResponseCode() + " " + connection.getResponseMessage());
            }

            inputStream = connection.getInputStream();

            result = convertStreamToString(inputStream);

            fileInputStream.close();
            inputStream.close();
            outputStream.flush();
            outputStream.close();

            return result;
        } catch (Exception e) {
            Log.e("LESSON DET", "exception uploading:" + e, e);
            return result;
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
