package com.mucaroo.helper;

public enum UserContentScheduleStatus {
    Unknown (-1),
    Assigned (0),
    InProgress (1),
    Completed (2),;

    private int mValue;

    UserContentScheduleStatus(int value) {
        this.mValue = value;
    }

    public int getValue() {
        return mValue;
    }
}
