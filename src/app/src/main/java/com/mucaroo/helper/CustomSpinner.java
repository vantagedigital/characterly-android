package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.adapters.CustomSpinnerAdapter;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.AuthenticationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahsan onImage 8/18/2017.
 */

public class CustomSpinner extends LinearLayout {

    @BindView(R.id.label)
    TextView label;

    @BindView(R.id.custom_spinner)
    Spinner spinner;

    Context context;

    int selectedPosition = 0;

    private ListItemSelectedListener mListener;

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.elem_spinner, this);
        ButterKnife.bind(this);


        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
        try {
            String text = ta.getString(R.styleable.CustomEditText_label);

            if (Common.isEmpty(text))
                label.setVisibility(GONE);
            else
                label.setText(text);

        } finally {
            ta.recycle();
        }

    }

    public Spinner getSpinner() {
        return spinner;
    }

    public TextView getLabel() {
        return label;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSpinner(ArrayList<String> strings) {

        final CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(context, R.layout.item_spinner, strings);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int pos, long arg3) {
                selectedPosition = pos;
                TextView selectedView = (TextView) parent.getChildAt(0);
                if (selectedView != null) {
                    selectedView.setBackgroundColor(Color.TRANSPARENT);
                }
                adapter.setSelectedIndex(pos);
                if (mListener != null) {
                    mListener.onListItemSelected(pos, selectedView);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setSelectedListener(ListItemSelectedListener mListener) {
        this.mListener = mListener;
    }
}


