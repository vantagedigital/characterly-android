package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.dialogs.BadgeDetailDialog;
import com.mucaroo.models.Badge;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ahsan onImage 8/16/2017.
 */

public class CustomBadgeLayout extends LinearLayout {

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.btn_badge)
    CircleImageView btn_badge;

    @BindView(R.id.badge_counter)
    CustomNotificationBadge badge_counter;

    Context context;

    private Badge mBadge;

    @OnClick(R.id.btn_badge)
    void badge_detail() {
        BadgeDetailDialog dialog = new BadgeDetailDialog(context);
        dialog.setBadge(mBadge);
        dialog.show();
    }

    public CustomBadgeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.elem_badge, this);

        ButterKnife.bind(this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
        try {
            String text = ta.getString(R.styleable.CustomEditText_label);
            name.setText(text);

        } finally {
            ta.recycle();
        }

    }

    public void setBadge(Badge mBadge) {
        this.mBadge = mBadge;
        try {
            setName(mBadge.pillar.name);
        }catch (Exception e) {

        }
        setCounter(0);
    }

    public CircleImageView getBadgeThumbnail() {
        return btn_badge;
    }

    private void setName(String name) {
        this.name.setText(name);
    }

    private void setCounter(int counter) {
        badge_counter.setCounter(counter);
    }
}