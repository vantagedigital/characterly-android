package com.mucaroo.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.before.Login;
import com.mucaroo.activities.classroom.Classrooms;
import com.mucaroo.activities.evaluation.EvaluationMain;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.activities.profile.ProfileEducator;
import com.mucaroo.activities.profile.ProfileStudent;
import com.mucaroo.activities.search.Search;
import com.mucaroo.application.App;
import com.mucaroo.cache.UserCache;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.Notification;
import com.mucaroo.models.NotificationModel;
import com.mucaroo.models.UserInfoModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.mucaroo.helper.NetworkHelper.FileAttachmentGetByEntityId;
import static com.mucaroo.helper.NetworkHelper.NotificationGetAll;

/**
 * Created by ahsan onImage 8/22/2017.
 */

public class CustomHeader extends LinearLayout {

    @BindView(R.id.et_search)
    EditText et_search;

    @BindView(R.id.lay_menu)
    LinearLayout lay_menu;

    @BindView(R.id.btn_search)
    CustomImageButton btn_search;

    @BindView(R.id.btn_menu)
    CustomImageButton btn_menu;

    @BindView(R.id.iv_user)
    CircleImageView iv_user;

    @BindView(R.id.profile_content)
    LinearLayout profile_content;

    @BindView(R.id.badge_counter)
    CustomNotificationBadge badge_counter;

    public NetworkHelper networkHelper;

    private String mCurrentImagePath = "";

    private Handler mHandler = new Handler();

    private String mSearchType;

    @OnClick(R.id.btn_user_profile)
    void profile() {
        Common.setInverseVisibility(profile_content);
    }

    @OnClick(R.id.btn_search)
    void search(CustomImageButton btn) {
        hideLayout(btn_menu, lay_menu);
        btn.toggle();
        et_search.setVisibility(btn.isChecked() ? VISIBLE : GONE);

    }

    @OnClick(R.id.btn_menu)
    void menu(CustomImageButton btn) {
        hideLayout(btn_search, et_search);
        btn.toggle();
        lay_menu.setVisibility(btn.isChecked() ? VISIBLE : GONE);
    }

    @OnClick(R.id.btn_notifications)
    void btn_notifications() {
        Intent myIntent = ProfileEducator.newIntent(context, true);
        if (UserCache.getInstance().getUserInfo().result.user.educator == null) {
            myIntent = ProfileStudent.newIntent(context, true);
        }
        context.startActivity(myIntent);
    }

    @OnClick(R.id.btn_myaccount)
    void btn_myaccount() {
        Intent myIntent = ProfileEducator.newIntent(context);
        if (UserCache.getInstance().getUserInfo().result.user.educator == null) {
            myIntent = ProfileStudent.newIntent(context);
        }
        context.startActivity(myIntent);
    }

    @OnClick(R.id.btn_signout)
    void btn_signout() {
        UserCache.getInstance().clear();
        Intent myIntent = new Intent(context, Login.class);
        context.startActivity(myIntent);
    }

    @OnClick(R.id.btn_lessons)
    void btn_lessons() {
        context.startActivity(LessonMain.newIntent(context, UserCache.getInstance().getContentInfo()));
    }

    @OnClick(R.id.btn_classrooms)
    void btn_classrooms() {
        Intent myIntent = new Intent(context, Classrooms.class);
        context.startActivity(myIntent);
    }

    @OnClick(R.id.btn_evaluations)
    void btn_evaluations() {
        context.startActivity(EvaluationMain.newIntent(context));
    }

    Context context;

    public CustomHeader(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.elem_header, this);
        ButterKnife.bind(this);

        initEditorActionListener();

        networkHelper = new NetworkHelper(this.context);
        updatePic();
        updateNotifications();

    }

    public void setSearchType(String mSearchType) {
        this.mSearchType = mSearchType;
    }

    private void updateNotifications() {
        networkHelper.getRequest(NotificationGetAll, new NotificationModel(), new NetworkHelper.Listener<NotificationModel>() {
            @Override
            public void onResponse(NotificationModel model) {
                UserCache.getInstance().setNotifications(model);

                updateBadgeCounter();
            }

            @Override
            public void onFailure(String msg) {
//                        Common.showToast("error getting notifications");
            }
        });
    }

    private void updateBadgeCounter() {
        int count = 0;
        if (UserCache.getInstance().getNotifications() != null) {
            for (Notification notification : UserCache.getInstance().getNotifications().result) {
                if (!notification.isRead) {
                    count++;
                }
            }
        }
        badge_counter.setCounter(count);
    }

    private void updatePic() {
        try {
            if (TextUtils.isEmpty(UserCache.getInstance().getUserInfo().result.user.imagePath)) {
                networkHelper.getRequest(FileAttachmentGetByEntityId + UserCache.getInstance().getUserInfo().result.user.id, new FileAttachmentModel(), new NetworkHelper.Listener<FileAttachmentModel>() {
                    @Override
                    public void onResponse(FileAttachmentModel model) {
                        try {
                            UserInfoModel userModel = UserCache.getInstance().getUserInfo();
                            userModel.result.user.imagePath = model.result.get(model.result.size() - 1).path;
                            UserCache.getInstance().setCurrentUser(userModel);

                            mCurrentImagePath = UserCache.getInstance().getUserInfo().result.user.imagePath;
                            ImageLoaderUtil.loadImageIntoView(App.getInstance(), iv_user, mCurrentImagePath);
                            checkCurrentImage();
                        } catch (Exception e) {
                            Log.e("ProfileStudent", "error saving profile image");
                        }
                    }

                    @Override
                    public void onFailure(String msg) {
//                        Common.showToast("error getting notifications");
                    }
                });
            } else {
                mCurrentImagePath = UserCache.getInstance().getUserInfo().result.user.imagePath;
                ImageLoaderUtil.loadImageIntoView(App.getInstance(), iv_user, mCurrentImagePath);
                checkCurrentImage();
            }
        } catch (Exception e) {
            checkCurrentImage();
        }
    }

    void hideLayout(CustomImageButton btn, View lay) {
        lay.setVisibility(GONE);
        btn.setChecked(false);
    }

    void initEditorActionListener() {
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH && et_search.getText().toString() != "") {
                    context.startActivity(Search.newIntent(context, mSearchType, et_search.getText().toString()));
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
            this.clearFocus();
            inputManager
                    .hideSoftInputFromWindow(this.getWindowToken(), 0);
    }

    private void checkCurrentImage() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (UserCache.getInstance().getUserInfo().result.user.imagePath != null && !mCurrentImagePath.equalsIgnoreCase(UserCache.getInstance().getUserInfo().result.user.imagePath)) {
                        mCurrentImagePath = UserCache.getInstance().getUserInfo().result.user.imagePath;
                        ImageLoaderUtil.loadImageIntoView(App.getInstance(), iv_user, mCurrentImagePath);
                    }
                } catch (Exception e) {

                }
                mHandler.postDelayed(this, 1000);
            }
        });

    }

}