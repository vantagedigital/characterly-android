package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.mucaroo.R;

/**
 * Created by ahsan onImage 8/23/2017.
 */

public class CustomBorderView extends LinearLayout {

    Context context;

    GradientDrawable drawable = new GradientDrawable();

    public CustomBorderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        drawable.setShape(GradientDrawable.RECTANGLE);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomBorderView, 0, 0);
        try {

            int borderWidth = ta.getDimensionPixelSize(R.styleable.CustomBorderView_borderWidth, 1);
            int borderColor = ta.getColor(R.styleable.CustomBorderView_borderColor, Color.TRANSPARENT);
            float borderRadius = ta.getDimensionPixelSize(R.styleable.CustomBorderView_borderRadius, 5);
            int fillColor = ta.getColor(R.styleable.CustomBorderView_fillColor, Color.TRANSPARENT);
            drawable.setStroke(borderWidth, borderColor);
            drawable.setCornerRadius(borderRadius);
            drawable.setColor(fillColor);
            setBorderView();

        } finally {
            ta.recycle();
        }
    }

    public void setBorderRadius(int borderRadius) {
        drawable.setCornerRadius(borderRadius);
    }


    public void setBorderStroke(int width, int color) {
        drawable.setStroke(width, getColor(color));
    }

    public void setFillColor(int fillColor) {
        drawable.setColor(getColor(fillColor));
    }

    public void setBorderView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            setBackground(drawable);
        else
            setBackgroundDrawable(drawable);
    }

    private int getColor(int color) {
        return Common.getColorWrapper(context, color);
    }

}
