package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Vantage onImage 8/3/2017.
 */

public class CustomEditText extends LinearLayout {

    @BindView(R.id.label)
    TextView label;

    @BindView(R.id.et)
    EditText input;

    Context context;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.elem_edittext, this);
        ButterKnife.bind(this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
        try {
            String text = ta.getString(R.styleable.CustomEditText_label);
            label.setText(text);

            String hint = ta.getString(R.styleable.CustomEditText_placeholder);
            setHint(hint);

        } finally {
            ta.recycle();
        }

    }

    public void setText(String label) {
        this.input.setText(label);
    }

    public EditText getInput() {
        return input;
    }

    public void makeSecure() {
        input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        input.setTransformationMethod(new PasswordTransformationMethod());

    }

    public String getText() {
        return input.getText().toString();
    }

    public void setCenter() {
        input.setGravity(Gravity.CENTER);
    }

    public void setHint(String hint) {
        input.setHint(hint);
    }

    public void setError(Boolean flag, String msg) {

        if (msg == null)
            msg = "Input missing";

        int bg = R.drawable.bg_field_normal;
        int text = R.color.grey_blue;
        input.setError(null);
        if (flag) {
            bg = R.drawable.bg_field_error;
            text = R.color.tomato;
            input.setError(msg);
        }

        input.setBackgroundResource(bg);
        input.setTextColor(Common.getColorWrapper(context, text));

    }


    //

    public boolean validateEmail() {
        boolean validate = isValidEmail(getText());
        setError(!validate, "Invalid Email");
        return validate;
    }

    public boolean validateLength(int length, String msg) {
        String string = getText();
        boolean validate = !(isNullOrEmpty(string) || string.length() < length);
        setError(!validate, msg);
        return validate;
    }


    // Validations

    public static boolean isValidEmail(String string) {
        string = string.trim();
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public static boolean isValidLength(String string, int length) {
        return !(isNullOrEmpty(string) || string.length() < length);
    }

    public static boolean isValidPassword(String string, boolean allowSpecialChars) {
        String PATTERN = allowSpecialChars ? "^[a-zA-Z@#$%]\\w{5,19}$" : "^[a-zA-Z]\\w{5,19}$";
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public static boolean isNullOrEmpty(String string) {
        return TextUtils.isEmpty(string);
    }

    public static boolean isNumeric(String string) {
        return TextUtils.isDigitsOnly(string);
    }


}