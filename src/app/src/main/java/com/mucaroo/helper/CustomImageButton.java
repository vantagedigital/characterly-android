package com.mucaroo.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.view.View;

import com.mucaroo.R;

/**
 * Created by ahsan onImage 8/20/2017.
 */

public class CustomImageButton extends AppCompatImageButton implements View.OnClickListener {

    private boolean checked = false;

    Drawable onImage;
    Drawable offImage;

    public CustomImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        setScaleType(ScaleType.CENTER_INSIDE);
        setOnClickListener(this);
        setAdjustViewBounds(true);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomImageButton, 0, 0);
        try {

            checked = ta.getBoolean(R.styleable.CustomImageButton_isChecked, false);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                onImage = ta.getDrawable(R.styleable.CustomImageButton_onImage);
                offImage = ta.getDrawable(R.styleable.CustomImageButton_offImage);
            } else {
                final int onRedId = ta.getResourceId(R.styleable.CustomImageButton_onImage, -1);
                final int offResId = ta.getResourceId(R.styleable.CustomImageButton_offImage, -1);

                if (onRedId != -1)
                    onImage = AppCompatResources.getDrawable(context, onRedId);
                if (offResId != -1)
                    offImage = AppCompatResources.getDrawable(context, offResId);

            }

            updateImageResource();

        } finally {
            ta.recycle();
        }
    }

    public void toggle() {
        this.checked = !this.checked;
        updateImageResource();
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        updateImageResource();
    }

    public boolean isChecked() {
        return checked;
    }

    void updateImageResource() {
        setImageDrawable(checked ? onImage : offImage);
    }

    @Override
    public void onClick(View view) {
        checked = !checked;
        updateImageResource();
    }
}