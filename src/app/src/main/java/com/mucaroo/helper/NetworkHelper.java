package com.mucaroo.helper;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.mucaroo.activities.before.Login;
import com.mucaroo.cache.UserCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by ahsan on 10/8/2017.
 */

public class NetworkHelper {

    public interface Listener<T> {
        void onResponse(T var1);

        void onFailure(String msg);
    }

    //    public static final String URL = "http://13.92.193.213:80/api/";
    public static final String URL = "http://betaapi.character.ly/api/";
    public static final int MAX_RESULTS = 10;
    public static final String Authenticate = "TokenAuth/Authenticate";
    public static final String UserInfo = "services/app/Session/GetCurrentLoginInformations";
    public static final String GetMyContent = "services/app/UserContent/GetMyContent?";
    public static final String GetMyAllContentSchedule = "services/app/ContentSchedule/GetAll";
    public static final String UserContentSearch = "services/app/UserContent/Search";
    public static final String UserContentSearchMyContent = "services/app/UserContent/SearchMyContent";
    public static final String StudentGetAll = "services/app/Student/GetAll";
    public static final String StudentUpdate = "services/app/Student/Update";
    public static final String EducatorUpdate = "services/app/Educator/Update";
    public static final String ParentUpdate = "services/app/Parent/Update";
    public static final String NotificationGetAll = "services/app/SystemNotification/GetMyNotifications";
    public static final String NotificationMarkRead = "services/app/SystemNotification//MarkAsRead?id=";
    public static final String StudentGetByClassroomId = "services/app/Student/GetByClassroomId?id=";
    public static final String EvaluationGetByClassroomId = "services/app/Evaluation/GetByClassroomId?id=";
    public static final String EvaluationGetAll = "services/app/Evaluation/GetAll?";
    public static final String EvaluationGetAllMine = "services/app/UserEvaluation/GetMyEvaluations?";
    public static final String EvaluationGetByContentId = "services/app/Evaluation/GetByContentId?id=";
    public static final String UserEvaluationGetByUserAndContentId = "services/app/UserEvaluation/GetByUserIdAndContentId?userId=";
    public static final String UserEvaluationGetByUserId = "services/app/UserEvaluation/GetByUserId?id=";
    public static final String EvaluationAssignToClassroom = "services/app/Evaluation/AssignToClassroom?";
    public static final String ContentScheduleAssignToClassroom = "services/app/ContentSchedule/AssignToClassroom?";
    public static final String StudentSearch = "services/app/Student/Search";
    public static final String EvaluationSearch = "services/app/Evaluation/Search";
    public static final String UserEvaluationSearch = "services/app/UserEvaluation/Search";
    public static final String EvaluationSubmit = "services/app/UserEvaluation/Submit";
    public static final String EvaluationReview = "services/app/UserEvaluation/Review";
    public static final String GradeGetAll = "services/app/Grade/GetAll";
    public static final String GetTimelineCommentsByClassId = "services/app/Timeline/GetTimelineComments?timelineId=";
    public static final String ClassroomCreate = "services/app/Classroom/Create";
    public static final String ClassroomUpdate = "services/app/Classroom/Update";
    public static final String UserContentUpdate = "services/app/UserContent/Update";
    public static final String ClassroomGet = "services/app/Classroom/Get?id=";
    public static final String ClassroomGetAll = "services/app/Classroom/GetAll";
    public static final String PillarGetAll = "services/app/Pillar/GetAll";
    public static final String SubjectGetAll = "services/app/Subject/GetAll";
    public static final String ClassroomDelete = "services/app/Classroom/Delete?id=";
    public static final String FileAttachmentGetByEntityId = "services/app/FileAttachment/GetByEntityId?id=";
    public static final String BadgeGetByUserId = "services/app/Badge/GetByUserId?id=";
    public static final String TimelineAddComment = "services/app/Timeline/AddComment";
    /*
If UserId is used, there'"'"'s no need to provide Name,
Surname, EmailAddress or Code.

TypeId is an existing record Id belonging to one of the following:
- Branch
- Classroom
- Student

For example:
If I want to add a student to a classroom,
type should be 3 and typeId should be the Id of the classroom

Invitation types:
- UserToCMS = 0,
- UserToContentManager = 1,
- EducatorToBranch = 2,
- StudentToClassroom = 3,
- ParentToClassroom = 4,
- ParentToStudent = 5,
*/
    public static final String StudentInvite = "services/app/Invite/Create";
    public static final String StudentCreate = "services/app/Student/Create";
    public static final String GetByEducatorId = "services/app/Classroom/GetByEducatorId?id=";
    public static final String GetByStudentId = "services/app/Classroom/GetByStudentId?id=";
    public static final String ResetPasswordRequest = "services/app/Account/ResetPasswordRequest";
    public static final String ResetPassword = "services/app/Account/ResetPassword";
    public static final String CreateAccount = "services/app/Invite/Validate";
    public static final String RecoverAccessCode = "services/app/Account/RecoverAccessCode?email=";


    private Context context;

    public NetworkHelper(Context context) {
        this.context = context;
    }

    public ANRequest.PostRequestBuilder postRequest(String api) {
        ANRequest.PostRequestBuilder builder = AndroidNetworking.post(getURL(api))
                .addHeaders("Accept", "application/json, text/json, text/plain")
                .addHeaders("Content-Type", "application/json");
        if (UserCache.getInstance().isLoggedIn()) {
            builder = builder.addHeaders("Authorization", UserCache.getInstance().getAccessToken());
        }
        return builder;
    }

    public ANRequest.PostRequestBuilder putRequest(String api) {
        ANRequest.PostRequestBuilder builder = AndroidNetworking.put(getURL(api))
                .addHeaders("Accept", "application/json, text/json, text/plain")
                .addHeaders("Content-Type", "application/json");
        if (UserCache.getInstance().isLoggedIn()) {
            builder = builder.addHeaders("Authorization", UserCache.getInstance().getAccessToken());
        }
        return builder;
    }

    public ANRequest.GetRequestBuilder getRequest(String api) {
        ANRequest.GetRequestBuilder builder = AndroidNetworking.get(getURL(api))
                .addHeaders("Accept", "application/json, text/json, text/plain")
                .addHeaders("Content-Type", "application/json");
        if (UserCache.getInstance().isLoggedIn()) {
            builder = builder.addHeaders("Authorization", UserCache.getInstance().getAccessToken());
        }
        return builder;
    }

    public ANRequest.PostRequestBuilder deleteRequest(String api) {
        ANRequest.PostRequestBuilder builder = AndroidNetworking.delete(getURL(api))
                .addHeaders("Accept", "application/json, text/json, text/plain")
                .addHeaders("Content-Type", "application/json");
        if (UserCache.getInstance().isLoggedIn()) {
            builder = builder.addHeaders("Authorization", UserCache.getInstance().getAccessToken());
        }
        return builder;
    }

    private static String getURL(String api) {
        return URL + api;
    }

    @SuppressWarnings("unchecked")
    public void getRequest(String api, final Object object, final Listener listener) {

        ANRequest request = getRequest(api)
                .build();
        getAsObject(request, object, listener);

    }

    public void deleteRequest(String api, final Object object, final Listener listener) {

        ANRequest request = deleteRequest(api)
                .build();
        getAsObject(request, object, listener);

    }

    public void postRequest(String api, Map<String, Object> map, final Object object, final Listener listener) {
        JSONObject jsonObject = new JSONObject(map);
        Common.logOutput(jsonObject.toString());
        ANRequest request = postRequest(api)
                .addJSONObjectBody(jsonObject)
                .build();
        getAsObject(request, object, listener);
    }

    public void putRequest(String api, Map<String, Object> map, final Object object, final Listener listener) {
        JSONObject jsonObject = new JSONObject(map);
        Common.logOutput(jsonObject.toString());
        ANRequest request = putRequest(api)
                .addJSONObjectBody(jsonObject)
                .build();
        getAsObject(request, object, listener);
    }


    @SuppressWarnings("unchecked")
    public void getAsObject(ANRequest request, final Object object, final Listener listener) {
//        final CustomProgressDialog pd = new CustomProgressDialog(context);
//        pd.show();
        request.getAsObject(object.getClass(), new ParsedRequestListener<Object>() {
            @Override
            public void onResponse(Object o) {
//                pd.dismiss();
                if (listener != null)
                    listener.onResponse(o);
            }

            @Override
            public void onError(ANError anError) {
//                pd.dismiss();
                listener.onFailure(errorHandler(anError, null));
            }
        });
    }

    public String errorHandler(ANError error, CustomMessage customMessage) {
        String messageReturned = "";
        if (error.getErrorCode() != 0) {
            try {
                JSONObject jObject = new JSONObject(error.getErrorBody());
                JSONObject errorObj = jObject.optJSONObject("error");
                String message = errorObj.optString("message");
                String details = errorObj.optString("details");
                if (message != null && message.contains("user session")) {
                    message = message + ".Please sign in again";
                    // Logout
                    UserCache.getInstance().clear();
                    Intent myIntent = new Intent(context, Login.class);
                    context.startActivity(myIntent);
                }
                if (customMessage != null)
                    customMessage.showMessage(CustomMessage.ERROR, message, details);
                else
                    Common.showToast(context, message);
                messageReturned = message;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("", "onError errorCode : " + error.getErrorCode());
            Log.d("", "onError errorBody : " + error.getErrorBody());
            Log.d("", "onError errorDetail : " + error.getErrorDetail());

        } else {
            Log.d("", "onError errorDetail : " + error.getErrorDetail());
        }
        return messageReturned;
    }


}
