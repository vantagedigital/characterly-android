package com.mucaroo.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mucaroo.application.App;
import com.mucaroo.models.BadgeModel;
import com.mucaroo.models.MyContentModel;
import com.mucaroo.models.NotificationModel;
import com.mucaroo.models.UserInfoModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class UserCache {

    private static UserCache sInstance;

    private static final String PREFS_NAME = "CharacterlyUserCache";

    private static final String KEY_ACCESS_TOKEN = "access_token";
    private static final String KEY_ENCRYPTED_ACCESS_TOKEN = "encrypted_access_token";
    private static final String KEY_USER_INFO = "user_info";
    private static final String KEY_CONTENT_INFO = "content_info";
    private static final String KEY_BADGE_INFO = "badge_info";
    private static final String KEY_NOTIFICATIONS = "notifications";
    private static final String KEY_USER_ID = "user_id";


    private transient final SharedPreferences mSharedPreferences;

    private UserInfoModel mUserInfo;
    private MyContentModel mMyContentModel;
    private BadgeModel mBadgeModel;
    private NotificationModel mNotificationModel;

    private Type mListType;
    private transient final Gson mGson = new Gson();

    private UserCache() {
        mSharedPreferences = App.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mListType = new TypeToken<ArrayList<String>>() {
        }.getType();
    }

    public static UserCache getInstance() {
        if (sInstance == null) {
            sInstance = new UserCache();
        }
        return sInstance;
    }

    public boolean isLoggedIn() {
        return mSharedPreferences.contains(KEY_ACCESS_TOKEN);
    }

    //region Saved Prefs

    public String getAccessToken() {
        return mSharedPreferences.getString(KEY_ACCESS_TOKEN, null);
    }

    public void setAccessToken(String value) {
        value = value == null ? value : "Bearer " + value;
        mSharedPreferences.edit().putString(KEY_ACCESS_TOKEN, value).commit();
    }

    public String getEncryptedAccessToken() {
        return mSharedPreferences.getString(KEY_ENCRYPTED_ACCESS_TOKEN, null);
    }

    public void setEncryptedAccessToken(String value) {
        mSharedPreferences.edit().putString(KEY_ENCRYPTED_ACCESS_TOKEN, value).commit();
    }

    public void setUserId(String value) {
        mSharedPreferences.edit().putString(KEY_USER_ID, value).commit();
    }

    public String getUserId() {
        return mSharedPreferences.getString(KEY_USER_ID, null);
    }


    public UserInfoModel getUserInfo() {
        if (mUserInfo == null) {
            String json = mSharedPreferences.getString(KEY_USER_INFO, null);
            if (!TextUtils.isEmpty(json)) {
                mUserInfo = mGson.fromJson(json, UserInfoModel.class);
            }
        }
        return mUserInfo;
    }
    public MyContentModel getContentInfo() {
        if (mMyContentModel == null) {
            String json = mSharedPreferences.getString(KEY_CONTENT_INFO, null);
            if (!TextUtils.isEmpty(json)) {
                mMyContentModel = mGson.fromJson(json, MyContentModel.class);
            }
        }
        return mMyContentModel;
    }
    public BadgeModel getBadgeInfo() {
        if (mBadgeModel == null) {
            String json = mSharedPreferences.getString(KEY_BADGE_INFO, null);
            if (!TextUtils.isEmpty(json)) {
                mBadgeModel = mGson.fromJson(json, BadgeModel.class);
            }
        }
        return mBadgeModel;
    }
    public NotificationModel getNotifications() {
        if (mNotificationModel == null) {
            String json = mSharedPreferences.getString(KEY_NOTIFICATIONS, null);
            if (!TextUtils.isEmpty(json)) {
                mNotificationModel = mGson.fromJson(json, NotificationModel.class);
            }
        }
        return mNotificationModel;
    }

    public void setCurrentUser(UserInfoModel data) {
        mUserInfo = data;
        mSharedPreferences.edit().putString(KEY_USER_INFO, data == null ? null : mGson.toJson(data)).commit();
    }

    public void setContentInfo(MyContentModel data) {
        mMyContentModel = data;
        mSharedPreferences.edit().putString(KEY_CONTENT_INFO, data == null ? null : mGson.toJson(data)).commit();
    }

    public void setBadgeInfo(BadgeModel data) {
        mBadgeModel = data;
        mSharedPreferences.edit().putString(KEY_BADGE_INFO, data == null ? null : mGson.toJson(data)).commit();
    }

    public void setNotifications(NotificationModel data) {
        mNotificationModel = data;
        mSharedPreferences.edit().putString(KEY_NOTIFICATIONS, data == null ? null : mGson.toJson(data)).commit();
    }

    public void clear() {
        setEncryptedAccessToken(null);
        setAccessToken(null);
        setUserId(null);
    }
    //endregion
}
