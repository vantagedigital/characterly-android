package com.mucaroo.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.AppActivity;
import com.mucaroo.application.AppDialog;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.UserInfoModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.ParentUpdate;
import static com.mucaroo.helper.NetworkHelper.StudentUpdate;

/**
 * Created by ahsan on 8/22/2017.
 */

public class ParentEditDialog extends AppDialog {

    @BindView(R.id.tv_name)
    CustomEditText mTvName;

    @BindView(R.id.tv_last)
    CustomEditText mTvLast;

    @BindView(R.id.tv_email)
    CustomEditText mTvEmail;

    @BindView(R.id.sp_parents)
    CustomSpinner mSpParents;

    private CustomProgressDialog pd;
    AppActivity context;

    public ParentEditDialog(@NonNull AppActivity context) {
        super(context);
        this.context = context;

        pd = new CustomProgressDialog(context);

        if (UserCache.getInstance().getUserInfo().result.user.student.parents != null && UserCache.getInstance().getUserInfo().result.user.student.parents.size() > 0) {
            ArrayList<String> parentFullNames = new ArrayList<>();
            for (UserInfoModel.Parent parent : UserCache.getInstance().getUserInfo().result.user.student.parents) {
                parentFullNames.add(parent.fullName);
            }
            mSpParents.setSpinner(parentFullNames);

            updateUI();

            mSpParents.setSelectedListener(new ListItemSelectedListener() {
                @Override
                public void onListItemSelected(int position, View view) {
                    updateUI();
                }
            });
        }
    }

    private void updateUI() {
        mTvEmail.getInput().setEnabled(false);
        mTvName.setText(UserCache.getInstance().getUserInfo().result.user.student.parents.get(mSpParents.getSelectedPosition()).name);
        mTvLast.setText(UserCache.getInstance().getUserInfo().result.user.student.parents.get(mSpParents.getSelectedPosition()).surname);
        mTvEmail.setText(UserCache.getInstance().getUserInfo().result.user.student.parents.get(mSpParents.getSelectedPosition()).emailAddress);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_parent_edit;
    }

    @OnClick(R.id.btn_cancel)
    protected void cancelTap() {
        onBackPressed();
    }

    @OnClick(R.id.btn_save)
    protected void saveTap() {
        String branchId = UserCache.getInstance().getUserInfo().result.user.branchId;
        String userId = UserCache.getInstance().getUserInfo().result.user.id;

        if (mTvName.validateLength(1, null) &&
                mTvLast.validateLength(1, null)) {

            Map<String, Object> map = new HashMap<>();
            map.put("BranchId", branchId);
            map.put("userId", userId);
            map.put("name", mTvName.getText());
            map.put("surname", mTvLast.getText());
            map.put("id", UserCache.getInstance().getUserInfo().result.user.student.parents.get(mSpParents.getSelectedPosition()).id);

            pd.show();
            context.networkHelper.putRequest(ParentUpdate, map, new Object(), new NetworkHelper.Listener<Object>() {
                @Override
                public void onResponse(Object model) {
                    pd.dismiss();
                    Common.showToast(context, "Successfully Updated Parents");
                    onBackPressed();
                }

                @Override
                public void onFailure(String msg) {
                    pd.dismiss();
                }
            });
        }

    }
}