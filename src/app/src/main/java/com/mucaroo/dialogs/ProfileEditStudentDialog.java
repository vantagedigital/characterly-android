package com.mucaroo.dialogs;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.application.AppDialog;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.FileUploadHelper;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.helper.UploadCategories;
import com.mucaroo.interfaces.CompletionListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.FileAttachmentModelSingle;
import com.mucaroo.models.GradeModel;
import com.mucaroo.models.Student;
import com.mucaroo.models.StudentModelSingle;
import com.mucaroo.models.UserInfoModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.GradeGetAll;
import static com.mucaroo.helper.NetworkHelper.StudentUpdate;

/**
 * Created by ahsan on 8/11/2017.
 */

public class ProfileEditStudentDialog extends AppDialog {

    @BindView(R.id.sp_grade)
    CustomSpinner sp_grade;

    @BindView(R.id.student_id)
    CustomEditText student_id;

    @BindView(R.id.name)
    CustomEditText name;
    @BindView(R.id.middle)
    CustomEditText middle;
    @BindView(R.id.last)
    CustomEditText last;
    @BindView(R.id.email)
    CustomEditText email;
    @BindView(R.id.profile_imageview)
    ImageView profile_imageview;
    @BindView(R.id.progress)
    ProgressBar progress;

    private CompletionListener completionListener;

    private transient final Gson mGson = new Gson();

    AppActivity context;
    GradeModel gradeModel;

    private View.OnClickListener mChoosePicturListener;

    private CustomProgressDialog pd;

    private ImagePicker imagePicker;

    private ImagePickerCallback imagePickerCallback;

    private Handler mHandler = new Handler();

    private Student mStudentData;
    private Classroom mClassroom;

    public ProfileEditStudentDialog(@NonNull AppActivity context, Classroom classroom, Student student) {
        super(context);
        this.context = context;

        imagePicker = new ImagePicker(this.context);

        mStudentData = student;
        mClassroom = classroom;

        if (classroom == null) {
            //add missing values
            updateStudentDataWithCache();
            try {
                mStudentData.userId = Integer.parseInt(UserCache.getInstance().getUserInfo().result.user.id);
            } catch (Exception e) {
            }


            sp_grade.setVisibility(View.GONE);
        } else {
            context.networkHelper.getRequest(GradeGetAll, new GradeModel(), new NetworkHelper.Listener() {
                @Override
                public void onResponse(Object object) {
                    if (object instanceof GradeModel) {
                        gradeModel = (GradeModel) object;

                        ArrayList<String> grades = new ArrayList<>();
                        for (int i = 0; i < gradeModel.result.items.size(); i++) {
                            GradeModel.Items items = gradeModel.result.items.get(i);
                            grades.add(items.name);
                        }
                        sp_grade.setSpinner(grades);
                    }
                }

                @Override
                public void onFailure(String msg) {

                }
            });
        }


//        String[] grades = {"First Grade", "Second Grade", "Third Grade", "Fourth Grade"};
//        sp_grade.setSpinner(grades);


        pd = new CustomProgressDialog(context);

        updateUI();

        updateProfilePic();

    }

    private void updateUI() {
        student_id.getInput().setEnabled(false);
        email.getInput().setEnabled(false);
        try {
            student_id.setText(mStudentData.userId + "");
        } catch (Exception e) {
        }
        try {
            email.setText(mStudentData.emailAddress);
        } catch (Exception e) {
        }
        try {
            name.setText(mStudentData.name);
            last.setText(mStudentData.surname);
        } catch (Exception e) {
        }
    }

    private void updateStudentDataWithCache() {
        mStudentData.branchId = UserCache.getInstance().getUserInfo().result.user.branchId;
        mStudentData.emailAddress = UserCache.getInstance().getUserInfo().result.user.emailAddress;
        mStudentData.name = UserCache.getInstance().getUserInfo().result.user.name;
        mStudentData.surname = UserCache.getInstance().getUserInfo().result.user.surname;
        mStudentData.imagePath = UserCache.getInstance().getUserInfo().result.user.imagePath;
    }

    private void updateProfilePic() {
        String path = mStudentData.imagePath;
        if (TextUtils.isEmpty(path)) {
            try {
                path = mStudentData.profilePicture.path;
            } catch (Exception e) {
            }
        }
        ImageLoaderUtil.loadImageIntoView(App.getInstance(), profile_imageview, path);
    }

    public void setCompletionListener(CompletionListener completionListener) {
        this.completionListener = completionListener;
    }

    @OnClick(R.id.btn_cancel)
    protected void cancelTap() {
        onBackPressed();
    }

    @OnClick(R.id.choose_picture_btn)
    protected void choosePicture() {
        imagePickerCallback = new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> images) {
                // Display images
                Log.e("PROFILEEDITSTUDENT", "images:" + images);
                if (images.size() > 0) {
                    ChosenImage image = images.get(0);
                    String imagePath = image.getOriginalPath();
                    String mimetype = image.getMimeType();

                    progress.setVisibility(View.VISIBLE);
                    // TODO fix orientation
//                    image.getOrientation();

                    FileUploadHelper.uploadFile(imagePath, mimetype, UploadCategories.UserProfile.getValue() + "", mStudentData.userId + "", new NetworkHelper.Listener<FileAttachmentModelSingle>() {
                        @Override
                        public void onResponse(final FileAttachmentModelSingle model) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        progress.setVisibility(View.GONE);
                                        if (mClassroom == null) {
                                            UserInfoModel userModel = UserCache.getInstance().getUserInfo();
                                            userModel.result.user.imagePath = model.result.path;
                                            mStudentData.imagePath = model.result.path;
                                            UserCache.getInstance().setCurrentUser(userModel);
                                            updateProfilePic();
                                        } else {
                                            mStudentData.imagePath = model.result.path;
                                            updateProfilePic();
                                        }
                                        Common.showToast("Successfully updated profile picture");
                                        if (completionListener != null) {
                                            completionListener.onCompleted();
                                        }
                                    } catch (Exception e) {
                                        Common.showToast("Error updating profile picture");
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailure(final String msg) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Common.showToast(msg);
                                    progress.setVisibility(View.GONE);
                                }
                            });
                        }
                    });

                }
            }

            @Override
            public void onError(String message) {
                // Do error handling
            }
        };

        imagePicker.setImagePickerCallback(imagePickerCallback);
// imagePicker.allowMultiple(); // Default is false
// imagePicker.shouldGenerateMetadata(false); // Default is true
// imagePicker.shouldGenerateThumbnails(false); // Default is true
        imagePicker.pickImage();
    }

    public ImagePickerCallback getImagePickerCallback() {
        return imagePickerCallback;
    }

    public void setImagePickerCallback(ImagePickerCallback imagePickerCallback) {
        this.imagePickerCallback = imagePickerCallback;
    }

    public ImagePicker getImagePicker() {
        return imagePicker;
    }

    public void setImagePicker(ImagePicker imagePicker) {
        this.imagePicker = imagePicker;
    }

    @OnClick(R.id.btn_save)
    protected void saveTap() {

        if (name.validateLength(2, "Name must be minimum 2 characters")
                && last.validateLength(2, "Name must be minimum 2 characters")
                && email.validateEmail()) {

            String branchId = mStudentData.branchId;
            String userId = mStudentData.userId + "";

            Map<String, Object> map = new HashMap<>();
            map.put("branchId", branchId);
            map.put("userId", userId);
            map.put("emailAddress", email.getText());
            map.put("name", name.getText());
            map.put("surname", last.getText());
            map.put("id", mStudentData.id);

            pd.show();
            context.networkHelper.putRequest(StudentUpdate, map, new StudentModelSingle(), new NetworkHelper.Listener<StudentModelSingle>() {
                @Override
                public void onResponse(StudentModelSingle model) {
                    pd.dismiss();
                    try {
                        if (mClassroom == null) {
                            UserInfoModel userInfoModel = UserCache.getInstance().getUserInfo();
                            userInfoModel.result.user.name = model.result.name;
                            userInfoModel.result.user.surname = model.result.surname;
                            if (!TextUtils.isEmpty(model.result.emailAddress)) {
                                userInfoModel.result.user.emailAddress = model.result.emailAddress;
                            }
                            userInfoModel.result.user.student.profilePicture = model.result.profilePicture;
                            userInfoModel.result.user.student.name = model.result.name;
                            userInfoModel.result.user.student.surname = model.result.surname;
                            if (!TextUtils.isEmpty(model.result.emailAddress)) {
                                userInfoModel.result.user.student.emailAddress = model.result.emailAddress;
                            }
                            try {
                                UserCache.getInstance().getUserInfo().result.user.imagePath = model.result.profilePicture.path;
                            } catch (Exception e) {
                            }

                            UserCache.getInstance().setCurrentUser(userInfoModel);
                            updateStudentDataWithCache();
                        } else {
                            mStudentData.name = model.result.name;
                            mStudentData.surname = model.result.surname;
                            try {
                                mStudentData.imagePath = model.result.profilePicture.path;
                            } catch (Exception e) {
                            }
                            if (!TextUtils.isEmpty(model.result.emailAddress)) {
                                mStudentData.emailAddress = model.result.emailAddress;
                            }
                        }
                    } catch (Exception e) {
                    }


                    Common.showToast(context, "Successfully Updated Profile");
                    onBackPressed();
                }

                @Override
                public void onFailure(String msg) {
                    pd.dismiss();
                }
            });
        }

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_profile_edit;
    }
}

