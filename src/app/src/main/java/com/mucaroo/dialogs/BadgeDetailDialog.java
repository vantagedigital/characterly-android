package com.mucaroo.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.AppDialog;
import com.mucaroo.models.Badge;

import butterknife.BindView;

/**
 * Created by ahsan on 8/11/2017.
 */

public class BadgeDetailDialog extends AppDialog {

    private Badge mBadge;

    @BindView(R.id.badge_title)
    TextView badge_title;

    @BindView(R.id.badge_desc)
    TextView badge_desc;

    public BadgeDetailDialog(@NonNull Context context) {
        super(context);

    }

    public void setBadge(Badge mBadge) {
        this.mBadge = mBadge;
        try {
            badge_title.setText(mBadge.pillar.name);
            badge_desc.setText(mBadge.pillar.description);
        } catch (Exception e) {

        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_badge_detail;
    }

}
