package com.mucaroo.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;
import android.view.WindowManager;

import com.mucaroo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ahsan on 8/20/2017.
 */

public class RegisterInfoDialog extends AppCompatDialog {

    @OnClick(R.id.btn_close)
    void close() {
        dismiss();
    }

    public RegisterInfoDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.dialog_info_register);
        ButterKnife.bind(this);
    }

}

