package com.mucaroo.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mucaroo.R;
import com.mucaroo.adapters.BadgeAdapter;
import com.mucaroo.application.AppDialog;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.models.Badge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by ahsan on 9/1/2017.
 */

public class BadgeAllDialog extends AppDialog {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private List<Badge> mBadges = new ArrayList<>();

    public BadgeAllDialog(@NonNull Context context) {
        super(context);
        initRecycler(context);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_badge_all;
    }

    private void initRecycler(Context context) {

        mBadges.addAll(UserCache.getInstance().getBadgeInfo().result);

        recycler_view.setLayoutManager(new GridLayoutManager(context, Common.getGridColumns(context)));
        recycler_view.setFocusable(false);
        BadgeAdapter adapter = new BadgeAdapter(context, mBadges);
        recycler_view.setAdapter(adapter);
    }
}
