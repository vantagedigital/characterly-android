package com.mucaroo.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.helper.Common;
import com.mucaroo.interfaces.OnDialogButtonSelectedListener;

import butterknife.ButterKnife;
import butterknife.BindView;

public class MessageDialogFragment extends BaseDialogFragment {

    public static final int RESULT_CODE_ONE_BUTTON = 0;

    public static final int RESULT_CODE_LEFT_BUTTON = 1;

    public static final int RESULT_CODE_RIGHT_BUTTON = 2;

    public static final String PARAMS_MESSAGE = "params_message";

    public static final String PARAMS_RIGHT_TEXT = "params_right_text";

    public static final String PARAMS_LEFT_TEXT = "params_left_text";

    public static final String PARAMS_TITLE_TEXT = "params_title_text";

    public static final String PARAMS_ONE_BUTTON = "params_one_button";

    @BindView(R.id.dialog_title)
    protected TextView mDialogTitle;

    @BindView(R.id.dialog_text_desc)
    protected TextView mDialogTextDesc;

    @BindView(R.id.dialog_right_button)
    protected TextView mDialogRightButton;

    @BindView(R.id.dialog_left_button)
    protected TextView mDialogLeftButton;

    private View mRootView;

    private boolean mOneButton = false;

    private String mRightText, mLeftText, mTitle, mMessage;

    private OnDialogButtonSelectedListener mOnDialogButtonSelectedListener;

    private DialogInterface.OnDismissListener mOnDismissListener;

    private Dialog mDialog;

    /**
     * Dialog with no buttons
     *
     * @param fragment    - Target Fragment
     * @param requestCode - Target Fragment's request code
     * @param title       - Dialog's Title
     * @param message     - Dialog's Message
     */
    public static MessageDialogFragment newInstance(Fragment fragment, int requestCode,
                                                    String title, String message) {
        return newInstance(fragment, requestCode, title, message, false);
    }

    /**
     * Dialog with one button, default text is "Ok"
     *
     * @param fragment    - Target Fragment
     * @param requestCode - Target Fragment's request code
     * @param title       - Dialog's Title
     * @param message     - Dialog's Message
     * @param oneButton   - Set to true is dialog should only present one button
     */
    public static MessageDialogFragment newInstance(Fragment fragment, int requestCode,
                                                    String title, String message, boolean oneButton) {
        return newInstance(fragment, requestCode, title, message, null, null, oneButton);
    }

    /**
     * Dialog with two buttons.
     *
     * @param fragment    - Target Fragment
     * @param requestCode - Target Fragment's request code
     * @param title       - Dialog's Title
     * @param message     - Dialog's Message
     * @param leftText    - Left button text
     * @param rightText   - Right button text
     */
    public static MessageDialogFragment newInstance(Fragment fragment, int requestCode,
                                                    String title, String message, String leftText, String rightText) {
        return newInstance(fragment, requestCode, title, message, leftText, rightText, false);
    }

    /**
     * @param fragment    - Target Fragment
     * @param requestCode - Target Fragment's request code
     * @param title       - Dialog's Title
     * @param message     - Dialog's Message
     * @param leftText    - Left button text
     * @param rightText   - Right button text
     * @param oneButton   - Set to true is dialog should only present one button
     */
    public static MessageDialogFragment newInstance(Fragment fragment, int requestCode,
                                                    String title, String message, String leftText, String rightText, boolean oneButton) {
        MessageDialogFragment frag = new MessageDialogFragment();
        Bundle args = new Bundle();
        args.putString(PARAMS_TITLE_TEXT, title);
        args.putString(PARAMS_MESSAGE, message);
        args.putString(PARAMS_LEFT_TEXT, leftText);
        args.putString(PARAMS_RIGHT_TEXT, rightText);
        args.putBoolean(PARAMS_ONE_BUTTON, oneButton);
        frag.setArguments(args);
        frag.setTargetFragment(fragment, requestCode);
        return frag;
    }

    /**
     * This method is meant to be used with Activities
     *
     * @param activity  - Target Activity
     * @param title     - Dialog's Title
     * @param message   - Dialog's Message
     * @param leftText  - Left button text
     * @param rightText - Right button text
     * @param listener  - OnDialogButtonSelectedListener
     */
    public static MessageDialogFragment newInstance(FragmentActivity activity,
                                                    String title, String message, String leftText, String rightText,
                                                    OnDialogButtonSelectedListener listener) {
        MessageDialogFragment frag = new MessageDialogFragment();
        Bundle args = new Bundle();
        args.putString(PARAMS_TITLE_TEXT, title);
        args.putString(PARAMS_MESSAGE, message);
        args.putString(PARAMS_LEFT_TEXT, leftText);
        args.putString(PARAMS_RIGHT_TEXT, rightText);
        frag.setArguments(args);
        frag.setTargetActivity(activity);
        frag.setOnDialogButtonSelectedListener(listener);
        return frag;
    }

    /**
     * @param title     - Dialog's Title
     * @param message   - Dialog's Message
     * @param leftText  - Left button text
     * @param rightText - Right button text
     * @param listener  - OnDialogButtonSelectedListener
     */
    public static MessageDialogFragment newInstance(Fragment fragment,
                                                    String title, String message, String leftText, String rightText,
                                                    OnDialogButtonSelectedListener listener) {
        MessageDialogFragment frag = new MessageDialogFragment();
        Bundle args = new Bundle();
        args.putString(PARAMS_TITLE_TEXT, title);
        args.putString(PARAMS_MESSAGE, message);
        args.putString(PARAMS_LEFT_TEXT, leftText);
        args.putString(PARAMS_RIGHT_TEXT, rightText);
        frag.setArguments(args);
        frag.setTargetFragment(fragment, 0);
        frag.setOnDialogButtonSelectedListener(listener);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        if (extras != null) {
            mTitle = extras.getString(PARAMS_TITLE_TEXT);
            mMessage = extras.getString(PARAMS_MESSAGE);
            mLeftText = extras.getString(PARAMS_LEFT_TEXT);
            mRightText = extras.getString(PARAMS_RIGHT_TEXT);
            mOneButton = extras.getBoolean(PARAMS_ONE_BUTTON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_message_dialog, container, false);

        ButterKnife.bind(this, mRootView);

        prepareDialog();

        return mRootView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mDialog = super.onCreateDialog(savedInstanceState);
        return mDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        mDialog.setOnDismissListener(mOnDismissListener);
    }

    public void setOnDialogButtonSelectedListener(
            OnDialogButtonSelectedListener onDialogButtonSelectedListener) {
        mOnDialogButtonSelectedListener = onDialogButtonSelectedListener;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    /**
     * Set text and edit views of the dialog
     */
    private void prepareDialog() {
        mDialogTitle.setText(mTitle);
        mDialogTextDesc.setText(mMessage);

        if (mOneButton) {
            mDialogLeftButton.setVisibility(View.GONE);
            mDialogRightButton.setText(getResources().getString(android.R.string.ok));
            mDialogRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.logOutput("One Button pressed");
                    dismiss();
                    getTargetFragment().onActivityResult(getTargetRequestCode(),
                            RESULT_CODE_ONE_BUTTON, null);
                }
            });
        } else {
            if (TextUtils.isEmpty(mLeftText)) {
                mDialogLeftButton.setVisibility(View.INVISIBLE);
            }
            if (TextUtils.isEmpty(mRightText)) {
                mDialogRightButton.setVisibility(View.INVISIBLE);
            }
            mDialogLeftButton.setText(mLeftText);
            mDialogRightButton.setText(mRightText);

            mDialogLeftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.logOutput("Left Button pressed");
                    dismiss();
                    if (mOnDialogButtonSelectedListener != null) {
                        mOnDialogButtonSelectedListener.leftButtonSelected();
                    } else {
                        if (getTargetFragment() != null) {
                            getTargetFragment().onActivityResult(getTargetRequestCode(),
                                    RESULT_CODE_LEFT_BUTTON, null);
                        }
                    }
                }
            });
            mDialogRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.logOutput("Right Button pressed");
                    dismiss();
                    if (mOnDialogButtonSelectedListener != null) {
                        mOnDialogButtonSelectedListener.rightButtonSelected();
                    } else {
                        if (getTargetFragment() != null) {
                            getTargetFragment().onActivityResult(getTargetRequestCode(),
                                    RESULT_CODE_RIGHT_BUTTON, null);
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (getTargetFragment() != null) {
            getTargetFragment()
                    .onActivityResult(getTargetRequestCode(), FragmentActivity.RESULT_CANCELED, null);
        }
        super.onDismiss(dialog);
    }

}



