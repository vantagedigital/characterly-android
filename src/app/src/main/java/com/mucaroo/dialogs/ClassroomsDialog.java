package com.mucaroo.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.mucaroo.R;
import com.mucaroo.activities.classroom.ClassHome;
import com.mucaroo.activities.classroom.Classrooms;
import com.mucaroo.adapters.BadgeAdapter;
import com.mucaroo.adapters.ClassroomsAdapter;
import com.mucaroo.application.AppDialog;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.CompletionStringListener;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Badge;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.mucaroo.helper.NetworkHelper.ClassroomGetAll;

public class ClassroomsDialog extends AppDialog {

    public NetworkHelper networkHelper;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    private ClassroomsAdapter mAdapter;

    private List<Classroom> mData = new ArrayList<>();

    private CompletionStringListener mListener;

    public ClassroomsDialog(@NonNull Context context) {
        super(context);
        networkHelper = new NetworkHelper(context);

        initRecycler(context);
        loadClassrooms();
    }

    private void loadClassrooms() {
        networkHelper.getRequest(ClassroomGetAll+"?SkipCount=0&MaxResultCount=100"
                , new ClassroomModel(), new NetworkHelper.Listener<ClassroomModel>() {
            @Override
            public void onResponse(ClassroomModel model) {
                mProgress.setVisibility(View.INVISIBLE);
                mData.clear();
                mData.addAll(model.result);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                Common.showToast(msg);
                mProgress.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void setListener(CompletionStringListener listener) {
        this.mListener = listener;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_classrooms;
    }

    private void initRecycler(Context context) {
        recycler_view.setLayoutManager(new LinearLayoutManager(context));
        recycler_view.setFocusable(false);
        Common.setRowDivider(context, recycler_view);

        mAdapter = new ClassroomsAdapter(context, mData);
        recycler_view.setAdapter(mAdapter);

        mAdapter.setItemSelectedListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                close();
                if (mListener != null) {
                    mListener.onCompleted(mData.get(position).id);
                }
            }
        });
    }
}
