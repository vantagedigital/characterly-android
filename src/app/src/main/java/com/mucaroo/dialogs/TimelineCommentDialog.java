package com.mucaroo.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.mucaroo.R;
import com.mucaroo.activities.classroom.ClassTimeline;
import com.mucaroo.adapters.TimelineCommentAdapter;
import com.mucaroo.application.AppDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.TimelineComment;
import com.mucaroo.models.TimelineCommentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.TimelineAddComment;

/**
 * Created by ahsan on 9/7/2017.
 */

public class TimelineCommentDialog extends AppDialog {

    private final String mClassroomId;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.comment_box)
    protected EditText mCommentBox;

    private TimelineComment mTimelineComment;

    private TimelineCommentAdapter adapter;

    private List<TimelineComment> mTimelineCommentList = new ArrayList<>();

    private CustomProgressDialog pd;

    public NetworkHelper networkHelper;

    public TimelineCommentDialog(@NonNull Context context, TimelineComment timelineComment, String classroomId) {
        super(context);

        pd = new CustomProgressDialog(context);
        networkHelper = new NetworkHelper(context);

        initRecycler(context);
        mTimelineComment = timelineComment;
        mClassroomId = classroomId;

        fetchComments();
    }

    private void fetchComments() {
        // TODO: fetch comments for timeline

//        It seems like the subcomments are also returned when I call
//
//        ## Timeline/GetTimelineComments
//        curl "http://betaapi.character.ly/api/services/app/Timeline/GetTimelineComments?timelineId=5df8a17a-ac63-4365-6772-08d5645cf8d4&SkipCount=0&MaxResultCount=30"
//        but they have a parentCommentId, so technically I can organize and strip out the subcomments for now. However the pagination will prevent this from working
//        properly so will hide button for now
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_comments;
    }

    private void initRecycler(Context context) {

        recycler_view.setLayoutManager(new LinearLayoutManager(context));
        recycler_view.setFocusable(false);
        adapter = new TimelineCommentAdapter(context, mTimelineCommentList);
        recycler_view.setAdapter(adapter);
    }

    @OnClick(R.id.send_button)
    protected void makeComment() {
        pd.setMessage("posting comment...");

        String comment = mCommentBox.getText().toString();

        Map<String, Object> map = new HashMap<>();
        map.put("TimelineId", mClassroomId);
        map.put("ParentCommentId", mTimelineComment.id);
        map.put("CommentText", comment);
        map.put("ContentScheduleId", "");
        map.put("EvaluationId", "");
        pd.show();

        networkHelper.postRequest(TimelineAddComment, map, new TimelineCommentModel(), new NetworkHelper.Listener<TimelineCommentModel>() {
            @Override
            public void onResponse(TimelineCommentModel model) {
                pd.dismiss();
                Common.showToast(getContext(), "Successfully added comment");
                mTimelineCommentList.add(model.result);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                pd.dismiss();
            }
        });

    }
}
