
package com.mucaroo.interfaces;

import android.view.View;

public interface ListItemSelectedListener {

    public void onListItemSelected(int position, View view);

}
