
package com.mucaroo.interfaces;

public interface CompletionStringListener {

    public void onCompleted(String value);

}
