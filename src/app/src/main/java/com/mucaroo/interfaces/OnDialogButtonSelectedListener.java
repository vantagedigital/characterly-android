package com.mucaroo.interfaces;

public interface OnDialogButtonSelectedListener {

    public void leftButtonSelected();

    public void rightButtonSelected();

}
