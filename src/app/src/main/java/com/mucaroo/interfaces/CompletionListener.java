
package com.mucaroo.interfaces;

import android.view.View;

public interface CompletionListener {

    public void onCompleted();

}
