package com.mucaroo.activities.lessons;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.ViewModel.LessonVM;
import com.mucaroo.activities.before.Login;
import com.mucaroo.activities.evaluation.EvaluationWizard;
import com.mucaroo.activities.search.Search;
import com.mucaroo.adapters.LessonAdapter;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomHeader;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.MyContentModel;
import com.mucaroo.models.MyContentResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.GetMyContent;

/**
 * Created by ahsan on 8/9/2017.
 */

public class LessonMain extends AppActivity implements NetworkHelper.Listener {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    LessonAdapter adapter;

    @BindView(R.id.no_content_text)
    protected TextView mNoContentText;

    @BindView(R.id.custom_header)
    CustomHeader mCustomHeader;

    @BindView(R.id.test_button)
    protected Button mTestButton;

    @BindView(R.id.datepicker)
    EditText datepicker;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

//    @Nullable
//    @BindView(R.id.expand_btn)
//    View mExpand;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.btn_load_more)
    Button mBtnLoadMore;

    private int currentPage = 0;

    Calendar myCalendar = Calendar.getInstance();
    private MyContentModel mData;
    private List<LessonVM> mViewModel = new ArrayList<>();

    public static Intent newIntent(Context context, MyContentModel data) {
        Intent intent = new Intent(context, LessonMain.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);
        if (mData == null) {
            mData = UserCache.getInstance().getContentInfo();
        }

        mCustomHeader.setSearchType(Search.SEARCH_TYPE_LESSONS);

        if (mData == null) {
            UserCache.getInstance().clear();
            Intent myIntent = new Intent(this, Login.class);
            startActivity(myIntent);
            return;
        }

        for (MyContentResult result : mData.result) {
            mViewModel.add(new LessonVM(result));
        }

        if (mViewModel.size() == 0) {
            mNoContentText.setVisibility(View.VISIBLE);
//            mTestButton.setVisibility(View.VISIBLE);
        } else {
            mBtnLoadMore.setVisibility(View.VISIBLE);
            currentPage = mViewModel.size() / NetworkHelper.MAX_RESULTS;
        }

        initRecycler1();
        initCalendar();

//        String imagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "Camera" + File.separator + "20180107_182133.jpg";
//        final File file = new File(imagePath);
//        if (file.exists()) {
//            Log.e("LESSON DET", "FILE EXISTS");
//        } else {
//            Log.e("LESSON DET", "FILE DOES NOT EXISTS");
//            Log.e("LESSON DET", "path:" + imagePath);
//
//        }

//        FileUploadHelper.uploadFile(imagePath, "image/jpg", UploadCategories.PillarThumbnail.getValue() + "", "ebe349c4-5a8c-40fc-5764-08d52cfe1f38", null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mData = UserCache.getInstance().getContentInfo();
        mViewModel.clear();
        for (MyContentResult result : mData.result) {
            mViewModel.add(new LessonVM(result));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_lesson_main;
    }

    void initRecycler1() {
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setFocusable(false);
        Common.setRowDivider(this, recycler_view);
        adapter = new LessonAdapter(this, mViewModel);
        recycler_view.setAdapter(adapter);
    }

    void initCalendar() {

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        datepicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(LessonMain.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

//    @Optional
//    @OnClick(R.id.expand_btn)
//    protected void expandButton() {
//        Toast.makeText(this, "Date not part of milestone 1", Toast.LENGTH_SHORT).show();
//    }

    @OnClick(R.id.btn_load_more)
    protected void loadMore() {
        mBtnLoadMore.setEnabled(false);
        mBtnLoadMore.setAlpha(0.5f);
        currentPage++;
        loadLessons();
    }

    private void loadLessons() {
        showLoader();
        String url = GetMyContent + "SkipCount=" + (currentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        networkHelper.getRequest(url, new MyContentModel(), this);
    }

    private void showLoader() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(Object object) {
        if (object instanceof MyContentModel) {
            MyContentModel model = (MyContentModel) object;

            for (MyContentResult result : model.result) {
                mViewModel.add(new LessonVM(result));
            }

            MyContentModel contentModel = UserCache.getInstance().getContentInfo();
            contentModel.result.addAll(model.result);
            UserCache.getInstance().setContentInfo(contentModel);

            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
            if (model.result.size() == NetworkHelper.MAX_RESULTS) {
                mBtnLoadMore.setVisibility(View.VISIBLE);
            } else {
                mBtnLoadMore.setVisibility(View.GONE);
            }

            hideLoader();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(String msg) {
        hideLoader();
    }

    @OnClick(R.id.test_button)
    protected void testButtonTap() {
        Evaluation eval = null;
        startActivity(EvaluationWizard.newIntent(this, eval));
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        datepicker.setText(sdf.format(myCalendar.getTime()));
    }

}
