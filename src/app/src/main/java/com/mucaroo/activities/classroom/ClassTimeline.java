package com.mucaroo.activities.classroom;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kbeanie.multipicker.api.AudioPicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.AudioPickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenAudio;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kbeanie.multipicker.api.entity.ChosenVideo;
import com.mucaroo.R;
import com.mucaroo.adapters.TimelineAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.FileUploadHelper;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.helper.UploadCategories;
import com.mucaroo.interfaces.CompletionListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomSingleModel;
import com.mucaroo.models.FileAttachment;
import com.mucaroo.models.FileAttachmentModelSingle;
import com.mucaroo.models.Student;
import com.mucaroo.models.TimelineComment;
import com.mucaroo.models.TimelineCommentModel;
import com.mucaroo.models.TimelineCommentParent;
import com.tolstykh.textviewrichdrawable.TextViewRichDrawable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.ClassroomGet;
import static com.mucaroo.helper.NetworkHelper.GetTimelineCommentsByClassId;
import static com.mucaroo.helper.NetworkHelper.TimelineAddComment;

/**
 */

public class ClassTimeline extends AppActivity {

    private static final String PARAMS_DATA = "PARAMS_DATA";
    private static final String PARAMS_CLASS_ID = "PARAMS_CLASS_ID";

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.class_code)
    protected TextView mClassCode;

    @BindView(R.id.class_name)
    protected TextView mClassName;

    @BindView(R.id.comment_text)
    protected EditText mCommentText;

    @BindView(R.id.content_container)
    protected View mContentContainer;

    @BindView(R.id.image_view)
    protected ImageView mImageView;

    @BindView(R.id.video_play_btn)
    protected View mVideoPlayBtn;

    @BindView(R.id.audio_btn)
    protected View mAudioBtn;

    @BindView(R.id.add_photo_button)
    protected View mAddPhotoButton;

    @BindView(R.id.add_file_button)
    protected View mAddFileButton;

    @BindViews({R.id.btn_notes, R.id.btn_audio, R.id.btn_video, R.id.btn_photo})
    TextViewRichDrawable[] post_buttons;

    @BindView(R.id.progress_base)
    ProgressBar mProgressBarBase;

    @BindView(R.id.btn_load_more)
    Button mBtnLoadMore;

    private Classroom mData;
    private String mClassId;
    private CustomProgressDialog pd;

    private List<TimelineComment> mTimelines = new ArrayList<>();
    private TimelineAdapter adapter;
    private int mSelectedButtonPos;

    private ImagePicker imagePicker;
    private ImagePickerCallback imagePickerCallback;

    private AudioPicker audioPicker;
    private AudioPickerCallback audioPickerCallback;

    private VideoPicker videoPicker;
    private VideoPickerCallback videoPickerCallback;

    private String currentFilePath;
    private String currentMimeType;

    private CompletionListener mPermissionCallback;

    private Handler mHandler = new Handler();

    private int currentPage = 0;

    public static Intent newIntent(Context context, Classroom data) {
        Intent intent = new Intent(context, ClassTimeline.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    public static Intent newIntent(Context context, String classId) {
        Intent intent = new Intent(context, ClassTimeline.class);
        intent.putExtra(PARAMS_CLASS_ID, classId);
        return intent;
    }

    @OnClick(R.id.btn_post)
    void post() {
        if (mSelectedButtonPos == 0) {
            //Comment
            postComment();
        } else if (mSelectedButtonPos == 1) {
            //Audio
            postFile();
        } else if (mSelectedButtonPos == 2) {
            //Video
            postFile();
        } else if (mSelectedButtonPos == 3) {
            //Photo
            postFile();
        }
    }

    void postFile() {
        pd.setMessage("posting content...");
        if (mContentContainer.getVisibility() != View.VISIBLE) {
            Common.showToast("Please attach content first.");
            return;
        }

        String commentText = "Audio Content";
        if (mSelectedButtonPos == 2) {
            //Video
            commentText = "Video Content";
        } else if (mSelectedButtonPos == 3) {
            //Photo
            commentText = "Photo Content";
        }

        //First post the comment to get id then attach file
        final List<String> attachmentIds = new ArrayList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("TimelineId", mClassId);
        map.put("ParentCommentId", "");//TODO: if commenting on comment
        map.put("CommentText", commentText);
        map.put("ContentScheduleId", "");
        map.put("EvaluationId", "");
        //TODO:
//        map.put("AttachmentIds", new JSONArray(attachmentIds));
        pd.show();

        networkHelper.postRequest(TimelineAddComment, map, new TimelineCommentModel(), new NetworkHelper.Listener<TimelineCommentModel>() {
            @Override
            public void onResponse(TimelineCommentModel model) {

                final TimelineComment timelineComment = model.result;
                FileUploadHelper.uploadFile(currentFilePath, currentMimeType, UploadCategories.TimelineComment.getValue() + "", timelineComment.id, new NetworkHelper.Listener<FileAttachmentModelSingle>() {
                    @Override
                    public void onResponse(final FileAttachmentModelSingle model) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                pd.hide();
                                try {
                                    // use local image for faster loading if just uploaded
                                    model.result.path = currentFilePath;
                                    List<FileAttachment> attachments = new ArrayList<>();
                                    attachments.add(model.result);
                                    timelineComment.attachments = attachments;

                                    mTimelines.add(timelineComment);
                                    adapter.notifyDataSetChanged();
                                    mContentContainer.setVisibility(View.GONE);
                                    Common.showToast(ClassTimeline.this, "Successfully added Content");
                                } catch (Exception e) {
                                    Common.showToast("Error adding content");
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(final String msg) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Common.showToast(msg);
                                pd.hide();
                            }
                        });
                    }
                });


            }

            @Override
            public void onFailure(String msg) {
                pd.dismiss();
            }
        });

    }

    void postComment() {
        if (TextUtils.isEmpty(mCommentText.getText().toString())) {
            Common.showToast("Comment cannot be empty.");
            return;
        }
        pd.setMessage("posting comment...");

        List<String> attachmentIds = new ArrayList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("TimelineId", mClassId);
        map.put("ParentCommentId", "");//TODO: if commenting on comment
        map.put("CommentText", mCommentText.getText().toString());
        map.put("ContentScheduleId", "");
        map.put("EvaluationId", "");
        //TODO:
//        map.put("AttachmentIds", new JSONArray(attachmentIds));
        pd.show();

        networkHelper.postRequest(TimelineAddComment, map, new TimelineCommentModel(), new NetworkHelper.Listener<TimelineCommentModel>() {
            @Override
            public void onResponse(TimelineCommentModel model) {
                pd.dismiss();
                Common.showToast(ClassTimeline.this, "Successfully added comment");
                mTimelines.add(model.result);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                pd.dismiss();
            }
        });

    }

    private void resetViews() {
        mCommentText.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.GONE);
        mImageView.setVisibility(View.GONE);
        mVideoPlayBtn.setVisibility(View.GONE);
        mAudioBtn.setVisibility(View.GONE);
        mAddPhotoButton.setVisibility(View.GONE);
        mAddFileButton.setVisibility(View.GONE);
    }

    @OnClick(R.id.add_file_button)
    protected void addFileTapCheck() {
        mPermissionCallback = new CompletionListener() {
            @Override
            public void onCompleted() {
                addFileTap();
            }
        };
        PermissionCheck();
    }

    protected void addFileTap() {
        if (mSelectedButtonPos == 1) {
            //Audio
            addAudioTap();
        } else if (mSelectedButtonPos == 2) {
            //Video
            addVideoTap();
        } else if (mSelectedButtonPos == 3) {
            //Photo
            addPhotoTap();
        }

    }

    @OnClick(R.id.add_photo_button)
    protected void addPhotoTapCheck() {
        mPermissionCallback = new CompletionListener() {
            @Override
            public void onCompleted() {
                addPhotoTap();
            }
        };
        PermissionCheck();
    }

    protected void addVideoTap() {
        videoPickerCallback = new VideoPickerCallback() {
            @Override
            public void onVideosChosen(List<ChosenVideo> list) {
                if (list.size() > 0) {
                    ChosenVideo video = list.get(0);
                    currentFilePath = video.getOriginalPath();
                    currentMimeType = video.getMimeType();

                    mContentContainer.setVisibility(View.VISIBLE);
                    mImageView.setVisibility(View.VISIBLE);
                    mVideoPlayBtn.setVisibility(View.VISIBLE);
                    mImageView.setBackgroundColor(Color.BLACK);
                    mVideoPlayBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // launch video with another application

                            if (!TextUtils.isEmpty(currentFilePath)) {
                                Uri uri = Uri.parse(currentFilePath);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                try {
                                    startActivity(intent);
                                } catch (Exception e) {
                                    Common.showToast("Error trying to play video link");
                                }
                            } else {
                                Common.showToast("No Video link available");
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(String s) {
                Log.e("ClassTimeline", "error Video:" + s);
            }
        };
        videoPicker.setVideoPickerCallback(videoPickerCallback);
// videoPicker.allowMultiple(); // Default is false
        videoPicker.pickVideo();
    }

    protected void addAudioTap() {
        audioPickerCallback = new AudioPickerCallback() {
            @Override
            public void onAudiosChosen(List<ChosenAudio> list) {
                if (list.size() > 0) {
                    ChosenAudio audio = list.get(0);
                    currentFilePath = audio.getOriginalPath();
                    currentMimeType = audio.getMimeType();

                    mContentContainer.setVisibility(View.VISIBLE);
                    mImageView.setVisibility(View.VISIBLE);
                    mAudioBtn.setVisibility(View.VISIBLE);
                    mImageView.setBackgroundColor(getResources().getColor(R.color.white_four));
                    mAudioBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // launch audio with another application

                            if (!TextUtils.isEmpty(currentFilePath)) {
                                Uri uri = Uri.parse(currentFilePath);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                try {
                                    startActivity(intent);
                                } catch (Exception e) {
                                    Common.showToast("Error trying to play audio link");
                                }
                            } else {
                                Common.showToast("No Audio link available");
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(String s) {

            }
        };
        audioPicker.setAudioPickerCallback(audioPickerCallback);
// audioPicker.allowMultiple(); // Default is false
        audioPicker.pickAudio();
    }

    protected void addPhotoTap() {
        imagePickerCallback = new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> images) {
                // Display images
                if (images.size() > 0) {
                    ChosenImage image = images.get(0);
                    currentFilePath = image.getOriginalPath();
                    currentMimeType = image.getMimeType();
//                    Log.e("TIMEE", "image.getDirectoryType():"+image.getDirectoryType());
//                    Log.e("TIMEE", "image.getOriginalPath():"+image.getOriginalPath());
//                    Log.e("TIMEE", "image.getThumbnailPath():"+image.getThumbnailPath());
//                    Log.e("TIMEE", "image.getDisplayName():"+image.getDisplayName());
//                    Log.e("TIMEE", "image.getTempFile():"+image.getTempFile());
//                    Log.e("TIMEE", "image.getExtension():"+image.getExtension());
//                    Log.e("TIMEE", "image.getQueryUri():"+image.getQueryUri());

                    mContentContainer.setVisibility(View.VISIBLE);
                    mImageView.setVisibility(View.VISIBLE);
                    ImageLoaderUtil.loadImageIntoView(App.getInstance(), mImageView, currentFilePath);
                }
            }

            @Override
            public void onError(String message) {
                // Do error handling
            }
        };

        imagePicker.setImagePickerCallback(imagePickerCallback);
// imagePicker.allowMultiple(); // Default is false
// imagePicker.shouldGenerateMetadata(false); // Default is true
// imagePicker.shouldGenerateThumbnails(false); // Default is true
        imagePicker.pickImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(imagePickerCallback);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_AUDIO) {
                if (audioPicker == null) {
                    audioPicker = new AudioPicker(this);
                    audioPicker.setAudioPickerCallback(audioPickerCallback);
                }
                audioPicker.submit(data);
            } else if (requestCode == Picker.PICK_VIDEO_DEVICE) {
                if (videoPicker == null) {
                    videoPicker = new VideoPicker(this);
                    videoPicker.setVideoPickerCallback(videoPickerCallback);
                }
                videoPicker.submit(data);
            }
        }
    }

    private void PermissionCheck() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            mPermissionCallback.onCompleted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPermissionCallback.onCompleted();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @OnClick(R.id.btn_notes)
    void btn_notes(TextViewRichDrawable btn) {
        selectPostButton(btn);
        resetViews();
        mCommentText.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_audio)
    void btn_audio(TextViewRichDrawable btn) {
        selectPostButton(btn);
        resetViews();
        mAddFileButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_video)
    void btn_video(TextViewRichDrawable btn) {
        selectPostButton(btn);
        resetViews();
        mAddFileButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_photo)
    void btn_photo(TextViewRichDrawable btn) {
        selectPostButton(btn);
        resetViews();
//        mAddFileButton.setVisibility(View.VISIBLE);
        mAddPhotoButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);
        mClassId = getIntent().getStringExtra(PARAMS_CLASS_ID);

        if (mData != null) {
            mClassId = mData.id;
        } else {
            loadClassroom();
        }

        pd = new CustomProgressDialog(this);

        imagePicker = new ImagePicker(this);
        audioPicker = new AudioPicker(this);
        videoPicker = new VideoPicker(this);

        updateUI();

        initRecycler();

        getTimelineComments();

    }

    private void loadClassroom() {
        try {
            String url = ClassroomGet + mClassId;
            Log.e("TIME", "url:" + url);
            networkHelper.getRequest(url, new ClassroomSingleModel(), new NetworkHelper.Listener<ClassroomSingleModel>() {
                @Override
                public void onResponse(ClassroomSingleModel model) {
                    mData = model.result;
                    updateUI();
                }

                @Override
                public void onFailure(String msg) {
                }
            });
        } catch (Exception e) {

        }
    }

    private void updateUI() {
        if (mData != null) {
            mClassCode.setText(mData.classCode);
            mClassName.setText(mData.name);
        }
    }

    private void showLoader() {
        mProgressBarBase.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBarBase.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_load_more)
    protected void loadMore() {
        mBtnLoadMore.setEnabled(false);
        mBtnLoadMore.setAlpha(0.5f);
        currentPage++;
        showLoader();
        getTimelineComments();
    }

    private void getTimelineComments() {
        String url = GetTimelineCommentsByClassId + mClassId;
        url = url + "+&SkipCount=" + (currentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        networkHelper.getRequest(url, new TimelineCommentParent(), new NetworkHelper.Listener<TimelineCommentParent>() {
            @Override
            public void onResponse(TimelineCommentParent model) {
                if (model.result.items != null) {
                    mTimelines.addAll(model.result.items);

                    adapter.notifyDataSetChanged();
                }

                mBtnLoadMore.setEnabled(true);
                mBtnLoadMore.setAlpha(1.0f);
                if (model.result.items != null && model.result.items.size() == NetworkHelper.MAX_RESULTS) {
                    mBtnLoadMore.setVisibility(View.VISIBLE);
                } else {
                    mBtnLoadMore.setVisibility(View.GONE);
                }

                hideLoader();

                if (mTimelines.size() == 0) {
                    Common.showToast("No timeline comments found for this classroom");
                }
            }

            @Override
            public void onFailure(String msg) {
                if (mProgressBarBase.getVisibility() == View.VISIBLE) {
                    Common.showToast(msg);
                    mBtnLoadMore.setEnabled(true);
                    mBtnLoadMore.setAlpha(1.0f);
                }
                hideLoader();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classroom_timeline;
    }

    void selectPostButton(TextViewRichDrawable button) {
        for (int i = 0; i < post_buttons.length; i++) {
            TextViewRichDrawable btn = post_buttons[i];
            if (btn == button) {
                mSelectedButtonPos = i;
            }
            btn.setBackgroundResource(btn == button ? R.drawable.box_blue : R.drawable.box_white);
        }
    }

    void initRecycler() {
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setFocusable(false);
        adapter = new TimelineAdapter(this, mTimelines);
        if (mData != null) {
            adapter.setClassroomId(mClassId);
        }
        recycler_view.setAdapter(adapter);
    }

}
