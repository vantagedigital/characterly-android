package com.mucaroo.activities.evaluation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.adapters.EvaluationAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.dialogs.ClassroomsDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomCheckbox;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.CompletionStringListener;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.Question;
import com.mucaroo.models.QuestionOption;
import com.mucaroo.models.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.EvaluationAssignToClassroom;

/**
 * Created by ahsan on 9/6/2017.
 */

public class EvaluationReviewEdu extends AppActivity {

    private static final String PARAMS_EVALUATION = "PARAMS_EVALUATION";

    private Evaluation mEvaluation;

    @BindView(R.id.header)
    TextView mHeader;

    @BindView(R.id.class_path)
    TextView mClassPath;

    @BindView(R.id.evaluation_id)
    TextView mEvaluationId;

    @BindView(R.id.evaluation_title)
    TextView mEvaluationTitle;

    @BindView(R.id.lay_questions)
    LinearLayout mLayQuestions;

    private CustomProgressDialog pd;

    private int singleSelection;

    public static Intent newIntent(Context context, Evaluation evaluation) {
        Intent intent = new Intent(context, EvaluationReviewEdu.class);
        intent.putExtra(PARAMS_EVALUATION, evaluation);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mEvaluation = getIntent().getParcelableExtra(PARAMS_EVALUATION);

        pd = new CustomProgressDialog(this);

        mClassPath.setText("Evaluations / " + mEvaluation.name);

        mEvaluationId.setText("Evaluation: " + mEvaluation.id);
        mEvaluationTitle.setText(mEvaluation.name);

        populateQuestions();

    }

    private void populateQuestions() {
        if (mEvaluation == null) {
            return;
        }
        for (int i = 0; i < mEvaluation.questions.size(); i++) {
            populateQuestion(i);
        }
    }

    private void populateQuestion(int i) {
        Question question = mEvaluation.questions.get(i);
        final View view = View.inflate(this, R.layout.item_evalutation_question_header, null);
        final TextView cbNumber = view.findViewById(R.id.cb_number);
        final TextView cbQuestion = view.findViewById(R.id.cb_question);

        cbNumber.setText((i + 1) + ".");
        cbQuestion.setText(question.description);

        mLayQuestions.addView(view);

        configureForType(question);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_evaluation;
    }


    private void configureForType(Question question) {
        int type = question.questionType;
        switch (type) {
            case 0: {//OpenQuestion
                final View view = View.inflate(this, R.layout.item_evalutation_edittext, null);
                final EditText open_edittext = view.findViewById(R.id.open_edittext);
                mLayQuestions.addView(view);

                break;
            }
            case 1: {//TrueFalse
                final View view = View.inflate(this, R.layout.item_evalutation_edittext, null);
                final EditText open_edittext = view.findViewById(R.id.open_edittext);
                mLayQuestions.addView(view);
                //TODO:? need design
                break;
            }
            case 2://SingleChoice
                List<QuestionOption> data = new ArrayList<>();
                for (QuestionOption option : question.questionOptions) {
                    data.add(option);
                }
                final LinearLayout subView = new LinearLayout(this);
                subView.setOrientation(LinearLayout.VERTICAL);
                Common.addViewFromLayout(this, R.layout.item_evalutation_radio, subView, data, new ListItemSelectedListener() {
                    @Override
                    public void onListItemSelected(int position, View view) {
                        // Reset views
                        for (int i = 0; i < subView.getChildCount(); i++) {
                            View child = subView.getChildAt(i);
                            View container = child.findViewById(R.id.container);
                            Button cbButton = child.findViewById(R.id.btn_cb);
                            TextView cbText = child.findViewById(R.id.cb_text);
                            container.setBackgroundColor(getResources().getColor(R.color.white));
                            cbButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.box_white));
                            cbText.setTextColor(getResources().getColor(R.color.charcoal_grey));
                        }

                        View container = view.findViewById(R.id.container);
                        container.setBackgroundColor(getResources().getColor(R.color.off_blue));
                        Button cbButton = view.findViewById(R.id.btn_cb);
                        TextView cbText = view.findViewById(R.id.cb_text);
                        cbButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.box_blue));
                        cbText.setTextColor(getResources().getColor(R.color.white));
                        singleSelection = position;
                    }
                });
                mLayQuestions.addView(subView);
                break;
            case 3://MultipleChoice
                List<QuestionOption> data1 = new ArrayList<>();
                for (QuestionOption option : question.questionOptions) {
                    data1.add(option);
                }
                Common.addViewFromLayout(this, R.layout.item_evalutation_checkbox, mLayQuestions, data1, null);
                break;
            case 4://SingleImageChoice
                final View view2 = View.inflate(this, R.layout.item_evalutation_image_edittext, null);
                final CustomEditText open_img_edittext = view2.findViewById(R.id.open_img_edittext);
                final ImageView eva_imageview = view2.findViewById(R.id.eva_imageview);
                if (question.fileAttachment != null) {
                    ImageLoaderUtil.loadImageIntoView(App.getInstance(), eva_imageview, question.fileAttachment.path);
                }
                mLayQuestions.addView(view2);
                break;
            case 5://MultipleImageChoice
                List<QuestionOption> data2 = new ArrayList<>();
                for (QuestionOption option : question.questionOptions) {
                    data2.add(option);
                }
                Common.addViewFromLayout(this, R.layout.item_evalutation_img_checkbox, mLayQuestions, data2, null);
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.assign_to_classroom_button)
    protected void assignToClassroomTap() {
        ClassroomsDialog dialog = new ClassroomsDialog(this);
        dialog.setListener(new CompletionStringListener() {
            @Override
            public void onCompleted(String value) {
                pd.show();
                String evaluationParam = "evaluationId=" + mEvaluation.id;
                String classroomParam = "classroomId=" + value;
                Map<String, Object> map = new HashMap<>();
                networkHelper.postRequest(EvaluationAssignToClassroom + evaluationParam + "&" + classroomParam, map, new Object(), new NetworkHelper.Listener<Object>() {
                    @Override
                    public void onResponse(Object model) {
                        pd.dismiss();
                        Common.showToast("Successfully assigned evaluation to classroom");
                    }

                    @Override
                    public void onFailure(String msg) {
                        pd.dismiss();
                        if (TextUtils.isEmpty(msg)) {
                            msg = "Error assigning evaluation to classroom";
                        }
                        Common.showToast(msg);
                    }
                });
            }
        });
        dialog.show();
    }
}
