package com.mucaroo.activities.before;

import android.os.Bundle;

import com.mucaroo.R;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomMessage;
import com.mucaroo.helper.NetworkHelper;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.RecoverAccessCode;

/**
 * Created by ahsan on 8/12/2017.
 */

public class ChangePassword extends AppActivity {

    @BindView(R.id.layout_msg)
    CustomMessage layout_msg;

    @BindView(R.id.old_password)
    CustomEditText old_password;

    @BindView(R.id.password)
    CustomEditText password;

    @BindView(R.id.r_password)
    CustomEditText r_password;


    @OnClick(R.id.btn_change)
    void change() {



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_change_password;
    }
}