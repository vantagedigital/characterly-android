package com.mucaroo.activities.before;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.mucaroo.R;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomMessage;
import com.mucaroo.helper.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.ResetPasswordRequest;

/**
 * Created by Vantage on 8/1/2017.
 */

public class RecoverPassword extends AppActivity {

    @BindView(R.id.layout_msg)
    CustomMessage layout_msg;

    @BindView(R.id.email)
    CustomEditText email;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    @OnClick(R.id.btn_enter)
    void enter() {

        if (email.validateEmail()) {
            Map<String, Object> map = new HashMap<>();
            map.put("emailAddress", email.getText().trim());

            mProgress.setVisibility(View.VISIBLE);
            networkHelper.postRequest(ResetPasswordRequest, map, new Object(), new NetworkHelper.Listener<Object>() {
                @Override
                public void onResponse(Object model) {
                    mProgress.setVisibility(View.GONE);
                    startActivity(ForgotPasswordNewPass.newIntent(RecoverPassword.this, email.getText().trim()));
                    Common.showToast(RecoverPassword.this, "Successfully Recovered");
                }
                @Override
                public void onFailure(String msg) {
                    mProgress.setVisibility(View.GONE);
                }
            });

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recover_password;
    }
}