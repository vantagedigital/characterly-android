package com.mucaroo.activities.before;

import android.os.Bundle;
import android.os.Handler;

import com.mucaroo.R;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;

/**
 * Created by ahsan on 8/14/2017.
 */

public class Splash extends AppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int SPLASH_TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (UserCache.getInstance().isLoggedIn()) {
                    Common.goTo(Splash.this, LessonMain.class);
                } else {
                    Common.goTo(Splash.this, Login.class);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

}