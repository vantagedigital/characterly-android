package com.mucaroo.activities.classroom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.mucaroo.R;
import com.mucaroo.adapters.StudentAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.MessageDialogFragment;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomRadioButton;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.FileUploadHelper;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.helper.UploadCategories;
import com.mucaroo.interfaces.OnDialogButtonSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomSingleModel;
import com.mucaroo.models.FileAttachmentModelSingle;
import com.mucaroo.models.GradeModel;
import com.mucaroo.models.Student;
import com.mucaroo.models.StudentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.mucaroo.helper.NetworkHelper.ClassroomCreate;
import static com.mucaroo.helper.NetworkHelper.ClassroomDelete;
import static com.mucaroo.helper.NetworkHelper.ClassroomGet;
import static com.mucaroo.helper.NetworkHelper.ClassroomUpdate;
import static com.mucaroo.helper.NetworkHelper.GradeGetAll;
import static com.mucaroo.helper.NetworkHelper.StudentGetAll;
import static com.mucaroo.helper.NetworkHelper.StudentSearch;

/**
 * Created by ahsan on 9/6/2017.
 */

public class ClassAdd extends AppActivity implements NetworkHelper.Listener {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.full_name_text)
    protected TextView mFullNameText;

    @BindView(R.id.search)
    CustomEditText mSearch;

    @BindView(R.id.sort)
    CustomSpinner sort;

    @BindView(R.id.color_tabs)
    CustomRadioButton color_tabs;

    @BindView(R.id.sp_grade)
    CustomSpinner sp_grade;

    @BindView(R.id.et_name)
    CustomEditText et_name;

    @BindView(R.id.et_class_code)
    CustomEditText et_class_code;

    @BindView(R.id.btn_add_class)
    Button mBtnAddClass;

    @BindView(R.id.btn_cancel)
    Button mBtnCancel;

    @BindView(R.id.customize_header_text)
    View customize_header_text;

    @BindView(R.id.header)
    View header;

    @BindView(R.id.pillar_selected)
    CircleImageView pillar_selected;

    @BindView(R.id.pillar_1)
    CircleImageView pillar_1;

    @BindView(R.id.pillar_2)
    CircleImageView pillar_2;

    @BindView(R.id.pillar_3)
    CircleImageView pillar_3;

    @BindView(R.id.pillar_4)
    CircleImageView pillar_4;

    @BindView(R.id.pillar_5)
    CircleImageView pillar_5;

    @BindView(R.id.pillar_6)
    CircleImageView pillar_6;

    @BindView(R.id.pillar_7)
    CircleImageView pillar_7;

    @BindView(R.id.pillar_8)
    CircleImageView pillar_8;

    @BindView(R.id.pillar_9)
    CircleImageView pillar_9;

    @BindViews({R.id.lay_image, R.id.lay_colors})
    LinearLayout[] lay_content;

    @BindView(R.id.profile_imageview)
    protected ImageView mProfileImageView;

    GradeModel gradeModel;
    StudentAdapter adapter;

    private String colorCode = "#e2343c";

    protected List<Student> mStudents = new ArrayList<>();
    private String mSearchedValue;
    private CustomProgressDialog pd, imagePd;
    private Classroom mData;
    private int mCurrentGrade;

    private ClassroomSingleModel classroomSingleModel;

    private ImagePicker imagePicker;

    private ImagePickerCallback imagePickerCallback;

    private Handler mHandler = new Handler();

    private boolean mIsEducator;

    public static Intent newIntent(Context context, Classroom data) {
        Intent intent = new Intent(context, ClassAdd.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    @OnClick(R.id.btn_add_class)
    void btn_add_class() {
//&&
//        et_class_code.validateLength(1, null)
        if (et_name.validateLength(1, null) ) {


            String branchId ="49d36102-2910-4a31-9487-08d5620dfaa0"; //UserCache.getInstance().getUserInfo().result.user.educator.branchId;//mData.educator.branchId;
            String educatorId = UserCache.getInstance().getUserInfo().result.user.educator.id;//mData.educator.id;
            String gradeId = gradeModel.result.items.get(sp_grade.getSelectedPosition()).id;

            Map<String, Object> map = new HashMap<>();
            map.put("BranchId", branchId);
            map.put("EducatorId", educatorId);
//            map.put("SchoolId", null);
            map.put("Name", et_name.getText());
            map.put("ColorCode", colorCode);
            map.put("ClassCode", et_class_code.getText());
            map.put("GradeId", gradeId);
            map.put("Students", adapter.getSelectedIds());

            pd.show();
            if (mData != null) {
                map.put("id", mData.id);
                networkHelper.putRequest(ClassroomCreate, map, new Object(), new NetworkHelper.Listener<Object>() {
                    @Override
                    public void onResponse(Object model) {
                        pd.dismiss();
                        Common.showToast(ClassAdd.this, "Successfully Updated Class");
                        onBackPressed();
                    }

                    @Override
                    public void onFailure(String msg) {
                        pd.dismiss();
                    }
                });
            } else {
                networkHelper.postRequest(ClassroomCreate, map, new Object(), new NetworkHelper.Listener<Object>() {
                    @Override
                    public void onResponse(Object model) {
                        pd.dismiss();
                        Common.showToast(ClassAdd.this, "Successfully Added Class");
                        onBackPressed();
                    }

                    @Override
                    public void onFailure(String msg) {
                        pd.dismiss();
                    }
                });
            }
        }
    }

    @OnClick(R.id.btn_cancel)
    void btn_cancel() {
        if (mData == null) {
            onBackPressed();
        } else {
            // show confirmation dialog
            MessageDialogFragment dialogFragment = MessageDialogFragment.newInstance(this, "Delete Class", "Are you sure you want to delete this classroom?", "Cancel", "Yes", new OnDialogButtonSelectedListener() {
                @Override
                public void leftButtonSelected() {
                }

                @Override
                public void rightButtonSelected() {
                    deleteClassroom();
                }
            });
            dialogFragment.show();

        }
    }

    private void deleteClassroom() {
        pd.show();
        networkHelper.deleteRequest(ClassroomDelete + mData.id, new Object(), new NetworkHelper.Listener<Object>() {
            @Override
            public void onResponse(Object model) {
                pd.dismiss();
                Common.showToast(ClassAdd.this, "Successfully Deleted: " + mData.name);
                onBackPressed();
            }

            @Override
            public void onFailure(String msg) {
                Common.showToast(ClassAdd.this, "Error deleting: " + mData.name);
                pd.dismiss();
            }
        });
    }

    @OnClick(R.id.btn_add_student)
    void btn_add_student() {
        startActivity(ClassAddStudent.class);
    }

    @OnClick(R.id.btn_invite)
    void btn_invite() {
        startActivity(ClassInvite.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);

        mIsEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;

        imagePicker = new ImagePicker(this);

        setColorListeners();

        if (mData != null) {
            mBtnCancel.setText("Delete");
            if (!mIsEducator) {
                mBtnCancel.setVisibility(View.GONE);
            }
            mBtnAddClass.setText("Save Changes");
            mBtnCancel.setTextColor(getResources().getColor(R.color.white));
            mBtnCancel.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_tomato_two));
            et_class_code.setText(mData.classCode);
            et_name.setText(mData.name);
            customize_header_text.setVisibility(View.VISIBLE);
            header.setVisibility(View.VISIBLE);
        } else {
            customize_header_text.setVisibility(View.GONE);
            header.setVisibility(View.GONE);
            et_class_code.setVisibility(View.GONE);
        }

        color_tabs.setupVisibility(lay_content);

        pd = new CustomProgressDialog(this);
        imagePd = new CustomProgressDialog(this);
        imagePd.setMessage("Uploading image...");

        try {
            mFullNameText.setText(mData.educator.name + " " + mData.educator.surname);
        } catch (Exception e) {
        }

        try {
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mProfileImageView, mData.educator.profilePicture.path);
        } catch (Exception e) {
        }

        ArrayList<String> sortOrders = new ArrayList<>();
        sortOrders.add("Descending");
        sortOrders.add("Ascending");
        sort.setSpinner(sortOrders);

        initRecycler();

        mSearch.getInput().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_NEXT) {
                    searchStudent(textView.getText().toString());
                    return true;
                }
                return false;
            }
        });

    }

    private void setColorListeners() {
        pillar_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#e2343c");
            }
        });
        pillar_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#ffbf00");
            }
        });
        pillar_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#ffe300");
            }
        });
        pillar_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#92be76");
            }
        });
        pillar_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#0c4599");
            }
        });
        pillar_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#924094");
            }
        });
        pillar_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#ff669a");
            }
        });
        pillar_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#000000");
            }
        });
        pillar_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSelectedPillar("#ffffff");
            }
        });
    }

    private void updateSelectedPillar(String colorCode) {
        this.colorCode = colorCode;
        try {
            pillar_selected.setImageDrawable(new ColorDrawable(Color.parseColor(colorCode)));
            if (colorCode.contains("ffffff")) {
                pillar_selected.setBorderColor(Color.BLACK);
                pillar_selected.setBorderWidth(getResources().getDimensionPixelOffset(R.dimen.dp_05));
            } else {
                pillar_selected.setBorderColor(Color.WHITE);
                pillar_selected.setBorderWidth(getResources().getDimensionPixelOffset(R.dimen.dp_0));
            }
        } catch (Exception e) {

        }
    }

    private void searchStudent(String value) {
        pd.show();
        mSearchedValue = value;

        Map<String, Object> map = new HashMap<>();
        map.put("searchText", value);
        map.put("sortOrder", String.valueOf(sort.getSelectedPosition()));
        map.put("maxResultCount", "20");
        map.put("skipCount", "0");
        networkHelper.postRequest(StudentSearch, map, new StudentModel(), this);
        hideKeyboard();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classroom_add_class;
    }

    void initRecycler() {

        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setFocusable(false);

        adapter = new StudentAdapter(ClassAdd.this, mStudents);
        recycler_view.setAdapter(adapter);


        getClassroomStudents();

        networkHelper.getRequest(GradeGetAll, new GradeModel(), this);
        networkHelper.getRequest(StudentGetAll, new StudentModel(), this);

    }

    @OnClick(R.id.btn_upload)
    protected void choosePicture() {
        if (mData == null) {
            Common.showToast("Cannot upload image while creating class");
            return;
        }
        imagePickerCallback = new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> images) {
                // Display images
                if (images.size() > 0) {
                    ChosenImage image = images.get(0);
                    String imagePath = image.getOriginalPath();
                    String mimetype = image.getMimeType();

                    imagePd.show();
                    // TODO fix orientation
//                    image.getOrientation();

                    FileUploadHelper.uploadFile(imagePath, mimetype, UploadCategories.ClassroomHeader.getValue() + "", mData.id, new NetworkHelper.Listener<FileAttachmentModelSingle>() {
                        @Override
                        public void onResponse(final FileAttachmentModelSingle model) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        imagePd.hide();
                                        mData.imagePath = model.result.path;
                                        Common.showToast("Successfully uploaded classroom image");
                                    } catch (Exception e) {
                                        Common.showToast("Error uploading classroom image");
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailure(final String msg) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Common.showToast(msg);
                                    imagePd.hide();
                                }
                            });
                        }
                    });

                }
            }

            @Override
            public void onError(String message) {
                // Do error handling
            }
        };

        imagePicker.setImagePickerCallback(imagePickerCallback);
// imagePicker.allowMultiple(); // Default is false
// imagePicker.shouldGenerateMetadata(false); // Default is true
// imagePicker.shouldGenerateThumbnails(false); // Default is true
        imagePicker.pickImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(imagePickerCallback);
                }
                imagePicker.submit(data);
            }
        }
    }

    private void getClassroomStudents() {
        if (mData != null) {
            try {
                networkHelper.getRequest(ClassroomGet + mData.id, new ClassroomSingleModel(), new NetworkHelper.Listener<ClassroomSingleModel>() {
                    @Override
                    public void onResponse(ClassroomSingleModel model) {
                        classroomSingleModel = model;
                        ArrayList<String> ids = new ArrayList<>();
                        for (Student items : model.result.students) {
                            ids.add(items.id);
                        }
                        adapter.addToSelectedIds(ids);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(String msg) {
//                        Common.showToast("error getting classroom students");
                    }
                });
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onResponse(Object object) {

        if (object instanceof GradeModel) {
            gradeModel = (GradeModel) object;

            ArrayList<String> grades = new ArrayList<>();
            for (int i = 0; i < gradeModel.result.items.size(); i++) {
                GradeModel.Items items = gradeModel.result.items.get(i);
                grades.add(items.name);
                if (mData != null && items.id.equalsIgnoreCase(mData.gradeId)) {
                    mCurrentGrade = i;
                }
            }
            sp_grade.setSpinner(grades);
            sp_grade.getSpinner().setSelection(mCurrentGrade);

        } else if (object instanceof StudentModel) {
            StudentModel studentModel = (StudentModel) object;
            if (studentModel.result.size() == 0 && mSearchedValue != null) {
                Common.showToast("No results found for: " + mSearchedValue);
            }

            mStudents.clear();
            mStudents.addAll(studentModel.result);
            pd.dismiss();
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure(String msg) {
        pd.dismiss();
    }
}
