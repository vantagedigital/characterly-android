package com.mucaroo.activities.before;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomMessage;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Vantage on 8/3/2017.
 */

public class ForgotPasswordNewPass extends AppActivity {

    private static final String PARAMS_EMAIL = "PARAMS_EMAIL";

    @BindView(R.id.subtitle)
    TextView mSubtitle;

    @BindView(R.id.layout_msg)
    CustomMessage layout_msg;

    private String mEmail;

    public static Intent newIntent(Context context, String email) {
        Intent intent = new Intent(context, ForgotPasswordNewPass.class);
        intent.putExtra(PARAMS_EMAIL, email);
        return intent;
    }

    @OnClick(R.id.btn_change)
    void lesson() {
// TODO: change password API
        Intent myIntent = new Intent(this, LessonMain.class);
        startActivity(myIntent);

        Common.showToast("Need API for this");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEmail = getIntent().getStringExtra(PARAMS_EMAIL);

        mSubtitle.setText("Please enter a new password for your " + mEmail + " account.");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_forgot_password_new_pass;
    }
}