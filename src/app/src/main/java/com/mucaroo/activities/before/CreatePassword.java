package com.mucaroo.activities.before;

import android.content.Intent;
import android.os.Bundle;

import com.mucaroo.R;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomMessage;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.ResetPasswordModel;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.CreateAccount;
import static com.mucaroo.helper.NetworkHelper.ResetPassword;

/**
 * Created by Vantage on 8/3/2017.
 */

public class CreatePassword extends AppActivity {

    @BindView(R.id.layout_msg)
    CustomMessage layout_msg;

    @BindView(R.id.password)
    protected CustomEditText etPassword;


    @BindView(R.id.r_password)
    protected CustomEditText retypePassword;


    String token;
    String email;

    @OnClick(R.id.btn_change)
    void lesson() {
//TODO: call create API

        final String password = etPassword.getText();
        final String r_password = retypePassword.getText();

        if(password.length() >= 6 && r_password.length() >= 6)
        {
        if (password.equals(r_password)) {
            Map<String, Object> map = new HashMap<>();
            map.put("password", password);
            map.put("emailAddress",email);
            map.put("token",token);


            networkHelper.postRequest(ResetPassword, map, new ResetPasswordModel(), new NetworkHelper.Listener<ResetPasswordModel>() {
                @Override
                public void onResponse(ResetPasswordModel model) {
                    if (model.result.errors == null || model.result.errors.size() == 0) {
                        Common.showToast(CreatePassword.this, "Password Successfully Changed");
                        Intent myIntent = new Intent(CreatePassword.this, Login.class);
                        startActivity(myIntent);
                    }
                    else
                    {
                        Common.showToast(CreatePassword.this, model.result.message);
                    }

                }

                @Override
                public void onFailure(String msg) {
                }
            });
        }

        else{
            Common.showToast(CreatePassword.this, "Passwords don't match");

        }

        }

        else{
            Common.showToast(CreatePassword.this, "Passwords should be minimum 6 characters");
        }


        // TODO: If first time regiter, it will go to complete your profile
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        email = getIntent().getExtras().getString("email");
        token = getIntent().getExtras().getString("token");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_create_password;
    }
}