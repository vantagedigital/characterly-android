package com.mucaroo.activities.classroom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.GetByEducatorId;
import static com.mucaroo.helper.NetworkHelper.StudentInvite;

/**
 * Created by ahsan on 10/16/2017.
 */

public class ClassAddStudent extends AppActivity {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    @BindView(R.id.sp_class)
    CustomSpinner sp_class;

    @BindView(R.id.et_name)
    CustomEditText et_name;

    @BindView(R.id.et_lastname)
    CustomEditText et_lastname;

    @BindView(R.id.profile_imageview)
    protected ImageView mProfileImageView;

    @BindView(R.id.et_email)
    CustomEditText et_email;

    @BindView(R.id.full_name_text)
    protected TextView mFullNameText;

    @BindView(R.id.class_path)
    protected TextView mClassPath;

    private Classroom mData;
    private CustomProgressDialog pd;

    public static Intent newIntent(Context context, Classroom data) {
        Intent intent = new Intent(context, ClassAddStudent.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    @OnClick(R.id.btn_add_class)
    void btn_add_class() {
        if (et_name.validateLength(1, null) &&
                et_lastname.validateLength(1, null) &&
                et_email.validateEmail()) {

            String type = "3";
            String branchId = mData.educator.branchId;

            Map<String, Object> map = new HashMap<>();
            map.put("BranchId", branchId);
            map.put("Type", type);
            if (classroomModel != null) {
                String typeId = classroomModel.result.get(sp_class.getSelectedPosition()).id;
                map.put("TypeId", typeId);
            }
            map.put("EmailAddress", et_email.getText());
            map.put("Name", et_name.getText());
            map.put("Surname", et_lastname.getText());

            pd.show();
            hideKeyboard();
            networkHelper.postRequest(StudentInvite, map, new Object(), new NetworkHelper.Listener<Object>() {
                @Override
                public void onResponse(Object model) {
                    pd.dismiss();
                    Common.showToast(ClassAddStudent.this, "Successfully Invited Student");
                }

                @Override
                public void onFailure(String msg) {
                    pd.dismiss();
                }
            });

        }

    }

    ClassroomModel classroomModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);

        pd = new CustomProgressDialog(this);

        try {
            mFullNameText.setText(mData.educator.name + " " + mData.educator.surname);
            mClassPath.setText("Classrooms / " + mData.name + " / Add Student");
        } catch (Exception e) {
        }
        try {
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mProfileImageView, mData.educator.profilePicture.path);
        } catch (Exception e) {
        }


        fetchData();
    }

    void fetchData() {
        try {
            String educatorId = mData.educator.id;
            networkHelper.getRequest(GetByEducatorId + educatorId, new ClassroomModel(), new NetworkHelper.Listener<ClassroomModel>() {
                @Override
                public void onResponse(ClassroomModel model) {
                    classroomModel = model;
                    ArrayList<String> classes = new ArrayList<>();
                    for (Classroom items : model.result) {
                        classes.add(items.name);
                    }
                    sp_class.setSpinner(classes);
                }

                @Override
                public void onFailure(String msg) {
                    Log.e("ClassAddStudent", "get class by educator failure");
                }
            });
        } catch (Exception e) {
            Log.e("ClassAddStudent", "get class by educator exception");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classroom_add_student;
    }

    @OnClick(R.id.btn_invite)
    protected void inviteTap() {
        startActivity(ClassInvite.newIntent(this, mData));

    }
}
