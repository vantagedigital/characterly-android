package com.mucaroo.activities.profile;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.mucaroo.R;
import com.mucaroo.adapters.SearchAdapter;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomNotificationBadge;
import com.mucaroo.helper.CustomRadioButton;
import com.mucaroo.models.Notification;

import butterknife.BindView;
import butterknife.BindViews;

/**
 * Created by ahsan on 9/6/2017.
 */

public class ProfileStudentEvaluation extends AppActivity {


    @BindView(R.id.tabs)
    CustomRadioButton tabs;

    @BindViews({R.id.lay_profile, R.id.lay_evaluation})
    LinearLayout[] lay_content;

    @BindView(R.id.evaluation_list)
    RecyclerView evaluation_list;

    @BindView(R.id.badge_counter)
    CustomNotificationBadge badge_counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabs.setupVisibility(lay_content);

        initRecycler();

        updateBadgeCounter();
    }

    private void updateBadgeCounter() {
        int count = 0;
        if (UserCache.getInstance().getNotifications() != null) {
            for (Notification notification : UserCache.getInstance().getNotifications().result) {
                if (!notification.isRead) {
                    count++;
                }
            }
        }
        badge_counter.setCounter(count);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile_student_evaluation;
    }

    void initRecycler() {
        evaluation_list.setLayoutManager(new LinearLayoutManager(this));
        evaluation_list.setFocusable(false);
//        SearchAdapter adapter = new SearchAdapter(this, Common.getDummyList());
//        evaluation_list.setAdapter(adapter);
    }

}
