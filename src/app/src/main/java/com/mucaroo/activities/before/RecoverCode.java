package com.mucaroo.activities.before;

import android.os.Bundle;

import com.mucaroo.R;
import com.mucaroo.activities.classroom.Classrooms;
import com.mucaroo.adapters.ClassroomsAdapter;
import com.mucaroo.application.AppActivity;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomMessage;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.ClassroomModel;
import com.mucaroo.models.StudentModel;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.GetByEducatorId;
import static com.mucaroo.helper.NetworkHelper.RecoverAccessCode;
import static com.mucaroo.helper.NetworkHelper.ResetPasswordRequest;
import static com.mucaroo.helper.NetworkHelper.StudentGetAll;

/**
 * Created by ahsan on 8/10/2017.
 */

public class RecoverCode extends AppActivity {

    @BindView(R.id.layout_msg)
    CustomMessage layout_msg;

    @BindView(R.id.email)
    CustomEditText email;

    @OnClick(R.id.btn_enter)
    void enter() {
        if (email.validateEmail()) {
            networkHelper.getRequest(RecoverAccessCode + email.getText(), new Object(), new NetworkHelper.Listener<Object>() {
                @Override
                public void onResponse(Object model) {
                    replaceActivity(Register.class);
                    Common.showToast(RecoverCode.this, "Successfully Recovered");
                }
                @Override
                public void onFailure(String msg) {
                }
            });
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recover_code;
    }
}