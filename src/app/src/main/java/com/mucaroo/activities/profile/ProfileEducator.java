package com.mucaroo.activities.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.mucaroo.R;
import com.mucaroo.adapters.NotificationAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomNotificationBadge;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomRadioButton;
import com.mucaroo.helper.FileUploadHelper;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.helper.UploadCategories;
import com.mucaroo.models.EducatorModel;
import com.mucaroo.models.FileAttachmentModelSingle;
import com.mucaroo.models.Notification;
import com.mucaroo.models.NotificationModel;
import com.mucaroo.models.UserInfoModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.EducatorUpdate;
import static com.mucaroo.helper.NetworkHelper.NotificationGetAll;

/**
 * Created by ahsan on 8/7/2017.
 */

public class ProfileEducator extends AppActivity {

    private static final String PARAMS_IS_NOTIFICATIONS = "PARAMS_IS_NOTIFICATIONS";

    @BindView(R.id.tabs)
    CustomRadioButton tabs;

    @BindView(R.id.user_name)
    TextView mNameText;

    @BindView(R.id.username)
    CustomEditText mUsername;

    @BindView(R.id.first_name)
    CustomEditText mFirstName;

    @BindView(R.id.middle_name)
    CustomEditText mMiddleName;

    @BindView(R.id.last_name)
    CustomEditText mLastName;

    @BindView(R.id.notification_list)
    RecyclerView notification_list;

    @BindViews({R.id.lay_profile, R.id.lay_notifications})
    LinearLayout[] lay_content;

    @BindView(R.id.badge_counter)
    CustomNotificationBadge badge_counter;

    @BindView(R.id.profile_imageview)
    ImageView profile_imageview;

    private CustomProgressDialog pd;

    private List<Notification> notifications;
    private NotificationAdapter adapter;

    private ImagePicker imagePicker;

    private ImagePickerCallback imagePickerCallback;

    private Handler mHandler = new Handler();

    @BindView(R.id.progress)
    ProgressBar progress;

    private boolean mIsNotificatons;


    public static Intent newIntent(Context context) {
        return newIntent(context, false);
    }

    public static Intent newIntent(Context context, boolean isNotifications) {
        Intent intent = new Intent(context, ProfileEducator.class);
        intent.putExtra(PARAMS_IS_NOTIFICATIONS, isNotifications);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsNotificatons = getIntent().getBooleanExtra(PARAMS_IS_NOTIFICATIONS, false);

        pd = new CustomProgressDialog(this);

        tabs.setupVisibility(lay_content);

        if (mIsNotificatons) {
            tabs.performTabClick(1);
        }

        imagePicker = new ImagePicker(this);

        initRecycler();

        updateUI();

        updateProfilePic();

        if (UserCache.getInstance().getNotifications() != null && UserCache.getInstance().getNotifications().result != null) {
            notifications.clear();
            notifications.addAll(UserCache.getInstance().getNotifications().result);
        }
        updateBadgeCounter();
    }

    private void updateUI() {
        mUsername.getInput().setEnabled(false);
        try {
            mNameText.setText(UserCache.getInstance().getUserInfo().result.user.name + " " + UserCache.getInstance().getUserInfo().result.user.surname);
        } catch (Exception e) {
        }
        try {
            mUsername.setText(UserCache.getInstance().getUserInfo().result.user.userName);
        } catch (Exception e) {
        }
        try {
            mFirstName.setText(UserCache.getInstance().getUserInfo().result.user.name);
        } catch (Exception e) {
        }
        try {
            mLastName.setText(UserCache.getInstance().getUserInfo().result.user.surname);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNotifications();
    }

    private void getNotifications() {
        networkHelper.getRequest(NotificationGetAll, new NotificationModel(), new NetworkHelper.Listener<NotificationModel>() {
            @Override
            public void onResponse(NotificationModel model) {
                UserCache.getInstance().setNotifications(model);
                notifications.clear();
                notifications.addAll(model.result);
                updateBadgeCounter();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
//                        Common.showToast("error getting notifications");
            }
        });
    }

    private void updateProfilePic() {
        ImageLoaderUtil.loadImageIntoView(App.getInstance(), profile_imageview, UserCache.getInstance().getUserInfo().result.user.imagePath);
    }

    @OnClick(R.id.profile_imageview)
    protected void choosePictureCheck() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            choosePicture();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePicture();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    protected void choosePicture() {
        imagePickerCallback = new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> images) {
                // Display images
                if (images.size() > 0) {
                    ChosenImage image = images.get(0);
                    String imagePath = image.getOriginalPath();
                    String mimetype = image.getMimeType();

                    progress.setVisibility(View.VISIBLE);
                    // TODO fix orientation
//                    image.getOrientation();

                    FileUploadHelper.uploadFile(imagePath, mimetype, UploadCategories.UserProfile.getValue() + "", UserCache.getInstance().getUserInfo().result.user.id, new NetworkHelper.Listener<FileAttachmentModelSingle>() {
                        @Override
                        public void onResponse(final FileAttachmentModelSingle model) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        progress.setVisibility(View.GONE);
                                        UserInfoModel userModel = UserCache.getInstance().getUserInfo();
                                        userModel.result.user.imagePath = model.result.path;
                                        UserCache.getInstance().setCurrentUser(userModel);
                                        updateProfilePic();
                                        Common.showToast("Successfully updated profile picture");
                                    } catch (Exception e) {
                                        Common.showToast("Error updating profile picture");
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailure(final String msg) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Common.showToast(msg);
                                    progress.setVisibility(View.GONE);
                                }
                            });
                        }
                    });

                }
            }

            @Override
            public void onError(String message) {
                // Do error handling
            }
        };

        imagePicker.setImagePickerCallback(imagePickerCallback);
// imagePicker.allowMultiple(); // Default is false
// imagePicker.shouldGenerateMetadata(false); // Default is true
// imagePicker.shouldGenerateThumbnails(false); // Default is true
        imagePicker.pickImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(imagePickerCallback);
                }
                imagePicker.submit(data);
            }
        }
    }

    private void updateBadgeCounter() {
        int count = 0;
        if (UserCache.getInstance().getNotifications() != null) {
            for (Notification notification : UserCache.getInstance().getNotifications().result) {
                if (!notification.isRead) {
                    count++;
                }
            }
        }
        badge_counter.setCounter(count);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_educator_account;
    }

    void initRecycler() {
        notifications = new ArrayList<>();
        notification_list.setLayoutManager(new LinearLayoutManager(this));
        notification_list.setFocusable(false);
        adapter = new NotificationAdapter(this, notifications);
        notification_list.setAdapter(adapter);
    }

    @OnClick(R.id.btn_change)
    protected void updateInfo() {
        if (mFirstName.validateLength(2, "Name must be minimum 2 characters")
                && mLastName.validateLength(2, "Name must be minimum 2 characters")
                && mUsername.validateEmail()) {
            String branchId = UserCache.getInstance().getUserInfo().result.user.branchId;
            String userId = UserCache.getInstance().getUserInfo().result.user.id;

            Map<String, Object> map = new HashMap<>();
            map.put("branchId", branchId);
//            map.put("userId", userId);
            map.put("emailAddress", mUsername.getText());
            map.put("userName", mUsername.getText());
            map.put("name", mFirstName.getText());
            map.put("surname", mLastName.getText());
            map.put("id", UserCache.getInstance().getUserInfo().result.user.educator.id);

            pd.show();
            Log.e("EDITEDU", "map:" + map);
            networkHelper.putRequest(EducatorUpdate, map, new EducatorModel(), new NetworkHelper.Listener<EducatorModel>() {
                @Override
                public void onResponse(EducatorModel model) {
                    pd.dismiss();
                    try {
                        UserInfoModel userInfoModel = UserCache.getInstance().getUserInfo();
                        userInfoModel.result.user.name = model.result.name;
                        userInfoModel.result.user.surname = model.result.surname;
                        userInfoModel.result.user.educator.profilePicture = model.result.profilePicture;
                        userInfoModel.result.user.educator.name = model.result.name;
                        userInfoModel.result.user.educator.surname = model.result.surname;
                        UserCache.getInstance().setCurrentUser(userInfoModel);
                    } catch (Exception e) {}


                    updateUI();
                    Common.showToast(ProfileEducator.this, "Successfully Updated Profile");
                }

                @Override
                public void onFailure(String msg) {
                    pd.dismiss();
                }
            });
        }
//
    }

}
