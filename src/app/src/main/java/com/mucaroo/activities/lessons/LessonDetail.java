package com.mucaroo.activities.lessons;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.evaluation.EvaluationWizard;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.ClassroomsDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.DateHelper;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.helper.UserContentScheduleStatus;
import com.mucaroo.interfaces.CompletionStringListener;
import com.mucaroo.models.ContentTag;
import com.mucaroo.models.FileAttachment;
import com.mucaroo.models.MyContentModel;
import com.mucaroo.models.MyContentModelSingle;
import com.mucaroo.models.MyContentResult;
import com.mucaroo.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import co.lujun.androidtagview.TagContainerLayout;

import static com.mucaroo.helper.NetworkHelper.ContentScheduleAssignToClassroom;
import static com.mucaroo.helper.NetworkHelper.UserContentUpdate;

/**
 * Created by ahsan on 8/9/2017.
 */

public class LessonDetail extends AppActivity {

    private static final String PARAMS_DATA = "PARAMS_DATA";


    @BindView(R.id.tagcontainerLayout1)
    TagContainerLayout mTagContainerLayout;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.tv_content)
    TextView mContent;
    @BindView(R.id.date_time)
    TextView mDateTime;
    @BindView(R.id.quote)
    TextView mQuote;
    @BindView(R.id.image_view)
    ImageView mImageView;
    @BindView(R.id.video_play_btn)
    ImageView mVideoPlayBtn;
    @BindView(R.id.audio_btn)
    ImageView mAudioBtn;
    @BindView(R.id.content_container)
    View mContentContainer;
    @BindView(R.id.flashcard_button)
    View mFlashCardButton;

    @BindView(R.id.assign_to_classroom_button)
    Button mAssignToClassroomButton;

    @BindView(R.id.mark_completed_button)
    ImageView mMarkCompletedButton;

    @BindView(R.id.mark_completed_percentage)
    TextView mMarkCompletedPercentage;

    @BindView(R.id.mark_completed_text)
    TextView mMarkCompletedText;

    @BindView(R.id.btn_take)
    Button mBtnTake;

    @BindView(R.id.completed_divider)
    View mCompletedDivider;

    @BindView(R.id.completed_subtitle)
    TextView mCompletedSubtitle;

    @BindView(R.id.flashcard_page_text)
    TextView mFlashcardPageText;

    @BindView(R.id.fb_button)
    View mFbButton;

    @BindView(R.id.btn_pre)
    ImageButton mBtnPre;

    @BindView(R.id.btn_next)
    ImageButton mBtnNext;

    private MyContentResult mData;

    private CustomProgressDialog pd;

    private boolean mIsEducator;

    private int flashcardPage = 0;

    public static Intent newIntent(Context context, MyContentResult data) {
        Intent intent = new Intent(context, LessonDetail.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    @OnClick(R.id.btn_take)
    void take() {
        startActivity(EvaluationWizard.newIntent(this, mData));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        pd = new CustomProgressDialog(this);

        mIsEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;

        mData = getIntent().getParcelableExtra(PARAMS_DATA);

        if (mIsEducator) {
            mAssignToClassroomButton.setVisibility(View.VISIBLE);
        }
//        if (mIsEducator && mData != null & mData.contentSchedule != null && mData.contentSchedule.classrooms != null && mData.contentSchedule.classrooms.size() == 0) {
//            mAssignToClassroomButton.setVisibility(View.VISIBLE);
//        }

        try {
            configureLayoutForContentType(mData);
        } catch (Exception e) {
        }
//        Log.e("LessonDetail", "data:"+mData);
        List<String> tags = new ArrayList<>();
        try {
            for (ContentTag contentTag : mData.contentSchedule.content.contentTags) {
                tags.add(contentTag.name);
            }
        } catch (Exception e) {
        }

        mTagContainerLayout.setTags(tags);

        try {
            mDateTime.setText(DateHelper.getFormattedDate(DateHelper.getCalendarDateFromFormattedString(mData.contentSchedule.startDate, "yyyy-MM-dd'T'HH:mm:ss").getTime(), "MMM dd, yyyy @ hh:mm a"));
        } catch (Exception e) {

        }


        try {
            mTitle.setText(mData.contentSchedule.content.title);
        } catch (Exception e) {

        }
        try {
            mContent.setText(mData.contentSchedule.content.contentText);
        } catch (Exception e) {

        }
    }

    private void configureLayoutForContentType(MyContentResult data) {
        mContentContainer.setVisibility(View.GONE);
        mImageView.setVisibility(View.GONE);
        mQuote.setVisibility(View.GONE);
        mVideoPlayBtn.setVisibility(View.GONE);
        mAudioBtn.setVisibility(View.GONE);
        mFlashCardButton.setVisibility(View.GONE);

        Integer contentType = data.contentSchedule.content.contentType;

        configureMarkAsCompletedButton(data);

        if (contentType == 0) {
            // Article type, just leave default description showing
            mContentContainer.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            String imageUrl = "";
            try {
                List<FileAttachment> attachments = mData.contentSchedule.content.fileAttachments == null ? mData.contentSchedule.content.attachments : mData.contentSchedule.content.fileAttachments;
                imageUrl = attachments.get(attachments.size() - 1).path;
            } catch (Exception e) {
            }
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mImageView, imageUrl);
        } else if (contentType == 1) {//Audio
            mContentContainer.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            mAudioBtn.setVisibility(View.VISIBLE);
            mImageView.setBackgroundColor(getResources().getColor(R.color.white_four));

            mAudioBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // launch audio with another application
                    String audioUrl = "";
                    try {
                        List<FileAttachment> attachments = mData.contentSchedule.content.fileAttachments == null ? mData.contentSchedule.content.attachments : mData.contentSchedule.content.fileAttachments;
                        audioUrl = attachments.get(attachments.size() - 1).path;
                    } catch (Exception e) {
                    }

                    if (!TextUtils.isEmpty(audioUrl)) {
                        Uri uri = Uri.parse(audioUrl);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(intent);
                        } catch (Exception e) {
                            Common.showToast("Error trying to play audio link");
                        }
                    } else {
                        Common.showToast("No Audio link available");
                    }
                }
            });
        } else if (contentType == 2) {//Image
            mContentContainer.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            String imageUrl = "";
            try {
                List<FileAttachment> attachments = mData.contentSchedule.content.fileAttachments == null ? mData.contentSchedule.content.attachments : mData.contentSchedule.content.fileAttachments;
                imageUrl = attachments.get(attachments.size() - 1).path;
            } catch (Exception e) {
            }
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mImageView, imageUrl);
        } else if (contentType == 3) {//Video
            mContentContainer.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            mVideoPlayBtn.setVisibility(View.VISIBLE);

            mImageView.setBackgroundColor(Color.BLACK);

            mVideoPlayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // launch video with another application
                    String videoUrl = "";
                    try {
                        List<FileAttachment> attachments = mData.contentSchedule.content.fileAttachments == null ? mData.contentSchedule.content.attachments : mData.contentSchedule.content.fileAttachments;
                        videoUrl = attachments.get(attachments.size() - 1).path;
                    } catch (Exception e) {
                    }

                    if (!TextUtils.isEmpty(videoUrl)) {
                        Uri uri = Uri.parse(videoUrl);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(intent);
                        } catch (Exception e) {
                            Common.showToast("Error trying to play video link");
                        }
                    } else {
                        Common.showToast("No Video link available");
                    }
                }
            });

        } else if (contentType == 4) {//Quote
            mContentContainer.setVisibility(View.VISIBLE);
            mQuote.setVisibility(View.VISIBLE);
            try {
                mQuote.setText("\"" + mData.contentSchedule.content.description + "\"");
            } catch (Exception e) {

            }

        } else if (contentType == 5) {//Flash Cards
            mContentContainer.setVisibility(View.VISIBLE);
            mFlashCardButton.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            mImageView.setBackgroundColor(getResources().getColor(R.color.white_four));
            mQuote.setVisibility(View.VISIBLE);

            if (mData.contentSchedule.content.contentPages == null) {
                return;
            }
            if (mData.contentSchedule.content.contentPages.size() == 0) {
                //show image
                mImageView.setVisibility(View.VISIBLE);
                String imageUrl = "";
                try {
                    List<FileAttachment> attachments = mData.contentSchedule.content.fileAttachments == null ? mData.contentSchedule.content.attachments : mData.contentSchedule.content.fileAttachments;
                    imageUrl = attachments.get(attachments.size() - 1).path;
                } catch (Exception e) {
                }
                ImageLoaderUtil.loadImageIntoView(App.getInstance(), mImageView, imageUrl);
            }
            updateFlashCardUI();
            mBtnPre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (flashcardPage <= 0) {
                        return;
                    }
                    flashcardPage--;
                    updateFlashCardUI();
                }
            });
            mBtnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (flashcardPage >= mData.contentSchedule.content.contentPages.size() - 1) {
                        return;
                    }
                    flashcardPage++;
                    updateFlashCardUI();
                }
            });

        }

    }

    private void updateFlashCardUI() {
        try {
            int increment = mData.contentSchedule.content.contentPages.size() == 0 ? 0 : 1;
            mFlashcardPageText.setText((flashcardPage + increment) + "/" + mData.contentSchedule.content.contentPages.size());
            mQuote.setText(mData.contentSchedule.content.contentPages.get(flashcardPage).contentText);
        } catch (Exception e) {
        }
    }

    private void configureMarkAsCompletedButton(MyContentResult data) {
        mMarkCompletedButton.setVisibility(View.GONE);
        mMarkCompletedPercentage.setVisibility(View.GONE);
        mMarkCompletedText.setVisibility(View.GONE);
        mBtnTake.setVisibility(View.GONE);
        mCompletedDivider.setVisibility(View.GONE);
        mCompletedSubtitle.setVisibility(View.GONE);
        mFbButton.setVisibility(View.GONE);

        if (mIsEducator) {
            return;
        }

        Integer status = data.status;
        Integer percentage = data.progressPercentage.intValue();
        if (status == UserContentScheduleStatus.Completed.getValue()) {
            mMarkCompletedButton.setVisibility(View.VISIBLE);
            mMarkCompletedText.setVisibility(View.VISIBLE);
            mMarkCompletedButton.setImageDrawable(getResources().getDrawable(R.drawable.ico_completed_active));

            mCompletedDivider.setVisibility(View.VISIBLE);
            mCompletedSubtitle.setVisibility(View.VISIBLE);
            mFbButton.setVisibility(View.VISIBLE);

            mMarkCompletedText.setText("Congratulations you have completed this task and earned 5pt. ");

            mFbButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Common.showToast("Function not implemented");
                }
            });

        } else if (status == UserContentScheduleStatus.InProgress.getValue()) {
            mMarkCompletedButton.setVisibility(View.VISIBLE);
            mMarkCompletedText.setVisibility(View.VISIBLE);
            mMarkCompletedPercentage.setVisibility(View.VISIBLE);
            mMarkCompletedButton.setImageDrawable(getResources().getDrawable(R.drawable.ico_completed_active_no_tick));
            mMarkCompletedPercentage.setText(percentage + "%");

            if (percentage >= 50) {
                mMarkCompletedText.setText("Congratulations you have completed a " + percentage + "%, " +
                        "now take the evaluation to complete this lesson.");
                mBtnTake.setVisibility(View.VISIBLE);
            } else {
                mMarkCompletedText.setText("");
            }

        } else {
            mMarkCompletedButton.setVisibility(View.VISIBLE);
            mMarkCompletedText.setVisibility(View.VISIBLE);
            mMarkCompletedButton.setImageDrawable(getResources().getDrawable(R.drawable.ico_completed_unactive));
            mMarkCompletedText.setText("Mark completed");

            mMarkCompletedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String userId = UserCache.getInstance().getUserInfo().result.user.id;

                    Map<String, Object> map = new HashMap<>();
                    map.put("progressPercentage", "50");
                    map.put("userId", userId);
                    map.put("contentScheduleId", mData.contentScheduleId);
                    map.put("status", UserContentScheduleStatus.InProgress.getValue() + "");
                    map.put("id", mData.id);

                    pd.show();

                    networkHelper.putRequest(UserContentUpdate, map, new MyContentModelSingle(), new NetworkHelper.Listener<MyContentModelSingle>() {
                        @Override
                        public void onResponse(MyContentModelSingle model) {
                            pd.dismiss();
                            Common.showToast(LessonDetail.this, "Successfully Marked as completed");

                            Common.logOutput(model.toString());
                            MyContentModel contentModel = UserCache.getInstance().getContentInfo();
                            int loc = -1;
                            for (int i = 0; i < contentModel.result.size(); i++) {
                                if (mData.id.equalsIgnoreCase(contentModel.result.get(i).id)) {
                                    loc = i;
                                }
                            }
                            mData.progressPercentage = model.result.progressPercentage;
                            mData.status = model.result.status;

                            if (loc > -1) {
                                contentModel.result.set(loc, mData);
                                UserCache.getInstance().setContentInfo(contentModel);
                            }

                            configureMarkAsCompletedButton(mData);
                        }

                        @Override
                        public void onFailure(String msg) {
                            pd.dismiss();
                        }
                    });

                }
            });
        }
    }

    @OnClick(R.id.assign_to_classroom_button)
    protected void assignToClassroomTap() {
        ClassroomsDialog dialog = new ClassroomsDialog(this);
        dialog.setListener(new CompletionStringListener() {
            @Override
            public void onCompleted(String value) {
                pd.show();
                String contentScheduleParam = "contentScheduleId=" + mData.contentSchedule.id;
                String classroomParam = "classroomId=" + value;
                Map<String, Object> map = new HashMap<>();
                networkHelper.postRequest(ContentScheduleAssignToClassroom + contentScheduleParam + "&" + classroomParam, map, new Object(), new NetworkHelper.Listener<Object>() {
                    @Override
                    public void onResponse(Object model) {
                        pd.dismiss();
                        Common.showToast("Successfully assigned lesson to classroom");
                    }

                    @Override
                    public void onFailure(String msg) {
                        pd.dismiss();
                        if (TextUtils.isEmpty(msg)) {
                            msg = "Error assigning lesson to classroom";
                        }
                        Common.showToast(msg);
                    }
                });
            }
        });
        dialog.show();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_lesson_detail;
    }
}
