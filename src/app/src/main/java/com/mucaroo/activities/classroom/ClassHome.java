package com.mucaroo.activities.classroom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.lessons.LessonDetail;
import com.mucaroo.adapters.SearchAdapter;
import com.mucaroo.adapters.StudentAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.MessageDialogFragment;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomRadioButton;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.interfaces.OnDialogButtonSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.EvaluationModel;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.MyContentResult;
import com.mucaroo.models.Student;
import com.mucaroo.models.StudentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.ClassroomDelete;
import static com.mucaroo.helper.NetworkHelper.EvaluationGetByClassroomId;
import static com.mucaroo.helper.NetworkHelper.EvaluationSearch;
import static com.mucaroo.helper.NetworkHelper.FileAttachmentGetByEntityId;
import static com.mucaroo.helper.NetworkHelper.StudentGetAll;
import static com.mucaroo.helper.NetworkHelper.StudentGetByClassroomId;
import static com.mucaroo.helper.NetworkHelper.StudentSearch;

/**
 * Created by ahsan on 9/1/2017.
 */

public class ClassHome extends AppActivity implements CustomRadioButton.listener, NetworkHelper.Listener {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    @BindView(R.id.tabs)
    CustomRadioButton tabs;

    @BindView(R.id.search)
    CustomEditText search;

    @BindView(R.id.class_code)
    protected TextView mClassCode;

    @BindView(R.id.class_name)
    protected TextView mClassName;

    @BindView(R.id.sort)
    CustomSpinner sort;

    @BindView(R.id.btn_delete)
    Button mBtnDelete;

    @BindView(R.id.pillars)
    CustomSpinner pillars;

    @BindView(R.id.full_name_text)
    protected TextView mFullNameText;

    @BindView(R.id.image_view)
    protected ImageView mImageView;

    @BindView(R.id.btn_add_student)
    protected View btn_add_student;

    @BindView(R.id.progress_base)
    ProgressBar mProgressBarBase;

    @BindView(R.id.btn_load_more)
    Button mBtnLoadMore;

    @BindView(R.id.profile_imageview)
    protected ImageView mProfileImageView;

    private int studentCurrentPage = 0, evalCurrentPage = 0;
    @BindViews({R.id.students_list, R.id.evaluation_list})
    RecyclerView[] recyclerView;
    private String mSearchedValue;
    private CustomProgressDialog pd;

    protected List<Student> mStudents = new ArrayList<>();
    protected List<Evaluation> mEvaluations = new ArrayList<>();

    StudentAdapter adapter;
    SearchAdapter evalAdapter;

    private Classroom mData;
    private boolean mIsStudentSegment = true;

    private boolean mIsEducator;

    public static Intent newIntent(Context context, Classroom data) {
        Intent intent = new Intent(context, ClassHome.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);

        pd = new CustomProgressDialog(this);

        mIsEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;

        if (!mIsEducator) {
            mBtnDelete.setVisibility(View.GONE);
        }

        try {
            mFullNameText.setText(mData.educator.name + " " + mData.educator.surname);
        } catch (Exception e) {
        }

        try {
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mProfileImageView, mData.educator.profilePicture.path);
        } catch (Exception e) {
        }

        boolean isEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;
        if (!isEducator) {
            btn_add_student.setVisibility(View.GONE);
        }

        ArrayList<String> sortOrders = new ArrayList<>();
        sortOrders.add("Descending");
        sortOrders.add("Ascending");
        sort.setSpinner(sortOrders);
        sort.setSelectedListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                if (mIsStudentSegment) {
                    searchStudent(search.getInput().getText().toString());
                } else {
                    searchEvaluation(search.getInput().getText().toString());
                }
            }
        });

        mClassCode.setText(mData.classCode);
        mClassName.setText(mData.name);

        recyclerView[1].setVisibility(View.GONE);
        tabs.setupVisibility(recyclerView);
        tabs.onSelectedListner(this);
        initRecycler();

        search.getInput().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (mIsStudentSegment) {
                        searchStudent(textView.getText().toString());
                    } else {
                        searchEvaluation(textView.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        try {
            if (TextUtils.isEmpty(mData.imagePath)) {
                networkHelper.getRequest(FileAttachmentGetByEntityId + mData.id, new FileAttachmentModel(), new NetworkHelper.Listener<FileAttachmentModel>() {
                    @Override
                    public void onResponse(FileAttachmentModel model) {
                        try {
                            mData.imagePath = model.result.get(model.result.size() - 1).path;

                            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mImageView, mData.imagePath);
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onFailure(String msg) {
                    }
                });
            } else {
                ImageLoaderUtil.loadImageIntoView(App.getInstance(), mImageView, mData.imagePath);
            }
        } catch (Exception e) {
        }
    }

    private void searchStudent(String value) {
        studentCurrentPage = 0;
        search(StudentSearch, value, new StudentModel());
    }

    private void searchEvaluation(String value) {
        evalCurrentPage = 0;
        search(EvaluationSearch, value, new EvaluationModel());
    }

    private void search(String url, String value, Object model) {
        pd.show();
        hideKeyboard();
        mSearchedValue = value;

        Map<String, Object> map = new HashMap<>();
        map.put("searchText", value);
        map.put("sortOrder", String.valueOf(sort.getSelectedPosition()));
        map.put("maxResultCount", NetworkHelper.MAX_RESULTS);
        int count = 0;
        if (model instanceof StudentModel) {
            count = studentCurrentPage;
        } else if (model instanceof EvaluationModel) {
            count = evalCurrentPage;
        }
        map.put("skipCount", (count * NetworkHelper.MAX_RESULTS));

        networkHelper.postRequest(url, map, model, this);
        hideKeyboard();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classroom_home;
    }

    void initRecycler() {

        getStudents();
        getEvaluations();

        for (RecyclerView list : recyclerView) {
            list.setLayoutManager(new LinearLayoutManager(this));
            list.setFocusable(false);
        }
        evalAdapter = new SearchAdapter(this, mEvaluations);
        recyclerView[1].setAdapter(evalAdapter);

        adapter = new StudentAdapter(this, mStudents);
        recyclerView[0].setAdapter(adapter);

        adapter.setListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                Student data = mStudents.get(position);
                startActivity(ClassStudentProfile.newIntent(ClassHome.this, data, mData));

            }
        });

    }

    private void getEvaluations() {
        String url = EvaluationGetByClassroomId + mData.id;
        url = url + "+&SkipCount=" + (evalCurrentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        networkHelper.getRequest(url, new EvaluationModel(), this);
    }

    private void getStudents() {
        String url = StudentGetByClassroomId + mData.id;
        url = url + "+&SkipCount=" + (studentCurrentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        networkHelper.getRequest(url, new StudentModel(), this);
    }

    @Override
    public void onSegmentSelected(int position) {

        if (position == 0) {
            mIsStudentSegment = true;
            search.setHint("Students");
            pillars.setVisibility(View.GONE);
        } else {
            mIsStudentSegment = false;
            search.setHint("Evaluation");
            pillars.setVisibility(View.VISIBLE);
        }
        mBtnLoadMore.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResponse(Object object) {

        if (object instanceof StudentModel) {
            StudentModel studentModel = (StudentModel) object;

            if (studentModel.result.size() == 0 && mSearchedValue != null) {
                Common.showToast("No results found for: " + mSearchedValue);
            }

            if (studentCurrentPage == 0) {
                mStudents.clear();
            }
            mStudents.addAll(studentModel.result);

            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
            if (studentModel.result != null && studentModel.result.size() == NetworkHelper.MAX_RESULTS) {
                mBtnLoadMore.setVisibility(View.VISIBLE);
            } else {
                mBtnLoadMore.setVisibility(View.GONE);
            }

            hideLoader();

            pd.dismiss();
            adapter.notifyDataSetChanged();
        } else if (object instanceof EvaluationModel) {
            EvaluationModel evaluationModel = (EvaluationModel) object;

            if (evaluationModel.result.size() == 0 && mSearchedValue != null) {
                Common.showToast("No results found for: " + mSearchedValue);
            }

            if (evalCurrentPage == 0) {
                mEvaluations.clear();
            }
            mEvaluations.addAll(evaluationModel.result);

            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
            if (evaluationModel.result != null && evaluationModel.result.size() == NetworkHelper.MAX_RESULTS) {
                mBtnLoadMore.setVisibility(View.VISIBLE);
            } else {
                mBtnLoadMore.setVisibility(View.GONE);
            }

            hideLoader();

            pd.dismiss();
            evalAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure(String msg) {
        if (mProgressBarBase.getVisibility() == View.VISIBLE) {
            Common.showToast(msg);
            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
        }
        hideLoader();
        pd.dismiss();
    }

    private void showLoader() {
        mProgressBarBase.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBarBase.setVisibility(View.GONE);
    }


    @OnClick(R.id.btn_load_more)
    protected void loadMoreTap() {
        mBtnLoadMore.setEnabled(false);
        mBtnLoadMore.setAlpha(0.5f);
        showLoader();
        if (mIsStudentSegment) {
            studentCurrentPage++;
            getStudents();
        } else {
            evalCurrentPage++;
            getEvaluations();
        }
    }

    @OnClick(R.id.btn_classroom)
    protected void classroomTap() {

    }

    @OnClick(R.id.btn_timeline)
    protected void timelineTap() {
        startActivity(ClassTimeline.newIntent(this, mData));
    }

    @OnClick(R.id.btn_add_student)
    protected void addStudentTap() {
        startActivity(ClassAddStudent.newIntent(this, mData));

    }

    @OnClick(R.id.btn_invite)
    protected void inviteTap() {
        startActivity(ClassInvite.newIntent(this, mData));

    }

    @OnClick(R.id.btn_manage_class)
    protected void manageClassTap() {
        startActivity(ClassAdd.newIntent(this, mData));
    }

    @OnClick(R.id.btn_delete)
    protected void deleteTap() {
        // show confirmation dialog
        MessageDialogFragment dialogFragment = MessageDialogFragment.newInstance(this, "Delete Class", "Are you sure you want to delete this classroom?", "Cancel", "Yes", new OnDialogButtonSelectedListener() {
            @Override
            public void leftButtonSelected() {
            }

            @Override
            public void rightButtonSelected() {
                deleteClassroom();
            }
        });
        dialogFragment.show();

    }

    private void deleteClassroom() {
        pd.show();
        networkHelper.deleteRequest(ClassroomDelete + mData.id, new Object(), new NetworkHelper.Listener<Object>() {
            @Override
            public void onResponse(Object model) {
                pd.dismiss();
                Common.showToast(ClassHome.this, "Successfully Deleted: " + mData.name);
                onBackPressed();
            }

            @Override
            public void onFailure(String msg) {
                Common.showToast(ClassHome.this, "Error deleting: " + mData.name);
                pd.dismiss();
            }
        });
    }
}
