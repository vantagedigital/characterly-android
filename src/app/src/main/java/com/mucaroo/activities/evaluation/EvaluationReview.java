package com.mucaroo.activities.evaluation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.classroom.ClassStudentProfile;
import com.mucaroo.adapters.EvaluationAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.CompletionListener;
import com.mucaroo.models.Answer;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.Question;
import com.mucaroo.models.Student;
import com.mucaroo.models.UserEvaluation;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.EvaluationReview;

/**
 * Created by ahsan on 9/6/2017.
 */

public class EvaluationReview extends AppActivity {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    private static final String PARAMS_DATA_CLASS = "PARAMS_DATA_CLASS";
    private static final String PARAMS_EVALUATION = "PARAMS_EVALUATION";

    private Student mData;
    private Classroom mDataClass;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.profile_imageview)
    ImageView mProfileImageView;

    @BindView(R.id.student_name)
    TextView mStudentName;

    @BindView(R.id.student_number)
    TextView mStudentNumber;

    @BindView(R.id.evaluation_name)
    TextView mEvaluationName;

    @BindView(R.id.evaluation_id)
    TextView mEvaluationId;

    @BindView(R.id.header)
    TextView mHeader;

    @BindView(R.id.class_path)
    TextView mClassPath;

    @BindView(R.id.correct_text)
    TextView mCorrectText;

    @BindView(R.id.incorrect_text)
    TextView mIncorrectText;

    @BindView(R.id.final_score_text)
    TextView mFinalScoreText;

    private EvaluationAdapter adapter;

    private CustomProgressDialog pd;

    private int mFinalScore;

    List<Answer> mAnswers = new ArrayList<>();
    List<Question> mQuestions = new ArrayList<>();
    private UserEvaluation mUserEvaluation;

    public static Intent newIntent(Context context, Student data, Classroom dataClass, UserEvaluation userEvaluation) {
        Intent intent = new Intent(context, EvaluationReview.class);
        intent.putExtra(PARAMS_DATA, data);
        intent.putExtra(PARAMS_DATA_CLASS, dataClass);
        intent.putExtra(PARAMS_EVALUATION, userEvaluation);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);
        mDataClass = getIntent().getParcelableExtra(PARAMS_DATA_CLASS);
        mUserEvaluation = getIntent().getParcelableExtra(PARAMS_EVALUATION);

        pd = new CustomProgressDialog(this);

        initRecycler();

        if (mUserEvaluation != null && mUserEvaluation.answers != null) {
            mAnswers.addAll(mUserEvaluation.answers);

        }
        if (mUserEvaluation != null && mUserEvaluation.evaluation != null && mUserEvaluation.evaluation.questions != null) {
            mQuestions.addAll(mUserEvaluation.evaluation.questions);

        }
        String evalName = "";
        String evalId = "";
        if (mUserEvaluation != null && mUserEvaluation.evaluation != null) {
            evalName = mUserEvaluation.evaluation.name;
            evalId = mUserEvaluation.evaluation.id;
        }

        try {
            String profilePicPath = mData.profilePicture == null ? null : mData.profilePicture.path;
            String imagePath = TextUtils.isEmpty(profilePicPath) ? mData.imagePath : profilePicPath;
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mProfileImageView, imagePath);
        } catch (Exception e) {
        }

        mHeader.setText(mData.name + " " + mData.surname);
        mStudentName.setText(mData.name + " " + mData.surname);
        mStudentNumber.setText("Student Number: " + mData.userId);
        mEvaluationName.setText(evalName);
        mEvaluationId.setText("Evaluation: " + evalId);

        mClassPath.setText("Classroom / " + mDataClass.name + " / " + mData.name + " " + mData.surname + " / " + evalName);

        updateScoreUI();
        adapter.setDataChangedListener(new CompletionListener() {
            @Override
            public void onCompleted() {
                updateScoreUI();
            }
        });
    }

    private void updateScoreUI() {
        int correct = 0;
        int incorrect = 0;

        for (int i = 0; i < mQuestions.size(); i++) {
            if (adapter.getScoredAnswers().containsKey(i) && adapter.getScoredAnswers().get(i) == true) {
                correct++;
            } else if (adapter.getScoredAnswers().containsKey(i) && adapter.getScoredAnswers().get(i) == false) {
                incorrect++;
            }
        }
        mFinalScore = (int) (((float) correct / mQuestions.size()) * 100);
        mCorrectText.setText("Correct: " + correct);
        mIncorrectText.setText("Incorrect: " + incorrect);
        mFinalScoreText.setText("Final score: " + mFinalScore + "%");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_evaluation_review;
    }

    void initRecycler() {
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setFocusable(false);
        adapter = new EvaluationAdapter(this, mAnswers, mQuestions, true);
        recycler_view.setAdapter(adapter);
    }

    @OnClick(R.id.resend_to_student_button)
    protected void resendToStudentTap() {

    }

    @OnClick(R.id.give_score_button)
    protected void giveScoreTap() {

//        String userId = UserCache.getInstance().getUserInfo().result.user.id;
//
//        Map<String, Object> map = new HashMap<>();
//        map.put("id", mEvaluation.id);
//        map.put("status", "3");
//        // TODO: fix after API is ready
////        map.put("answers", new JSONArray(mAnswers));
//        map.put("userId", userId);
//        // TODO: fix after API is ready
////        map.put("evaluationId", mEvaluation.evaluation.id);
//
//        Log.e("EvaluationReview", "map:" + map);
//
//        pd.show();
//        networkHelper.postRequest(EvaluationReview, map, new Object(), new NetworkHelper.Listener<Object>() {
//            @Override
//            public void onResponse(Object model) {
//                pd.dismiss();
//                onBackPressed();
//            }
//
//            @Override
//            public void onFailure(String msg) {
//                pd.dismiss();
//            }
//        });
    }


}
