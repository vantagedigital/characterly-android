package com.mucaroo.activities.classroom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.activities.evaluation.EvaluationReview;
import com.mucaroo.adapters.NotificationAdapter;
import com.mucaroo.adapters.SearchAdapter;
import com.mucaroo.adapters.StudentAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.BadgeAllDialog;
import com.mucaroo.dialogs.ParentEditDialog;
import com.mucaroo.dialogs.ProfileEditStudentDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomBadgeLayout;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomNotificationBadge;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomRadioButton;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.CustomTextView;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Badge;
import com.mucaroo.models.BadgeModel;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomSingleModel;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.EvaluationModel;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.GradeModel;
import com.mucaroo.models.Notification;
import com.mucaroo.models.Student;
import com.mucaroo.models.StudentModel;
import com.mucaroo.models.UserEvaluation;
import com.mucaroo.models.UserEvaluationModels;
import com.mucaroo.models.UserInfoModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.BadgeGetByUserId;
import static com.mucaroo.helper.NetworkHelper.ClassroomCreate;
import static com.mucaroo.helper.NetworkHelper.ClassroomDelete;
import static com.mucaroo.helper.NetworkHelper.ClassroomGet;
import static com.mucaroo.helper.NetworkHelper.ClassroomUpdate;
import static com.mucaroo.helper.NetworkHelper.EvaluationGetByClassroomId;
import static com.mucaroo.helper.NetworkHelper.EvaluationSearch;
import static com.mucaroo.helper.NetworkHelper.FileAttachmentGetByEntityId;
import static com.mucaroo.helper.NetworkHelper.GradeGetAll;
import static com.mucaroo.helper.NetworkHelper.StudentGetAll;
import static com.mucaroo.helper.NetworkHelper.StudentSearch;
import static com.mucaroo.helper.NetworkHelper.UserEvaluationGetByUserId;
import static com.mucaroo.helper.NetworkHelper.UserEvaluationSearch;

/**
 * Created by ahsan on 9/6/2017.
 */

public class ClassStudentProfile extends AppActivity implements NetworkHelper.Listener {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    private static final String PARAMS_DATA_CLASS = "PARAMS_DATA_CLASS";

    private Student mData;
    private Classroom mDataClass;

    @BindView(R.id.header)
    TextView mHeader;

    @BindView(R.id.class_path)
    TextView mClassPath;

    @BindView(R.id.tabs)
    CustomRadioButton tabs;

    @BindView(R.id.search)
    CustomEditText search;

    @BindView(R.id.tv_grade)
    CustomTextView tv_grade;

    @BindView(R.id.tv_homeroom)
    CustomTextView tv_homeroom;

    @BindView(R.id.tv_dateofbirth)
    CustomTextView tv_dateofbirth;

    @BindView(R.id.tv_gender)
    CustomTextView tv_gender;

    @BindView(R.id.tv_school_address)
    CustomTextView tv_school_address;

    @BindView(R.id.tv_email)
    CustomTextView tv_email;

    @BindView(R.id.tv_phone)
    CustomTextView tv_phone;

    @BindView(R.id.name_text)
    TextView mNameText;

    @BindView(R.id.id_text)
    TextView mIdText;

    @BindView(R.id.tv_father_name)
    TextView tv_father_name;

    @BindView(R.id.tv_father_address)
    CustomTextView tv_father_address;

    @BindView(R.id.tv_father_email)
    CustomTextView tv_father_email;

    @BindView(R.id.tv_father_phone)
    CustomTextView tv_father_phone;

    @BindView(R.id.tv_mother_name)
    TextView tv_mother_name;

    @BindView(R.id.tv_mother_address)
    CustomTextView tv_mother_address;

    @BindView(R.id.tv_mother_email)
    CustomTextView tv_mother_email;

    @BindView(R.id.tv_mother_phone)
    CustomTextView tv_mother_phone;

    @BindView(R.id.evaluation_list)
    RecyclerView mEvaluationList;

    @BindView(R.id.profile_imageview)
    ImageView profile_imageview;

    @BindView(R.id.badge1)
    CustomBadgeLayout mBadge1;

    @BindView(R.id.badge2)
    CustomBadgeLayout mBadge2;

    @BindView(R.id.badge3)
    CustomBadgeLayout mBadge3;

    @BindView(R.id.badge4)
    CustomBadgeLayout mBadge4;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    @BindView(R.id.sort)
    CustomSpinner sort;

    @BindView(R.id.badge_counter)
    CustomNotificationBadge badge_counter;

    @BindView(R.id.btn_load_more)
    Button mBtnLoadMore;

    @BindView(R.id.progress_base)
    ProgressBar mProgressBarBase;

    private int currentPage = 0;

    private String mSearchedValue;
    private CustomProgressDialog pd;

    private SearchAdapter adapter;
    protected List<UserEvaluation> mEvaluations = new ArrayList<>();

    @OnClick(R.id.btn_badge_all)
    void btn_badge_all() {
        BadgeAllDialog dialog = new BadgeAllDialog(this);
        dialog.show();
    }

    @BindViews({R.id.lay_profile, R.id.lay_evaluation, R.id.lay_report})
    LinearLayout[] lay_content;

    @OnClick(R.id.btn_profile_edit)
    void profile_edit() {
        ProfileEditStudentDialog dialog = new ProfileEditStudentDialog(this, mDataClass, mData);
        dialog.show();
    }

    @OnClick(R.id.btn_parent_edit)
    void parent_edit() {
        if (mData.parents != null && mData.parents.size() > 0) {
            ParentEditDialog dialog = new ParentEditDialog(this);
            dialog.show();
        } else {
            Common.showToast("No parents to edit!");
        }
    }

    public static Intent newIntent(Context context, Student data, Classroom dataClass) {
        Intent intent = new Intent(context, ClassStudentProfile.class);
        intent.putExtra(PARAMS_DATA, data);
        intent.putExtra(PARAMS_DATA_CLASS, dataClass);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);
        mDataClass = getIntent().getParcelableExtra(PARAMS_DATA_CLASS);

        pd = new CustomProgressDialog(this);

        ArrayList<String> sortOrders = new ArrayList<>();
        sortOrders.add("Descending");
        sortOrders.add("Ascending");
        sort.setSpinner(sortOrders);

        mHeader.setText(mData.name + " " + mData.surname);
        mClassPath.setText("Classroom / " + mData.name + " " + mData.surname);

        tabs.setupVisibility(lay_content);

        search.getInput().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_NEXT) {
                    searchEvaluation(textView.getText().toString());
                    return true;
                }
                return false;
            }
        });

        initRecycler();


        tv_grade.setText(" ");
        tv_homeroom.setText(" ");
        tv_dateofbirth.setText(" ");
        tv_gender.setText(" ");
        tv_school_address.setText(" ");

        resetUI();
        if (mData.parents != null && mData.parents.size() > 0) {
            configureFather(mData.parents.get(0));

            if (mData.parents.size() > 1) {
                configureMother(mData.parents.get(1));
            }
        }

        try {
            tv_email.setText(mData.emailAddress);
        } catch (Exception e) {
        }
        tv_phone.setText(" ");

        try {
            mNameText.setText(mData.name + " " + mData.surname);
        } catch (Exception e) {
        }
        try {
            mIdText.setText(String.valueOf(mData.userId));
        } catch (Exception e) {
        }

        configureBadges();

        String profilePicPath = mData.profilePicture == null ? null : mData.profilePicture.path;
        if (TextUtils.isEmpty(mData.imagePath) && TextUtils.isEmpty(profilePicPath)) {
            networkHelper.getRequest(FileAttachmentGetByEntityId + mData.userId, new FileAttachmentModel(), new NetworkHelper.Listener<FileAttachmentModel>() {
                @Override
                public void onResponse(FileAttachmentModel model) {
                    try {
                        mData.imagePath = model.result.get(model.result.size() - 1).path;


                        updateProfilePic();
                    } catch (Exception e) {
                        Log.e("ClassStudentProfile", "error saving profile image");
                    }
                }

                @Override
                public void onFailure(String msg) {
                }
            });
        } else {
            updateProfilePic();
        }

        updateBadgeCounter();
    }

    private void configureBadges() {
        mProgress.setVisibility(View.VISIBLE);
        mBadge1.setVisibility(View.GONE);
        mBadge2.setVisibility(View.GONE);
        mBadge3.setVisibility(View.GONE);
        mBadge4.setVisibility(View.GONE);
        networkHelper.getRequest(BadgeGetByUserId + mData.userId, new BadgeModel(), new NetworkHelper.Listener<BadgeModel>() {
            @Override
            public void onResponse(BadgeModel model) {
                UserCache.getInstance().setBadgeInfo(model);
                mProgress.setVisibility(View.GONE);
                try {
                    for (int i = 0; i < model.result.size() && i < 4; i++) {
                        CustomBadgeLayout badgeView = getBadgeFor(i);
                        Badge badge = model.result.get(i);
                        badgeView.setVisibility(View.VISIBLE);
                        badgeView.setBadge(badge);
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String msg) {
                mProgress.setVisibility(View.GONE);
            }
        });
    }

    private CustomBadgeLayout getBadgeFor(int i) {
        if (i == 0) {
            return mBadge1;
        } else if (i == 1) {
            return mBadge2;
        } else if (i == 2) {
            return mBadge3;
        } else {
            return mBadge4;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateProfilePic();
    }

    private void updateProfilePic() {
        String profilePicPath = mData.profilePicture == null ? null : mData.profilePicture.path;
        String imagePath = TextUtils.isEmpty(profilePicPath) ? mData.imagePath : profilePicPath;
        ImageLoaderUtil.loadImageIntoView(App.getInstance(), profile_imageview, imagePath);
    }

    private void updateBadgeCounter() {
        int count = 0;
        if (UserCache.getInstance().getNotifications() != null) {
            for (Notification notification : UserCache.getInstance().getNotifications().result) {
                if (!notification.isRead) {
                    count++;
                }
            }
        }
        badge_counter.setCounter(count);
    }

    private void resetUI() {
        tv_father_name.setText("");
        tv_father_address.setText("");
        tv_father_email.setText("");
        tv_father_phone.setText("");
        tv_mother_name.setText("");
        tv_mother_address.setText("");
        tv_mother_email.setText("");
        tv_mother_phone.setText("");
    }

    private void configureFather(UserInfoModel.Parent father) {
        tv_father_name.setText(father.fullName);
        tv_father_address.setText("");
        tv_father_email.setText(father.emailAddress);
        tv_father_phone.setText("");
    }

    private void configureMother(UserInfoModel.Parent mother) {
        tv_mother_name.setText(mother.fullName);
        tv_mother_address.setText("");
        tv_mother_email.setText(mother.emailAddress);
        tv_mother_phone.setText("");
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classroom_student_profile;
    }

    void initRecycler() {
        loadEvaluations();

        mEvaluations = new ArrayList<>();
        mEvaluationList.setLayoutManager(new LinearLayoutManager(this));
        mEvaluationList.setFocusable(false);
        adapter = new SearchAdapter(this, mEvaluations);
        mEvaluationList.setAdapter(adapter);

        adapter.setListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                UserEvaluation data = mEvaluations.get(position);
                startActivity(EvaluationReview.newIntent(ClassStudentProfile.this, mData, mDataClass, data));
            }
        });
    }

    @OnClick(R.id.btn_load_more)
    protected void loadMore() {
        mBtnLoadMore.setEnabled(false);
        mBtnLoadMore.setAlpha(0.5f);
        currentPage++;
        showLoader();
        loadEvaluations();
    }

    private void showLoader() {
        mProgressBarBase.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBarBase.setVisibility(View.GONE);
    }

    private void loadEvaluations() {
        String url = UserEvaluationGetByUserId + mData.userId;
        url = url + "&SkipCount=" + (currentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        networkHelper.getRequest(url, new UserEvaluationModels(), this);
    }

    private void searchEvaluation(String value) {
        search(UserEvaluationSearch, value, new UserEvaluationModels());
    }

    private void search(String url, String value, Object model) {
        pd.show();
        hideKeyboard();
        mSearchedValue = value;

        Map<String, Object> map = new HashMap<>();
        map.put("searchText", value);
        map.put("sortOrder", String.valueOf(sort.getSelectedPosition()));
        map.put("maxResultCount", "20");
        map.put("skipCount", "0");
        networkHelper.postRequest(url, map, model, this);
        hideKeyboard();
    }

    @Override
    public void onResponse(Object object) {
        if (object instanceof UserEvaluationModels) {
            UserEvaluationModels evaluationModel = (UserEvaluationModels) object;

            if (evaluationModel.result.size() == 0 && mSearchedValue != null) {
                Common.showToast("No results found for: " + mSearchedValue);
            }

            mEvaluations.addAll(evaluationModel.result);

            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
            if (evaluationModel.result.size() == NetworkHelper.MAX_RESULTS) {
                mBtnLoadMore.setVisibility(View.VISIBLE);
            } else {
                mBtnLoadMore.setVisibility(View.GONE);
            }

            hideLoader();

            pd.dismiss();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(String msg) {
        if (mProgressBarBase.getVisibility() == View.VISIBLE) {
            Common.showToast(msg);
            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
        }
        hideLoader();
        pd.dismiss();
    }
}
