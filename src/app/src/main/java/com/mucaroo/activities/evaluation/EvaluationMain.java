package com.mucaroo.activities.evaluation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.mucaroo.R;
import com.mucaroo.activities.search.Search;
import com.mucaroo.adapters.SearchAdapter;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomHeader;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.EvaluationModel;
import com.mucaroo.models.UserEvaluation;
import com.mucaroo.models.UserEvaluationModel;
import com.mucaroo.models.UserEvaluationModels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.EvaluationGetAll;
import static com.mucaroo.helper.NetworkHelper.EvaluationGetAllMine;

/**
 * Created by ahsan on 9/6/2017.
 */

public class EvaluationMain extends AppActivity implements NetworkHelper.Listener {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    @BindView(R.id.custom_header)
    CustomHeader mCustomHeader;

    @BindView(R.id.btn_load_more)
    Button mBtnLoadMore;

    private SearchAdapter adapter;
    protected List<UserEvaluation> mEvaluations = new ArrayList<>();
    private boolean mIsEducator;
    private int currentPage = 0;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, EvaluationMain.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;
        initRecycler();


        mCustomHeader.setSearchType(Search.SEARCH_TYPE_EVALUATIONS);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_evaluation_main;
    }

    void initRecycler() {

        loadEvaluations();

        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setFocusable(false);
        adapter = new SearchAdapter(this, mEvaluations);
        recycler_view.setAdapter(adapter);

        adapter.setListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                UserEvaluation data = mEvaluations.get(position);

                if (mIsEducator) {
                    startActivity(EvaluationReviewEdu.newIntent(EvaluationMain.this, data.evaluation));
                } else {
                    startActivity(EvaluationWizard.newIntent(EvaluationMain.this, data));
                }
            }
        });
    }

    @OnClick(R.id.btn_load_more)
    protected void loadMore() {
        mBtnLoadMore.setEnabled(false);
        mBtnLoadMore.setAlpha(0.5f);
        currentPage++;
        loadEvaluations();
    }

    private void loadEvaluations() {
        showLoader();
        String url = EvaluationGetAllMine + "SkipCount=" + (currentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        networkHelper.getRequest(url, new UserEvaluationModels(), this);
    }

    private void showLoader() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(Object object) {
        if (object instanceof UserEvaluationModels) {
            UserEvaluationModels evaluationModel = (UserEvaluationModels) object;

            mEvaluations.addAll(evaluationModel.result);

            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
            if (evaluationModel.result.size() == NetworkHelper.MAX_RESULTS) {
                mBtnLoadMore.setVisibility(View.VISIBLE);
            } else {
                mBtnLoadMore.setVisibility(View.GONE);
            }

            hideLoader();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(String msg) {
        if (mProgressBar.getVisibility() == View.VISIBLE) {
            Common.showToast(msg);
            mBtnLoadMore.setEnabled(true);
            mBtnLoadMore.setAlpha(1.0f);
        }
        hideLoader();
    }

}
