package com.mucaroo.activities.classroom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.GetByEducatorId;
import static com.mucaroo.helper.NetworkHelper.StudentInvite;

/**
 * Created by ahsan on 8/24/2017.
 */

public class ClassInvite extends AppActivity {

    private static final String PARAMS_DATA = "PARAMS_DATA";

    @BindView(R.id.code_spinner)
    protected CustomSpinner mCodeSpinner;

    @BindView(R.id.profile_imageview)
    protected ImageView mProfileImageView;

    @BindView(R.id.full_name_text)
    protected TextView mFullNameText;

    @BindView(R.id.class_path)
    protected TextView mClassPath;

    @BindView(R.id.class_code_text)
    protected TextView mClassCodeText;

    @BindView(R.id.radio_group)
    protected RadioGroup mRadioGroup;

    @BindView(R.id.email_edittext)
    protected CustomEditText mEmailEdittext;

    @BindView(R.id.surname_edittext)
    protected CustomEditText mSurnameEdittext;


    @BindView(R.id.name_edittext)
    protected CustomEditText mNameEdittext;

    private CustomProgressDialog pd;

    ClassroomModel classroomModel;

    private Classroom mData;
    private String mType = "4";

    public static Intent newIntent(Context context, Classroom data) {
        Intent intent = new Intent(context, ClassInvite.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);

        pd = new CustomProgressDialog(this);

        try {
            mFullNameText.setText(mData.educator.name + " " + mData.educator.surname);
        } catch (Exception e) {
        }
        try {
            mClassPath.setText("Classrooms / " + mData.name + " / Invite");
        } catch (Exception e) {
        }
        try {
            ImageLoaderUtil.loadImageIntoView(App.getInstance(), mProfileImageView, mData.educator.profilePicture.path);
        } catch (Exception e) {
        }

        fetchData();

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.parent_radio) {
                    mType = "4";
                } else if (checkedId == R.id.student_radio) {
                    mType = "3";
                } else {
                    mType = "0";
                }
            }

        });

        mCodeSpinner.setSelectedListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                mClassCodeText.setText(classroomModel.result.get(position).classCode);
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classroom_invite;
    }

    @OnClick(R.id.btn_invite)
    protected void inviteTap() {
        final String email = mEmailEdittext.getText();
        final String surname = mSurnameEdittext.getText();
        final String name = mNameEdittext.getText();
        if (TextUtils.isEmpty(email)) {
            Common.showToast("Email address can't be empty");
            return;
        }
        if(TextUtils.isEmpty(name)) {
            Common.showToast("Name can't be empty");
            return;
        }
        if(TextUtils.isEmpty(surname)) {
            Common.showToast("Surname can't be empty");
            return;
        }
        if (!mEmailEdittext.validateEmail() && !TextUtils.isEmpty(surname) && !TextUtils.isEmpty(name)) {
            return;
        }
        String classCode = classroomModel.result.get(mCodeSpinner.getSelectedPosition()).classCode;


        String branchId = mData.educator.branchId;
        String typeId = classroomModel.result.get(mCodeSpinner.getSelectedPosition()).id;

        Map<String, Object> map = new HashMap<>();
        map.put("BranchId", branchId);
        map.put("Type", mType);
        map.put("TypeId", typeId);
        map.put("EmailAddress", email);
        map.put("Code", classCode);
        map.put("Name",name);
        map.put("Surname",surname);
        pd.show();
        hideKeyboard();
        networkHelper.postRequest(StudentInvite, map, new Object(), new NetworkHelper.Listener<Object>() {
            @Override
            public void onResponse(Object model) {
                pd.dismiss();
                Common.showToast(ClassInvite.this, "Successfully Invited " + getNameForType() + ":" + email);
            }

            @Override
            public void onFailure(String msg) {
                pd.dismiss();
            }
        });

    }

    private String getNameForType() {
        String name = "Other";
        if (mType.equalsIgnoreCase("3")) {
            name = "Student";
        } else if (mType.equalsIgnoreCase("4")) {
            name = "Parent";
        }
        return name;
    }


    @OnClick(R.id.btn_add_student)
    protected void addStudentTap() {
        startActivity(ClassAddStudent.newIntent(this, mData));
    }

    void fetchData() {
        try {
            String id = mData.educator.id;
            networkHelper.getRequest(GetByEducatorId + id, new ClassroomModel(), new NetworkHelper.Listener<ClassroomModel>() {
                @Override
                public void onResponse(ClassroomModel model) {
                    classroomModel = model;
                    ArrayList<String> classes = new ArrayList<>();
                    for (Classroom items : model.result) {
                        classes.add(items.name);
                    }
                    mCodeSpinner.setSpinner(classes);
                    if (classes.size() > 0) {
                        mClassCodeText.setText(model.result.get(0).classCode);
                    }
                }

                @Override
                public void onFailure(String msg) {

                }
            });
        } catch (Exception e) {

        }
    }


}
