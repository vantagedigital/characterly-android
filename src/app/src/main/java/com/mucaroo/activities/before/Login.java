package com.mucaroo.activities.before;

import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;

import com.mucaroo.R;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.AuthenticationModel;
import com.mucaroo.models.MyContentModel;
import com.mucaroo.models.UserInfoModel;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.Authenticate;
import static com.mucaroo.helper.NetworkHelper.GetMyContent;
import static com.mucaroo.helper.NetworkHelper.UserInfo;

/**
 * Created by Vantage on 8/1/2017.
 */

public class Login extends AppActivity {

    @BindView(R.id.username)
    CustomEditText username;

    @BindView(R.id.password)
    CustomEditText password;

    @BindView(R.id.check_remem)
    AppCompatCheckBox check_remem;

    private CustomProgressDialog pd;

    @OnClick(R.id.btn_login)
    void login() {

        if ((username.validateEmail()) && (password.validateLength(4, "Password must be minimum 4 characters"))) {

            Map<String, Object> map = new HashMap<>();
            map.put("userNameOrEmailAddress", username.getText());
            map.put("password", password.getText());
            map.put("rememberClient", check_remem.isChecked());
            pd.show();
            networkHelper.postRequest(Authenticate, map, new AuthenticationModel(), new NetworkHelper.Listener<AuthenticationModel>() {
                @Override
                public void onResponse(AuthenticationModel model) {
                    UserCache.getInstance().setUserId(model.result.userId);
                    UserCache.getInstance().setAccessToken(model.result.accessToken);
                    UserCache.getInstance().setEncryptedAccessToken(model.result.encryptedAccessToken);

                    getUserInfo();
                }

                @Override
                public void onFailure(String msg) {
                    pd.dismiss();
                }
            });

        }

    }

    private void getUserInfo() {
        networkHelper.getRequest(UserInfo, new UserInfoModel(), new NetworkHelper.Listener<UserInfoModel>() {
            @Override
            public void onResponse(UserInfoModel model) {
                UserCache.getInstance().setCurrentUser(model);
                getContentInfo();
//                Log.e("app", "saved Model:"+UserCache.getInstance().getUserInfo());
            }
            @Override
            public void onFailure(String msg) {
            }
        });
    }

    private void getContentInfo() {
//        String url = GetMyContent + "SkipCount=" + (0 * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;
        String url = GetMyContent + "SkipCount=0&MaxResultCount=100";
        networkHelper.getRequest(url, new MyContentModel(), new NetworkHelper.Listener<MyContentModel>() {
            @Override
            public void onResponse(MyContentModel model) {

                UserCache.getInstance().setContentInfo(model);

                pd.dismiss();

                startActivity(LessonMain.newIntent(Login.this, model));  // LessonMain & ProfileStudent
                finish();
            }
            @Override
            public void onFailure(String msg) {
                pd.dismiss();
            }
        });
    }

    @OnClick(R.id.btn_forgot)
    void forgot() {
        startActivity(RecoverPassword.class);
    }

    @OnClick(R.id.btn_create)
    void register() {
        startActivity(Register.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        password.makeSecure();

        pd = new CustomProgressDialog(this);

        // TODO: remove before release
//        username.setText("educator@mailer.com");
//        password.setText("111111");
//        username.setText("student@mailer.com");
//        password.setText("111111");
//        username.setText("ana_lopez@mailer.edu");
//        password.setText("Password1!");
//        username.setText("john+educator@mucaroo.com");
//        password.setText("Password1!");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

}