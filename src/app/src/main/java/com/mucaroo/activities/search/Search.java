package com.mucaroo.activities.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mucaroo.R;
import com.mucaroo.ViewModel.LessonVM;
import com.mucaroo.activities.evaluation.EvaluationMain;
import com.mucaroo.activities.evaluation.EvaluationReviewEdu;
import com.mucaroo.activities.evaluation.EvaluationWizard;
import com.mucaroo.adapters.LessonAdapter;
import com.mucaroo.adapters.SearchAdapter;
import com.mucaroo.adapters.StudentAdapter;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.DatePickerDialogFragment;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.CustomSpinner;
import com.mucaroo.helper.DateHelper;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.CompletionStringListener;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomModel;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.EvaluationModel;
import com.mucaroo.models.GradeModel;
import com.mucaroo.models.MyContentModel;
import com.mucaroo.models.MyContentResult;
import com.mucaroo.models.Pillar;
import com.mucaroo.models.PillarModel;
import com.mucaroo.models.Student;
import com.mucaroo.models.Subject;
import com.mucaroo.models.SubjectModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;
import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

import static com.mucaroo.helper.NetworkHelper.ClassroomGetAll;
import static com.mucaroo.helper.NetworkHelper.EvaluationSearch;
import static com.mucaroo.helper.NetworkHelper.GradeGetAll;
import static com.mucaroo.helper.NetworkHelper.PillarGetAll;
import static com.mucaroo.helper.NetworkHelper.SubjectGetAll;
import static com.mucaroo.helper.NetworkHelper.UserContentSearch;
import static com.mucaroo.helper.NetworkHelper.UserContentSearchMyContent;

/**
 * Created by ahsan on 8/13/2017.
 */

public class Search extends AppActivity {

    public static final String SEARCH_TYPE_EVALUATIONS = "SEARCH_TYPE_EVALUATIONS";
    public static final String SEARCH_TYPE_LESSONS = "SEARCH_TYPE_LESSONS";
    public static final String SEARCH_TYPE_CLASSROOMS = "SEARCH_TYPE_CLASSROOMS";
    public static final String SEARCH_TYPE_STUDENTS = "SEARCH_TYPE_STUDENTS";


    public static final String PARAMS_SEARCH_TYPE = "PARAMS_SEARCH_TYPE";
    public static final String PARAMS_SEARCH_DATA = "PARAMS_SEARCH_DATA";

    private static final int RC_BIRTHDATE_DIALOG = 1;
    private static String BIRTHDATE_DISPLAY_FORMAT = "MM/dd/yyyy";
    private static String BIRTHDATE_ISO_FORMAT = "yyyy-MM-dd";

    @BindView(R.id.search_results_header)
    TextView search_results_header;

    @BindView(R.id.sp_sort)
    CustomSpinner sp_sort;

    @BindView(R.id.sp_grades)
    CustomSpinner sp_grades;

    @BindView(R.id.sp_class)
    CustomSpinner sp_class;

    @BindView(R.id.sp_pillars)
    CustomSpinner sp_pillars;

    @BindView(R.id.sp_subject)
    CustomSpinner sp_subject;

    @BindView(R.id.search)
    CustomEditText mSearch;

    @BindView(R.id.date_edittext)
    EditText mDateEditText;

    @BindView(R.id.scroll_view)
    ScrollView mScrollView;

    private CustomProgressDialog pd;

    private String mSearchType, mSearchData;

    protected List<Student> mStudents = new ArrayList<>();
    protected List<Evaluation> mEvaluations = new ArrayList<>();
    private List<LessonVM> mLessonsVM = new ArrayList<>();

    StudentAdapter studentAdapter;
    SearchAdapter evalAdapter;
    LessonAdapter lessonAdapter;

    GradeModel gradeModel;
    ClassroomModel classroomModel;
    PillarModel pillarModel;
    SubjectModel subjectModel;

    @BindView(R.id.evaluation_list)
    RecyclerView search_list;
    private boolean mIsEducator;
    private String mSelectedDateISO;

    public static Intent newIntent(Context context, String searchType, String searchData) {
        Intent intent = new Intent(context, Search.class);
        intent.putExtra(PARAMS_SEARCH_TYPE, searchType);
        intent.putExtra(PARAMS_SEARCH_DATA, searchData);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSearchData = getIntent().getStringExtra(PARAMS_SEARCH_DATA);
        mSearchType = getIntent().getStringExtra(PARAMS_SEARCH_TYPE);

        mIsEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;

        pd = new CustomProgressDialog(this);

        initEditorActionListener();
        initRecycler();
        initSpinners();

        mSearch.setText(mSearchData);
        performSearch();

    }

    private void performSearch() {
        if (SEARCH_TYPE_EVALUATIONS.equalsIgnoreCase(mSearchType)) {
            mSearch.setHint("Evaluation");
            if (!TextUtils.isEmpty(mSearch.getText().toString())) {
                searchEvaluation(mSearch.getText().toString());
            }
        } else if (SEARCH_TYPE_LESSONS.equalsIgnoreCase(mSearchType)) {
            mSearch.setHint("Lessons");
            if (!TextUtils.isEmpty(mSearch.getText().toString())) {
                searchLesson(mSearch.getText().toString());
            }
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search;
    }

    void initEditorActionListener() {
        mSearch.getInput().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                performSearch();
                return true;
            }
        });
    }

    void initSpinners() {
        ArrayList<String> sortOrders = new ArrayList<>();
        sortOrders.add("Descending");
        sortOrders.add("Ascending");
        sp_sort.setSpinner(sortOrders);
//
//        String[] grades = {"First Grade", "Second Grade", "Third Grade", "Fourth Grade"};
//        sp_grades.setSpinner(grades);
//
//        String[] classes = {"All", "First Class", "Second Class"};
//        sp_class.setSpinner(classes);

    }

    void initRecycler() {

        search_list.setLayoutManager(new LinearLayoutManager(this));
        search_list.setFocusable(false);
        Common.setRowDivider(this, search_list);


        if (SEARCH_TYPE_EVALUATIONS.equalsIgnoreCase(mSearchType)) {
            evalAdapter = new SearchAdapter(this, mEvaluations);
            search_list.setAdapter(evalAdapter);

            evalAdapter.setListener(new ListItemSelectedListener() {
                @Override
                public void onListItemSelected(int position, View view) {
                    Evaluation data = mEvaluations.get(position);
                    if (mIsEducator) {
                        startActivity(EvaluationReviewEdu.newIntent(Search.this, data));
                    } else {
                        startActivity(EvaluationWizard.newIntent(Search.this, data));
                    }
                }
            });
        } else if (SEARCH_TYPE_LESSONS.equalsIgnoreCase(mSearchType)) {
            lessonAdapter = new LessonAdapter(this, mLessonsVM);
            search_list.setAdapter(lessonAdapter);
        }

        networkHelper.getRequest(GradeGetAll, new GradeModel(), new NetworkHelper.Listener() {
            @Override
            public void onResponse(Object object) {
                gradeModel = (GradeModel) object;

                ArrayList<String> data = new ArrayList<>();
                data.add("None");
                for (int i = 0; i < gradeModel.result.items.size(); i++) {
                    GradeModel.Items items = gradeModel.result.items.get(i);
                    data.add(items.name);
                }
                sp_grades.setSpinner(data);
            }

            @Override
            public void onFailure(String msg) {

            }
        });

        networkHelper.getRequest(ClassroomGetAll, new ClassroomModel(), new NetworkHelper.Listener<ClassroomModel>() {
            @Override
            public void onResponse(ClassroomModel model) {
                classroomModel = model;
                ArrayList<String> data = new ArrayList<>();
                data.add("None");
                for (Classroom items : model.result) {
                    data.add(items.name);
                }
                sp_class.setSpinner(data);
            }

            @Override
            public void onFailure(String msg) {

            }
        });
        networkHelper.getRequest(PillarGetAll, new PillarModel(), new NetworkHelper.Listener<PillarModel>() {
            @Override
            public void onResponse(PillarModel model) {
                pillarModel = model;
                ArrayList<String> data = new ArrayList<>();
                data.add("None");
                for (Pillar items : model.result) {
                    data.add(items.name);
                }
                sp_pillars.setSpinner(data);
            }

            @Override
            public void onFailure(String msg) {

            }
        });
        networkHelper.getRequest(SubjectGetAll, new SubjectModel(), new NetworkHelper.Listener<SubjectModel>() {
            @Override
            public void onResponse(SubjectModel model) {
                subjectModel = model;
                ArrayList<String> data = new ArrayList<>();
                data.add("None");
                for (Subject items : model.result) {
                    data.add(items.name);
                }
                sp_subject.setSpinner(data);
            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    private void searchEvaluation(String value) {
        search(EvaluationSearch, value, new EvaluationModel());
    }

    private void searchLesson(String value) {
        search(UserContentSearchMyContent, value, new MyContentModel());
    }

    private void search(String url, String value, Object model) {
        pd.show();
        hideKeyboard();

        Map<String, Object> map = new HashMap<>();
        map.put("searchText", value);
        map.put("sortOrder", String.valueOf(sp_sort.getSelectedPosition()));
        map.put("maxResultCount", "20");
        map.put("skipCount", "0");

        if (gradeModel != null && sp_grades.getSelectedPosition() != 0) {
            String gradeId = gradeModel.result.items.get(sp_grades.getSelectedPosition() - 1).id;
            ArrayList<String> gradeIds = new ArrayList<>();
            gradeIds.add(gradeId);
            map.put("gradeIds", new JSONArray(gradeIds));
        }

        if (classroomModel != null && sp_class.getSelectedPosition() != 0) {
            String classId = classroomModel.result.get(sp_class.getSelectedPosition() - 1).id;
            ArrayList<String> classIds = new ArrayList<>();
            classIds.add(classId);
            map.put("classIds", new JSONArray(classIds));
        }

        if (pillarModel != null && sp_pillars.getSelectedPosition() != 0) {
            String pillarId = pillarModel.result.get(sp_pillars.getSelectedPosition() - 1).id;
            ArrayList<String> pillarIds = new ArrayList<>();
            pillarIds.add(pillarId);
            map.put("pillarIds", new JSONArray(pillarIds));
        }
        if (subjectModel != null && sp_subject.getSelectedPosition() != 0) {
            String subjectId = subjectModel.result.get(sp_subject.getSelectedPosition() - 1).id;
            ArrayList<String> subjectIds = new ArrayList<>();
            subjectIds.add(subjectId);
            map.put("subjectIds", new JSONArray(subjectIds));
        }
        if (mSelectedDateISO != null) {
            map.put("date", mSelectedDateISO);
        }
//        "status": null,
//         "type":null,
        networkHelper.postRequest(url, map, model, new NetworkHelper.Listener() {
            @Override
            public void onResponse(Object object) {
                if (object instanceof EvaluationModel) {
                    EvaluationModel evaluationModel = (EvaluationModel) object;

                    if (evaluationModel.result.size() == 0) {
                        Common.showToast("No evaluations found for: " + mSearchData);
                    }

                    mEvaluations.clear();
                    mEvaluations.addAll(evaluationModel.result);
                    pd.dismiss();
                    evalAdapter.notifyDataSetChanged();

                    mScrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            mScrollView.scrollTo(0, search_results_header.getBottom() * 10);
                        }
                    });
                } else if (object instanceof MyContentModel) {
                    MyContentModel myContentModel = (MyContentModel) object;

                    if (myContentModel.result.size() == 0) {
                        Common.showToast("No lessons found for: " + mSearchData);
                    }

                    for (MyContentResult result : myContentModel.result) {
                        mLessonsVM.add(new LessonVM(result));
                    }
                    pd.dismiss();
                    lessonAdapter.notifyDataSetChanged();

                    mScrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            mScrollView.scrollTo(0, search_results_header.getBottom() * 10);
                        }
                    });
                }
            }

            @Override
            public void onFailure(String msg) {
                pd.dismiss();
            }
        });
    }

    @OnClick(R.id.date_edittext)
    protected void birthDatePress() {
        showDateDialog();
    }

    private void showDateDialog() {
        String formattedDate = getISOFormat();
        if (TextUtils.isEmpty(formattedDate)) {
            Calendar defaultDate = Calendar.getInstance();
            formattedDate = DateHelper
                    .getFormattedDate(defaultDate.getTime(), BIRTHDATE_ISO_FORMAT);
        }
        DatePickerDialogFragment dialogFragment = DatePickerDialogFragment
                .newInstance(this, "Date",
                        formattedDate, new CompletionStringListener() {
                            @Override
                            public void onCompleted(String selectedDateISO) {
                                mSelectedDateISO = selectedDateISO;
                                Calendar birthDateCal = DateHelper
                                        .getCalendarDateFromFormattedString(selectedDateISO,
                                                BIRTHDATE_ISO_FORMAT);
                                mDateEditText.setText(
                                        DateHelper.getFormattedDate(birthDateCal.getTime(),
                                                BIRTHDATE_DISPLAY_FORMAT));
                            }
                        });
        dialogFragment.show();
    }

    private String getISOFormat() {
        String formattedDate = null;
        String birthDateText = mDateEditText.getText().toString().trim();
        if (!TextUtils.isEmpty(birthDateText)) {
            Calendar calendar = DateHelper.getCalendarDateFromFormattedString(birthDateText,
                    BIRTHDATE_DISPLAY_FORMAT);
            formattedDate = DateHelper
                    .getFormattedDate(calendar.getTime(), BIRTHDATE_ISO_FORMAT);
        }
        return formattedDate;
    }
}