package com.mucaroo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mucaroo.R;
import com.mucaroo.activities.before.Login;
import com.mucaroo.activities.classroom.ClassAdd;
import com.mucaroo.activities.classroom.ClassAddStudent;
import com.mucaroo.activities.classroom.ClassHome;
import com.mucaroo.activities.classroom.ClassInvite;
import com.mucaroo.activities.classroom.ClassTimeline;
import com.mucaroo.activities.classroom.Classrooms;
import com.mucaroo.activities.evaluation.EvaluationMain;
import com.mucaroo.activities.evaluation.EvaluationReview;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.activities.profile.ProfileEducator;
import com.mucaroo.activities.profile.ProfileStudent;
import com.mucaroo.activities.profile.ProfileStudentEvaluation;
import com.mucaroo.activities.search.Search;
import com.mucaroo.helper.Common;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.educator)
    void educator() {
        Common.goTo(this, ProfileEducator.class);
    }

    @OnClick(R.id.classroom_manage)
    void classroom_manage() {
//        Common.goTo(this, ClassManage.class);
    }

    @OnClick(R.id.home)
    void home() {
        Common.goTo(this, LessonMain.class);
    }

    @OnClick(R.id.student_flow)
    void student_flow() {
        Common.goTo(this, Login.class);
    }

    @OnClick(R.id.btn_profile)
    void btn_profile() {
        Common.goTo(this, ProfileStudent.class);
    }

    @OnClick(R.id.btn_eva)
    void eva() {
        Common.goTo(this, Search.class);
    }

    @OnClick(R.id.btn_timeline)
    void btn_timeline() {
        Common.goTo(this, ClassTimeline.class);
    }

    @OnClick(R.id.btn_invite)
    void btn_invite() {
        Common.goTo(this, ClassInvite.class);
    }

    @OnClick(R.id.btn_classrooms)
    void btn_classrooms() {
        Common.goTo(this, Classrooms.class);
    }

    @OnClick(R.id.btn_classroom_home)
    void btn_classroom_home() {
        Common.goTo(this, ClassHome.class);
    }

    @OnClick(R.id.btn_student_evaluation)
    void btn_student_evaluation() {
        Common.goTo(this, ProfileStudentEvaluation.class);
    }

    @OnClick(R.id.btn_add_class)
    void btn_add_class() {
        Common.goTo(this, ClassAdd.class);
    }

    @OnClick(R.id.btn_evaluation)
    void btn_evaluation() {
        Common.goTo(this, EvaluationMain.class);
    }

    @OnClick(R.id.btn_evaluation_review)
    void btn_evaluation_review() {
        Common.goTo(this, EvaluationReview.class);
    }


    @OnClick(R.id.btn_add_student)
    void btn_add_student() {
        Common.goTo(this, ClassAddStudent.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }
}
