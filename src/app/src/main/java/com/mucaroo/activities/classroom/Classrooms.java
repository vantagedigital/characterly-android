package com.mucaroo.activities.classroom;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.adapters.ClassroomsAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Classroom;
import com.mucaroo.models.ClassroomModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.GetByEducatorId;
import static com.mucaroo.helper.NetworkHelper.GetByStudentId;

/**
 * Created by ahsan on 8/27/2017.
 */

public class Classrooms extends AppActivity {

    @BindView(R.id.full_name_text)
    protected TextView mFullNameText;

    @BindView(R.id.title_text)
    protected TextView mTitleText;

    @BindView(R.id.btn_add_class)
    protected Button mAddClassButton;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.profile_imageview)
    ImageView profile_imageview;

    @BindView(R.id.btn_load_more)
    Button mBtnLoadMore;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    private int currentPage = 0;

    private ClassroomsAdapter mAdapter;

    private List<Classroom> mData = new ArrayList<>();

    private boolean mIsEducator;

    private String mId;

    @OnClick(R.id.btn_add_class)
    void btn_add_class() {
        startActivity(ClassAdd.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;
        if (!mIsEducator) {
            mAddClassButton.setVisibility(View.GONE);
        }

        try {
            mFullNameText.setText(UserCache.getInstance().getUserInfo().result.user.name + " " + UserCache.getInstance().getUserInfo().result.user.surname);
        } catch (Exception e) {
        }

        try {
            if (mIsEducator) {
                mTitleText.setText("Teacher");
                mData.addAll(UserCache.getInstance().getUserInfo().result.user.educator.classrooms);
                mId = UserCache.getInstance().getUserInfo().result.user.educator.id;
            } else {
                mTitleText.setText("Student");
                mData.addAll(UserCache.getInstance().getUserInfo().result.user.student.classrooms);
                mId = UserCache.getInstance().getUserInfo().result.user.student.id;
            }
        } catch (Exception e) {

        }

        ImageLoaderUtil.loadImageIntoView(App.getInstance(), profile_imageview, UserCache.getInstance().getUserInfo().result.user.imagePath);

        initRecycler();
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadClassrooms(true);
    }

    @OnClick(R.id.btn_load_more)
    protected void loadMore() {
        mBtnLoadMore.setEnabled(false);
        mBtnLoadMore.setAlpha(0.5f);
        currentPage++;
        showLoader();
        loadClassrooms(false);
    }

    private void showLoader() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBar.setVisibility(View.GONE);
    }

    private void loadClassrooms(final boolean clear) {
        String url = mIsEducator ? GetByEducatorId : GetByStudentId;
        url = url + mId;
        url = url + "&SkipCount=" + (currentPage * NetworkHelper.MAX_RESULTS) + "&MaxResultCount=" + NetworkHelper.MAX_RESULTS;

        Common.logOutput(url);
        networkHelper.getRequest(url, new ClassroomModel(), new NetworkHelper.Listener<ClassroomModel>() {
            @Override
            public void onResponse(ClassroomModel model) {
                if (clear) {
                    mData.clear();
                }
                mData.addAll(model.result);
                updateCache(model.result);


                mBtnLoadMore.setEnabled(true);
                mBtnLoadMore.setAlpha(1.0f);
                if (model.result.size() == NetworkHelper.MAX_RESULTS) {
                    mBtnLoadMore.setVisibility(View.VISIBLE);
                } else {
                    mBtnLoadMore.setVisibility(View.GONE);
                }
                hideLoader();

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                if (mProgressBar.getVisibility() == View.VISIBLE) {
                    Common.showToast(msg);
                    mBtnLoadMore.setEnabled(true);
                    mBtnLoadMore.setAlpha(1.0f);
                }
                hideLoader();
            }
        });
    }

    private void updateCache(ArrayList<Classroom> result) {
        if (mIsEducator) {
            UserCache.getInstance().getUserInfo().result.user.educator.classrooms = result;
        } else {
            UserCache.getInstance().getUserInfo().result.user.student.classrooms = result;
        }
        UserCache.getInstance().setCurrentUser(UserCache.getInstance().getUserInfo());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_classrooms;
    }

    void initRecycler() {
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setFocusable(false);
        Common.setRowDivider(this, recycler_view);

        mAdapter = new ClassroomsAdapter(Classrooms.this, mData);
        recycler_view.setAdapter(mAdapter);

        mAdapter.setItemSelectedListener(new ListItemSelectedListener() {
            @Override
            public void onListItemSelected(int position, View view) {
                boolean isEducator = UserCache.getInstance().getUserInfo().result.user.educator != null;
//                if (isEducator) {
                    startActivity(ClassHome.newIntent(Classrooms.this, mData.get(position)));
//                } else {
//                    Common.showToast("Students can't view classroom settings");
//                }
            }
        });
    }

}
