package com.mucaroo.activities.evaluation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mucaroo.R;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomCheckbox;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.ListItemSelectedListener;
import com.mucaroo.models.Evaluation;
import com.mucaroo.models.EvaluationModel;
import com.mucaroo.models.MyContentResult;
import com.mucaroo.models.Question;
import com.mucaroo.models.QuestionOption;
import com.mucaroo.models.User;
import com.mucaroo.models.UserEvaluation;
import com.mucaroo.models.UserEvaluationModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.EvaluationSubmit;

/**
 * Created by ahsan on 8/12/2017.
 */

public class EvaluationWizard extends AppActivity {

    private static final String PARAMS_DATA = "PARAMS_DATA";
    private static final String PARAMS_EVALUATION = "PARAMS_EVALUATION";
    public boolean prevPressed = false;
    @BindView(R.id.eva_checkbox)
    LinearLayout eva_checkbox;

    @BindView(R.id.eva_radio)
    LinearLayout eva_radio;

    @BindView(R.id.eva_img_checkbox)
    LinearLayout eva_img_checkbox;

    @BindView(R.id.eva_edittext)
    LinearLayout eva_edittext;

    @BindView(R.id.open_edittext)
    EditText open_edittext;

    @BindView(R.id.open_img_edittext)
    CustomEditText open_img_edittext;

    @BindView(R.id.eva_et_img)
    LinearLayout eva_et_img;

    @BindView(R.id.lay_questions)
    LinearLayout lay_questions;

    @BindView(R.id.lay_done)
    LinearLayout lay_done;

    @BindView(R.id.lay_question_types)
    LinearLayout lay_question_types;

    @BindView(R.id.btn_next)
    Button mBtnNext;

    @BindView(R.id.btn_pre)
    Button mBtnPre;

    @BindView(R.id.question_text)
    TextView mQuestionText;

    @BindView(R.id.question_number)
    TextView mQuestionNumber;

    @BindView(R.id.title)
    TextView mTitle;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    int counter = 0, prevType;

    int questionsSize;

    private MyContentResult mData;

    private CustomProgressDialog pd;

    private UserEvaluation mEvaluation;

    private List<Integer> multiSelections = new ArrayList<>();
    private int singleSelection;
    private List<Map<String, Object>> mAnswers = new ArrayList<>();

    public static Intent newIntent(Context context, MyContentResult data) {
        Intent intent = new Intent(context, EvaluationWizard.class);
        intent.putExtra(PARAMS_DATA, data);
        return intent;
    }

    public static Intent newIntent(Context context, Evaluation data) {
        Intent intent = new Intent(context, EvaluationWizard.class);
        intent.putExtra(PARAMS_EVALUATION, data);
        return intent;
    }

    public static Intent newIntent(Context context, UserEvaluation data) {
        Intent intent = new Intent(context, EvaluationWizard.class);
        intent.putExtra(PARAMS_EVALUATION, data);
        return intent;
    }

    @OnClick(R.id.btn_return)
    void returnLessons() {
        finish();
    }

    @OnClick(R.id.btn_next)
    void next() {
       // counter++;
        counter += 1;
        onLoad();
    }
    void onLoad()
    {
        if (counter > 0 && questionsSize > 1) {
            mBtnPre.setVisibility(View.VISIBLE);
        }
        if (counter + 1 == questionsSize) {
            mBtnNext.setText("Submit");
        }
        else
        {
            mBtnNext.setText("Next");
        }
        if (counter > 0 && counter < questionsSize) {
            populateAnswer();
        }
        if (counter + 1 > questionsSize) {
            populateAnswer();
            disableNext();
            submitEvaluation();
            // if request fails, lay_questions.setVisibility(View.VISIBLE);
        } else {
            configureForType(mEvaluation.evaluation.questions.get(counter));
            //counter++;
        }
    }

    private void submitEvaluation() {
        String userId = UserCache.getInstance().getUserInfo().result.user.id;

        Map<String, Object> map = new HashMap<>();
        map.put("id", mEvaluation.id);
        map.put("status", "2");
        map.put("answers", new JSONArray(mAnswers));
        map.put("userId", userId);
        map.put("evaluationId", mEvaluation.evaluation.id);

        Log.e("EvaluationWizard", "map:" + map);

        pd.show();
        networkHelper.postRequest(EvaluationSubmit, map, new Object(), new NetworkHelper.Listener<Object>() {
            @Override
            public void onResponse(Object model) {
                pd.dismiss();
                doneEvaluation();
            }

            @Override
            public void onFailure(String msg) {
                pd.dismiss();
                if (msg.equals("This evaluation is aready submitted.")) {
                    {
                        finish();
                    }
                }
            }
        });
    }

    private void populateAnswer() {
        if (mEvaluation.evaluation.questions.size() == 0) {
            return;
        }
        Question question = mEvaluation.evaluation.questions.get(counter - 1);
        boolean exist = false;
        if (mAnswers.size() >= counter)
        {
            exist = true;
        }

        switch (prevType) {
            case 0: {//OpenQuestion


                Map<String, Object> map = new HashMap<>();
                map.put("answerText", open_edittext.getText().toString().trim());
                map.put("evaluationQuestionId", question.id);
                if (exist == true){
                    mAnswers.set(counter-1,map);
                }
                else
                mAnswers.add(map);
            }
            break;
            case 1: {//TrueFalse
                //TODO:? need design
                Map<String, Object> map = new HashMap<>();
                map.put("answerText", open_edittext.getText().toString().trim());
                map.put("evaluationQuestionId", question.id);
                if (exist == true){
                    mAnswers.set(counter-1,map);
                }
                else
                    mAnswers.add(map);
            }
            break;
            case 2: {//SingleChoice
                QuestionOption selectedOption = question.questionOptions.get(singleSelection);

                List<Map<String, Object>> mapNested = new ArrayList<>();
                Map<String, Object> map2 = new HashMap<>();
                map2.put("evaluationQuestionOptionId", selectedOption.id);
                mapNested.add(map2);

                Map<String, Object> map = new HashMap<>();
                map.put("evaluationQuestionId", question.id);
                map.put("selectedOptions", new JSONArray(mapNested));
                if (exist == true){
                    mAnswers.set(counter-1,map);
                }
                else
                    mAnswers.add(map);
            }
            case 3: {//MultipleChoice
                List<Map<String, Object>> mapNested = new ArrayList<>();
                for (int i = 0; i < multiSelections.size(); i++) {
                    QuestionOption selectedOption = question.questionOptions.get(multiSelections.get(i));

                    Map<String, Object> map2 = new HashMap<>();
                    map2.put("evaluationQuestionOptionId", selectedOption.id);
                    mapNested.add(map2);
                }
                Map<String, Object> map = new HashMap<>();
                map.put("evaluationQuestionId", question.id);
                map.put("selectedOptions", new JSONArray(mapNested));
                if (exist == true){
                    mAnswers.set(counter-1,map);
                }
                else
                    mAnswers.add(map);
            }
            break;
            case 4: {//SingleImageChoice
                Map<String, Object> map = new HashMap<>();
                map.put("answerText", open_img_edittext.getText().toString().trim());
                map.put("evaluationQuestionId", question.id);
                if (exist == true){
                    mAnswers.set(counter-1,map);
                }
                else
                    mAnswers.add(map);
            }
            break;
            case 5: {//MultipleImageChoice
                List<Map<String, Object>> mapNested = new ArrayList<>();
                for (int i = 0; i < multiSelections.size(); i++) {
                    QuestionOption selectedOption = question.questionOptions.get(multiSelections.get(i));

                    Map<String, Object> map2 = new HashMap<>();
                    map2.put("evaluationQuestionOptionId", selectedOption.id);
                    mapNested.add(map2);
                }
                Map<String, Object> map = new HashMap<>();
                map.put("evaluationQuestionId", question.id);
                map.put("selectedOptions", new JSONArray(mapNested));
                if (exist == true){
                    mAnswers.set(counter-1,map);
                }
                else
                    mAnswers.add(map);
            }
            break;
        }
    }

    @OnClick(R.id.btn_pre)
    void prev() {
        mBtnNext.setText("Next");
        prevPressed = true;
        counter--;

        if (counter == 1) {
            mBtnPre.setVisibility(View.INVISIBLE);
//            startEvaluation();
        }
        else
        {mBtnPre.setVisibility(View.INVISIBLE);}
        if (counter != 0) {
            configureForType(mEvaluation.evaluation.questions.get(counter));
        }
        else
        {
            counter = 0;
            configureForType(mEvaluation.evaluation.questions.get(counter));
            //counter--;
        }

    }

    private void configureForType(Question question) {
        eva_checkbox.setVisibility(View.GONE);
        eva_radio.setVisibility(View.GONE);
        eva_img_checkbox.setVisibility(View.GONE);
        eva_edittext.setVisibility(View.GONE);
        if (counter >= mAnswers.size())
            disableNext();
        else
            enableNext();

        mQuestionText.setText(question.description);
        mQuestionNumber.setText(counter + 1 + " of " + questionsSize);
        int type = question.questionType;
//        int type = 4;
        prevType = type;
        switch (type) {
            case 0://OpenQuestion
                eva_edittext.setVisibility(View.VISIBLE);
                open_edittext.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (open_edittext.getText().toString().trim().length() > 0) {
                            enableNext();
                        } else {
                            disableNext();
                        }
                    }
                });
                break;
            case 1://TrueFalse
                eva_edittext.setVisibility(View.VISIBLE);
                open_edittext.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (open_edittext.getText().toString().trim().length() > 0) {
                            enableNext();
                        } else {
                            disableNext();
                        }
                    }
                });
                //TODO:? need design
                break;
            case 2://SingleChoice
                // TODO: update prev singleselection
                singleSelection = -1;
                if (eva_radio.getChildCount() == 0) {
                    List<QuestionOption> data = new ArrayList<>();
                    for (QuestionOption option : question.questionOptions) {
                        data.add(option);
                    }
                    Common.addViewFromLayout(this, R.layout.item_evalutation_radio, eva_radio, data, new ListItemSelectedListener() {
                        @Override
                        public void onListItemSelected(int position, View view) {
                            // Reset views
                            for (int i = 0; i < eva_radio.getChildCount(); i++) {
                                View child = eva_radio.getChildAt(i);
                                Button cbButton = child.findViewById(R.id.btn_cb);
                                TextView cbText = child.findViewById(R.id.cb_text);
                                View container = child.findViewById(R.id.container);
                                container.setBackgroundColor(getResources().getColor(R.color.white));
                                cbButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.box_white));
                                cbText.setTextColor(getResources().getColor(R.color.charcoal_grey));
                            }

                            View container = view.findViewById(R.id.container);
                            container.setBackgroundColor(getResources().getColor(R.color.off_blue));
                            Button cbButton = view.findViewById(R.id.btn_cb);
                            TextView cbText = view.findViewById(R.id.cb_text);
                            cbButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.box_blue));
                            cbText.setTextColor(getResources().getColor(R.color.white));
                            singleSelection = position;
                            enableNext();
                        }
                    });
                }
                eva_radio.setVisibility(View.VISIBLE);
                break;
            case 3://MultipleChoice
                // TODO: update prev multiSelections
                if(!prevPressed) {
                    multiSelections.clear();
                }
                else
                {
                    prevPressed = false;
                }
                if (eva_checkbox.getChildCount() == 0) {
                    List<QuestionOption> data = new ArrayList<>();
                    for (QuestionOption option : question.questionOptions) {
                        data.add(option);
                    }
                    Common.addViewFromLayout(this, R.layout.item_evalutation_checkbox, eva_checkbox, data, new ListItemSelectedListener() {
                        @Override
                        public void onListItemSelected(int position, View view) {
                            if (view instanceof CustomCheckbox) {
                                if (((CustomCheckbox) view).isChecked()) {
                                    multiSelections.add(position);
                                } else {
                                    multiSelections.remove((Integer) position);
                                }
                                if (multiSelections.isEmpty()) {
                                    disableNext();
                                } else {
                                    enableNext();
                                }
                            }
                        }
                    });
                }
                eva_checkbox.setVisibility(View.VISIBLE);
                break;
            case 4://SingleImageChoice
                eva_et_img.setVisibility(View.VISIBLE);
                open_img_edittext.getInput().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (open_img_edittext.getText().trim().length() > 0) {
                            enableNext();
                        } else {
                            disableNext();
                        }
                    }
                });
                break;
            case 5://MultipleImageChoice
                // TODO: update prev multiSelections
                multiSelections.clear();
                if (eva_checkbox.getChildCount() == 0) {
                    List<QuestionOption> data = new ArrayList<>();
                    for (QuestionOption option : question.questionOptions) {
                        data.add(option);
                    }
                    Common.addViewFromLayout(this, R.layout.item_evalutation_img_checkbox, eva_img_checkbox, data, new ListItemSelectedListener() {
                        @Override
                        public void onListItemSelected(int position, View view) {
                            if (view instanceof CustomCheckbox) {
                                if (((CustomCheckbox) view).isChecked()) {
                                    multiSelections.add(position);
                                } else {
                                    multiSelections.remove((Integer) position);
                                }
                                if (multiSelections.isEmpty()) {
                                    disableNext();
                                } else {
                                    enableNext();
                                }
                            }
                        }
                    });
                }
                eva_img_checkbox.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void doneEvaluation() {
        lay_questions.setVisibility(View.GONE);
        lay_done.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = getIntent().getParcelableExtra(PARAMS_DATA);
        if (getIntent().getParcelableExtra(PARAMS_EVALUATION) instanceof UserEvaluation) {
            mEvaluation = getIntent().getParcelableExtra(PARAMS_EVALUATION);
        } else if (getIntent().getParcelableExtra(PARAMS_EVALUATION) instanceof Evaluation) {
            UserEvaluation userEvaluation = new UserEvaluation();
            userEvaluation.evaluation = getIntent().getParcelableExtra(PARAMS_EVALUATION);
            mEvaluation = userEvaluation;
        } else {
            Common.showToast("wrong data type passed in");
            finish();
        }

        pd = new CustomProgressDialog(this);

        if (mEvaluation != null) {
            startEvaluation();
            hideLoader();
        } else {
            try {
                String contentId = "3c9898d6-1e38-4608-a632-08d52c5c5b22";
                if (mData != null) {
                    contentId = mData.contentSchedule.content.id;
                }
                String url = NetworkHelper.UserEvaluationGetByUserAndContentId + UserCache.getInstance().getUserInfo().result.user.id + "&contentId=" + contentId;
                Common.logOutput(url);
                networkHelper.getRequest(url, new UserEvaluationModel(), new NetworkHelper.Listener<UserEvaluationModel>() {
                    @Override
                    public void onResponse(UserEvaluationModel evaluationModel) {
                        hideLoader();
                        Common.logOutput("result:" + evaluationModel.result);
                        mEvaluation = evaluationModel.result;
                        startEvaluation();
                    }

                    @Override
                    public void onFailure(String msg) {
                        hideLoader();
                        Common.logOutput("failure:" + msg);
                        Common.showToast(msg);
                    }
                });

            } catch (Exception e) {
                Common.showToast(e.getMessage());
                Common.logOutput("exception:" + e.getMessage());
                hideLoader();
            }
        }
    }

    private void startEvaluation() {
        mTitle.setText(mEvaluation.evaluation.name);
        questionsSize = mEvaluation.evaluation.questions.size();
        if (questionsSize > 0) {
          //  next();
        onLoad();
        }
        else
        {
            Common.showToast("No Questions in this Evaluation right now.");
            finish();
        }
    }

    private void hideLoader() {
        mProgressBar.setVisibility(View.GONE);
    }

    private void showLoader() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void enableNext() {

        mBtnNext.setEnabled(true);
        mBtnNext.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_blue));
    }

    private void disableNext() {
//        if (counter >= mAnswers.size()) {
            mBtnNext.setEnabled(false);
            mBtnNext.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_blue_disabled));
//        }
//        else {
//            mBtnNext.setText("Next");
//            mBtnNext.setEnabled(true);
//            mBtnNext.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_blue));
//        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_evaluation_wizard;
    }

}