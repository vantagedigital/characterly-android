package com.mucaroo.activities.before;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.internal.LinkedTreeMap;
import com.mucaroo.R;
import com.mucaroo.activities.lessons.LessonMain;
import com.mucaroo.application.AppActivity;
import com.mucaroo.dialogs.RegisterInfoDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomEditText;
import com.mucaroo.helper.CustomProgressDialog;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.models.CreateAccountResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.CreateAccount;
import static com.mucaroo.helper.NetworkHelper.ResetPasswordRequest;

/**
 * Created by Vantage on 8/1/2017.
 */

public class Register extends AppActivity {

    @BindView(R.id.email)
    CustomEditText email;

    @BindView(R.id.passcode)
    CustomEditText passcode;

    private CustomProgressDialog pd;

    @OnClick(R.id.btn_inf)
    void show() {
        RegisterInfoDialog dialog = new RegisterInfoDialog(this);
        dialog.show();
    }

    @OnClick(R.id.btn_enter)
    void enter() {



        if (email.validateEmail() && (passcode.validateLength(7, "Passcode must be minimum 7 characters"))) {
            Map<String, Object> map = new HashMap<>();
            map.put("EmailAddress", email.getText());
            map.put("Code", passcode.getText());
            pd = new CustomProgressDialog(this);
            pd.show();
            networkHelper.postRequest(CreateAccount, map, new CreateAccountResponse(), new NetworkHelper.Listener<CreateAccountResponse>() {
                @Override
                public void onResponse(CreateAccountResponse model) {
                    pd.dismiss();
                    //replaceActivity(CreatePassword.class);
                    //LinkedTreeMap mm = new LinkedTreeMap((Comparator) model);
                        Intent myIntent = new Intent(Register.this, CreatePassword.class);
                        myIntent.putExtra("email",email.getText().toString());
                        myIntent.putExtra("token",model.result.resetPasswordToken);
                        startActivity(myIntent);
//                        Common.showToast(Register.this, "Successfully registered");


                }

                @Override
                public void onFailure(String msg) {
                    pd.dismiss();
                }
            });

        }


    }

    @OnClick(R.id.btn_resend)
    void resend() {
        Common.goTo(this, RecoverCode.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_register;
    }


}
