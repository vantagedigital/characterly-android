package com.mucaroo.activities.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.mucaroo.R;
import com.mucaroo.adapters.NotificationAdapter;
import com.mucaroo.application.App;
import com.mucaroo.application.AppActivity;
import com.mucaroo.cache.UserCache;
import com.mucaroo.dialogs.BadgeAllDialog;
import com.mucaroo.dialogs.ParentEditDialog;
import com.mucaroo.dialogs.ProfileEditStudentDialog;
import com.mucaroo.helper.Common;
import com.mucaroo.helper.CustomBadgeLayout;
import com.mucaroo.helper.CustomNotificationBadge;
import com.mucaroo.helper.CustomRadioButton;
import com.mucaroo.helper.CustomTextView;
import com.mucaroo.helper.ImageLoaderUtil;
import com.mucaroo.helper.NetworkHelper;
import com.mucaroo.interfaces.CompletionListener;
import com.mucaroo.models.Badge;
import com.mucaroo.models.BadgeModel;
import com.mucaroo.models.FileAttachmentModel;
import com.mucaroo.models.Notification;
import com.mucaroo.models.NotificationModel;
import com.mucaroo.models.UserInfoModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

import static com.mucaroo.helper.NetworkHelper.BadgeGetByUserId;
import static com.mucaroo.helper.NetworkHelper.FileAttachmentGetByEntityId;
import static com.mucaroo.helper.NetworkHelper.NotificationGetAll;
import static com.mucaroo.helper.NetworkHelper.UserInfo;

/**
 * Created by ahsan on 8/10/2017.
 */

public class ProfileStudent extends AppActivity {

    private static final String PARAMS_IS_NOTIFICATIONS = "PARAMS_IS_NOTIFICATIONS";

    @BindView(R.id.tabs)
    CustomRadioButton tabs;

    @BindView(R.id.tv_grade)
    CustomTextView tv_grade;

    @BindView(R.id.tv_homeroom)
    CustomTextView tv_homeroom;

    @BindView(R.id.tv_dateofbirth)
    CustomTextView tv_dateofbirth;

    @BindView(R.id.tv_gender)
    CustomTextView tv_gender;

    @BindView(R.id.tv_school_address)
    CustomTextView tv_school_address;

    @BindView(R.id.tv_email)
    CustomTextView tv_email;

    @BindView(R.id.tv_phone)
    CustomTextView tv_phone;

    @BindView(R.id.name_text)
    TextView mNameText;

    @BindView(R.id.id_text)
    TextView mIdText;

    @BindView(R.id.tv_father_name)
    TextView tv_father_name;

    @BindView(R.id.tv_father_address)
    CustomTextView tv_father_address;

    @BindView(R.id.tv_father_email)
    CustomTextView tv_father_email;

    @BindView(R.id.tv_father_phone)
    CustomTextView tv_father_phone;

    @BindView(R.id.tv_mother_name)
    TextView tv_mother_name;

    @BindView(R.id.tv_mother_address)
    CustomTextView tv_mother_address;

    @BindView(R.id.tv_mother_email)
    CustomTextView tv_mother_email;

    @BindView(R.id.tv_mother_phone)
    CustomTextView tv_mother_phone;

    @BindView(R.id.profile_imageview)
    ImageView profile_imageview;

    @BindView(R.id.badge1)
    CustomBadgeLayout mBadge1;

    @BindView(R.id.badge2)
    CustomBadgeLayout mBadge2;

    @BindView(R.id.badge3)
    CustomBadgeLayout mBadge3;

    @BindView(R.id.badge4)
    CustomBadgeLayout mBadge4;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    @BindView(R.id.badge_counter2)
    CustomNotificationBadge badge_counter;

    private List<Notification> notifications;
    private NotificationAdapter adapter;
    private boolean mIsNotificatons;

    private ProfileEditStudentDialog profileEditStudentDialog;

    @OnClick(R.id.btn_badge_all)
    void btn_badge_all() {
        BadgeAllDialog dialog = new BadgeAllDialog(this);
        dialog.show();
    }

    @BindView(R.id.notification_list)
    RecyclerView notification_list;

    @BindViews({R.id.lay_profile, R.id.lay_notifications})
    LinearLayout[] lay_content;

    @OnClick(R.id.btn_profile_edit)
    void profile_edit() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            profileEditStudentDialog = new ProfileEditStudentDialog(this, null, UserCache.getInstance().getUserInfo().result.user.student);
            profileEditStudentDialog.show();
            profileEditStudentDialog.setCompletionListener(new CompletionListener() {
                @Override
                public void onCompleted() {
                    updateProfilePic();
                }
            });
        }
    }

    private void updateBadgeCounter() {
        int count = 0;
        if (UserCache.getInstance().getNotifications() != null) {
            for (Notification notification : UserCache.getInstance().getNotifications().result) {
                if (!notification.isRead) {
                    count++;
                }
            }
        }
        badge_counter.setCounter(count);
    }

    @OnClick(R.id.btn_parent_edit)
    void parent_edit() {
        if (UserCache.getInstance().getUserInfo().result.user.student.parents != null && UserCache.getInstance().getUserInfo().result.user.student.parents.size() > 0) {
            ParentEditDialog dialog = new ParentEditDialog(this);
            dialog.show();
        } else {
            Common.showToast("No parents to edit.");
        }
    }

    public static Intent newIntent(Context context) {
        return newIntent(context, false);
    }

    public static Intent newIntent(Context context, boolean isNotifications) {
        Intent intent = new Intent(context, ProfileStudent.class);
        intent.putExtra(PARAMS_IS_NOTIFICATIONS, isNotifications);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsNotificatons = getIntent().getBooleanExtra(PARAMS_IS_NOTIFICATIONS, false);

        tabs.setupVisibility(lay_content);

        if (mIsNotificatons) {
            tabs.performTabClick(1);
        }

        initRecycler();

        resetUI();
        updateUI();

        if (UserCache.getInstance().getNotifications() != null && UserCache.getInstance().getNotifications().result != null) {
            notifications.clear();
            notifications.addAll(UserCache.getInstance().getNotifications().result);
        }
        configureBadges();

        if (TextUtils.isEmpty(UserCache.getInstance().getUserInfo().result.user.imagePath)) {
            networkHelper.getRequest(FileAttachmentGetByEntityId + UserCache.getInstance().getUserInfo().result.user.id, new FileAttachmentModel(), new NetworkHelper.Listener<FileAttachmentModel>() {
                @Override
                public void onResponse(FileAttachmentModel model) {
                    try {
                        UserInfoModel userModel = UserCache.getInstance().getUserInfo();
                        userModel.result.user.imagePath = model.result.get(model.result.size() - 1).path;
                        UserCache.getInstance().setCurrentUser(userModel);


                        updateProfilePic();
                    } catch (Exception e) {
                        Log.e("ProfileStudent", "error saving profile image");
                    }
                }

                @Override
                public void onFailure(String msg) {
                }
            });
        } else {
            updateProfilePic();
        }
        updateBadgeCounter();
    }

    private void getNotifications() {
        networkHelper.getRequest(NotificationGetAll, new NotificationModel(), new NetworkHelper.Listener<NotificationModel>() {
            @Override
            public void onResponse(NotificationModel model) {
                UserCache.getInstance().setNotifications(model);
                notifications.clear();
                notifications.addAll(model.result);
                updateBadgeCounter();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
//                        Common.showToast("error getting notifications");
            }
        });
    }

    private void updateUI() {
        if (UserCache.getInstance().getUserInfo().result.user.student.parents != null && UserCache.getInstance().getUserInfo().result.user.student.parents.size() > 0) {
            configureFather(UserCache.getInstance().getUserInfo().result.user.student.parents.get(0));

            if (UserCache.getInstance().getUserInfo().result.user.student.parents.size() > 1) {
                configureMother(UserCache.getInstance().getUserInfo().result.user.student.parents.get(1));
            }
        }

        try {
            tv_email.setText(UserCache.getInstance().getUserInfo().result.user.emailAddress);
        } catch (Exception e) {
        }
        tv_phone.setText(" ");

        try {
            mNameText.setText(UserCache.getInstance().getUserInfo().result.user.name + " " + UserCache.getInstance().getUserInfo().result.user.surname);
        } catch (Exception e) {
        }
        try {
            mIdText.setText(UserCache.getInstance().getUserInfo().result.user.id);
        } catch (Exception e) {
        }
    }

    private void configureBadges() {
        mProgress.setVisibility(View.VISIBLE);
        mBadge1.setVisibility(View.GONE);
        mBadge2.setVisibility(View.GONE);
        mBadge3.setVisibility(View.GONE);
        mBadge4.setVisibility(View.GONE);
        networkHelper.getRequest(BadgeGetByUserId + UserCache.getInstance().getUserInfo().result.user.id, new BadgeModel(), new NetworkHelper.Listener<BadgeModel>() {
            @Override
            public void onResponse(BadgeModel model) {
                UserCache.getInstance().setBadgeInfo(model);
                mProgress.setVisibility(View.GONE);
                try {
                    for (int i = 0; i < model.result.size() && i < 4; i++) {
                        CustomBadgeLayout badgeView = getBadgeFor(i);
                        Badge badge = model.result.get(i);
                        badgeView.setVisibility(View.VISIBLE);
                        badgeView.setBadge(badge);
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String msg) {
                mProgress.setVisibility(View.GONE);
            }
        });
    }

    private CustomBadgeLayout getBadgeFor(int i) {
        if (i == 0) {
            return mBadge1;
        } else if (i == 1) {
            return mBadge2;
        } else if (i == 2) {
            return mBadge3;
        } else {
            return mBadge4;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateProfilePic();
        updateData();
        getNotifications();
    }

    private void updateData() {
            networkHelper.getRequest(UserInfo, new UserInfoModel(), new NetworkHelper.Listener<UserInfoModel>() {
                @Override
                public void onResponse(UserInfoModel model) {
                    UserCache.getInstance().setCurrentUser(model);
                    updateUI();
                }
                @Override
                public void onFailure(String msg) {
                }
            });
    }

    private void updateProfilePic() {
        ImageLoaderUtil.loadImageIntoView(App.getInstance(), profile_imageview, UserCache.getInstance().getUserInfo().result.user.imagePath);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    profile_edit();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void resetUI() {
        tv_grade.setText(" ");
        tv_homeroom.setText(" ");
        tv_dateofbirth.setText(" ");
        tv_gender.setText(" ");
        tv_school_address.setText(" ");

        tv_father_name.setText("");
        tv_father_address.setText("");
        tv_father_email.setText("");
        tv_father_phone.setText("");
        tv_mother_name.setText("");
        tv_mother_address.setText("");
        tv_mother_email.setText("");
        tv_mother_phone.setText("");
    }

    private void configureFather(UserInfoModel.Parent father) {
        tv_father_name.setText(father.fullName);
        tv_father_address.setText("");
        tv_father_email.setText(father.emailAddress);
        tv_father_phone.setText("");
    }

    private void configureMother(UserInfoModel.Parent mother) {
        tv_mother_name.setText(mother.fullName);
        tv_mother_address.setText("");
        tv_mother_email.setText(mother.emailAddress);
        tv_mother_phone.setText("");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile_student;
    }

    void initRecycler() {
        notifications = new ArrayList<>();
        notification_list.setLayoutManager(new LinearLayoutManager(this));
        notification_list.setFocusable(false);
        adapter = new NotificationAdapter(this, notifications);
        notification_list.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (profileEditStudentDialog.getImagePicker() == null) {
                    profileEditStudentDialog.setImagePicker(new ImagePicker(this));
                    profileEditStudentDialog.getImagePicker().setImagePickerCallback(profileEditStudentDialog.getImagePickerCallback());
                }
                profileEditStudentDialog.getImagePicker().submit(data);
            }
        }
    }
}