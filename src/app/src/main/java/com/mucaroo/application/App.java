package com.mucaroo.application;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.androidnetworking.AndroidNetworking;
import com.mucaroo.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by ahsan on 8/7/2017.
 */
// App mApplication = (App)getApplicationContext();

public class App extends Application {
    private static App app;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    public static App getInstance() {
        return app;
    }

    public App() {
        app = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initFonts();
        AndroidNetworking.initialize(getApplicationContext());

    }

    private void initFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }


}