package com.mucaroo.application;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.mucaroo.helper.NetworkHelper;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ahsan on 9/13/2017.
 */

public abstract class AppActivity extends AppCompatActivity {

    public NetworkHelper networkHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        networkHelper = new NetworkHelper(this);
    }

    protected abstract int getLayoutResourceId();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void startActivity(Class target) {
        Intent intent = new Intent(this, target);
        startActivity(intent);
    }

    public void replaceActivity(Class target) {
        startActivity(target);
        finish();
    }

    protected void hideKeyboard() {
        hideKeyboard(getCurrentFocus());
    }

    protected void hideKeyboard(View view) {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view != null) {
            view.clearFocus();
            inputManager
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
