package com.mucaroo.application;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;
import android.view.WindowManager;

import com.mucaroo.R;
import com.mucaroo.helper.Common;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by ahsan on 9/16/2017.
 */

public abstract class AppDialog extends AppCompatDialog {

    @Optional
    @OnClick(R.id.btn_close)
    protected void close() {
        dismiss();
    }

    public AppDialog(@NonNull Context context) {
        super(context, R.style.DialogStyle);
        setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        setLayout(context);


    }

    private void setLayout(Context context) {
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.95);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.95);

        getWindow().setLayout(width, height);

    }

    protected abstract int getLayoutResourceId();

}
