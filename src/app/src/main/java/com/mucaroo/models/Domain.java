package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Domain implements Parcelable {
    public String parentDomainId;
    public String code;
    public String name;
    public Integer domainCategory;
    public String id;

    @Override
    public String toString() {
        return "Domain{" +
                "parentDomainId='" + parentDomainId + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", domainCategory=" + domainCategory +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.parentDomainId);
        dest.writeString(this.code);
        dest.writeString(this.name);
        dest.writeValue(this.domainCategory);
        dest.writeString(this.id);
    }

    public Domain() {
    }

    protected Domain(Parcel in) {
        this.parentDomainId = in.readString();
        this.code = in.readString();
        this.name = in.readString();
        this.domainCategory = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Domain> CREATOR = new Parcelable.Creator<Domain>() {
        @Override
        public Domain createFromParcel(Parcel source) {
            return new Domain(source);
        }

        @Override
        public Domain[] newArray(int size) {
            return new Domain[size];
        }
    };
}
