package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Student implements Parcelable {
//    public UserInfoModel.School school;
    public List<Classroom> classrooms;
    public String id;
    public List<UserInfoModel.Parent> parents;
    public List<Badge> badges;
    public String userName;
    public String fullName;
    public String emailAddress;
    public String surname;
    public String name;
    public int userId;
    public String schoolId;
    public boolean isActive;
    public String branchId;
    public String creationTime;
    public String lastLoginTime;
    public FileAttachment profilePicture;

    // extra field not in response
    public String imagePath;

    @Override
    public String toString() {
        return "Student{" +
                "classrooms=" + classrooms +
                ", id='" + id + '\'' +
                ", parents=" + parents +
                ", badges=" + badges +
                ", userName='" + userName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", userId=" + userId +
                ", schoolId='" + schoolId + '\'' +
                ", isActive=" + isActive +
                ", branchId='" + branchId + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", lastLoginTime='" + lastLoginTime + '\'' +
                ", profilePicture=" + profilePicture +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }

    public Student() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.classrooms);
        dest.writeString(this.id);
        dest.writeList(this.parents);
        dest.writeTypedList(this.badges);
        dest.writeString(this.userName);
        dest.writeString(this.fullName);
        dest.writeString(this.emailAddress);
        dest.writeString(this.surname);
        dest.writeString(this.name);
        dest.writeInt(this.userId);
        dest.writeString(this.schoolId);
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeString(this.branchId);
        dest.writeString(this.creationTime);
        dest.writeString(this.lastLoginTime);
        dest.writeParcelable(this.profilePicture, flags);
        dest.writeString(this.imagePath);
    }

    protected Student(Parcel in) {
        this.classrooms = in.createTypedArrayList(Classroom.CREATOR);
        this.id = in.readString();
        this.parents = new ArrayList<UserInfoModel.Parent>();
        in.readList(this.parents, UserInfoModel.Parent.class.getClassLoader());
        this.badges = in.createTypedArrayList(Badge.CREATOR);
        this.userName = in.readString();
        this.fullName = in.readString();
        this.emailAddress = in.readString();
        this.surname = in.readString();
        this.name = in.readString();
        this.userId = in.readInt();
        this.schoolId = in.readString();
        this.isActive = in.readByte() != 0;
        this.branchId = in.readString();
        this.creationTime = in.readString();
        this.lastLoginTime = in.readString();
        this.profilePicture = in.readParcelable(FileAttachment.class.getClassLoader());
        this.imagePath = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel source) {
            return new Student(source);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };
}
