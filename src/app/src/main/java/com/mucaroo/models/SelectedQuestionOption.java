package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class SelectedQuestionOption implements Parcelable {
    public String evaluationQuestionOptionId;

    @Override
    public String toString() {
        return "SelectedQuestionOption{" +
                "evaluationQuestionOptionId='" + evaluationQuestionOptionId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SelectedQuestionOption that = (SelectedQuestionOption) o;

        return evaluationQuestionOptionId != null ? evaluationQuestionOptionId.equals(that.evaluationQuestionOptionId) : that.evaluationQuestionOptionId == null;
    }

    @Override
    public int hashCode() {
        return evaluationQuestionOptionId != null ? evaluationQuestionOptionId.hashCode() : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.evaluationQuestionOptionId);
    }

    public SelectedQuestionOption() {
    }

    protected SelectedQuestionOption(Parcel in) {
        this.evaluationQuestionOptionId = in.readString();
    }

    public static final Creator<SelectedQuestionOption> CREATOR = new Creator<SelectedQuestionOption>() {
        @Override
        public SelectedQuestionOption createFromParcel(Parcel source) {
            return new SelectedQuestionOption(source);
        }

        @Override
        public SelectedQuestionOption[] newArray(int size) {
            return new SelectedQuestionOption[size];
        }
    };
}
