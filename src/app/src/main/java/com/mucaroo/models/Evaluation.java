package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Evaluation implements Parcelable {
    public String branchId;
    public String classroomId;
    public String contentId;
    public String name;
    public int status;
    public List<Question> questions;
//    public List<UserEvaluation> userEvaluations;
    public String id;


    @Override
    public String toString() {
        return "Evaluation{" +
                "branchId='" + branchId + '\'' +
                ", classroomId='" + classroomId + '\'' +
                ", contentId='" + contentId + '\'' +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", questions=" + questions +
//                ", userEvaluations=" + userEvaluations +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.classroomId);
        dest.writeString(this.contentId);
        dest.writeString(this.name);
        dest.writeInt(this.status);
        dest.writeList(this.questions);
//        dest.writeParcelable(this.userEvaluations, flags);
        dest.writeString(this.id);
    }

    public Evaluation() {
    }

    protected Evaluation(Parcel in) {
        this.branchId = in.readString();
        this.classroomId = in.readString();
        this.contentId = in.readString();
        this.name = in.readString();
        this.status = in.readInt();
        this.questions = new ArrayList<Question>();
        in.readList(this.questions, Question.class.getClassLoader());
//        this.userEvaluations = in.readParcelable(UserEvaluation.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Evaluation> CREATOR = new Parcelable.Creator<Evaluation>() {
        @Override
        public Evaluation createFromParcel(Parcel source) {
            return new Evaluation(source);
        }

        @Override
        public Evaluation[] newArray(int size) {
            return new Evaluation[size];
        }
    };
}
