package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Classroom implements Parcelable {
    public String branchId;
    public String gradeId;
    public String name;
    public String colorCode;
    public String classCode;
    public String motto;
//    public UserInfoModel.School school;
    public Educator educator;
    public String id;
    public Grade grade;
    public List<Student> students;

    // extra field not in response
    public String imagePath;

    @Override
    public String toString() {
        return "Classroom{" +
                "branchId='" + branchId + '\'' +
                ", gradeId='" + gradeId + '\'' +
                ", name='" + name + '\'' +
                ", colorCode='" + colorCode + '\'' +
                ", classCode='" + classCode + '\'' +
                ", motto='" + motto + '\'' +
                ", educator=" + educator +
                ", id='" + id + '\'' +
                ", grade=" + grade +
                ", students=" + students +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }

    public Classroom() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.gradeId);
        dest.writeString(this.name);
        dest.writeString(this.colorCode);
        dest.writeString(this.classCode);
        dest.writeString(this.motto);
        dest.writeParcelable(this.educator, flags);
        dest.writeString(this.id);
        dest.writeParcelable(this.grade, flags);
        dest.writeTypedList(this.students);
        dest.writeString(this.imagePath);
    }

    protected Classroom(Parcel in) {
        this.branchId = in.readString();
        this.gradeId = in.readString();
        this.name = in.readString();
        this.colorCode = in.readString();
        this.classCode = in.readString();
        this.motto = in.readString();
        this.educator = in.readParcelable(Educator.class.getClassLoader());
        this.id = in.readString();
        this.grade = in.readParcelable(Grade.class.getClassLoader());
        this.students = in.createTypedArrayList(Student.CREATOR);
        this.imagePath = in.readString();
    }

    public static final Creator<Classroom> CREATOR = new Creator<Classroom>() {
        @Override
        public Classroom createFromParcel(Parcel source) {
            return new Classroom(source);
        }

        @Override
        public Classroom[] newArray(int size) {
            return new Classroom[size];
        }
    };
}