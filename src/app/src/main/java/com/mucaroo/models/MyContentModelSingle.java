package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MyContentModelSingle implements Parcelable {

    public MyContentResult result;

    @Override
    public String toString() {
        return "MyContentModelSingle{" +
                "result=" + result +
                '}';
    }

    public MyContentModelSingle() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.result, flags);
    }

    protected MyContentModelSingle(Parcel in) {
        this.result = in.readParcelable(MyContentResult.class.getClassLoader());
    }

    public static final Creator<MyContentModelSingle> CREATOR = new Creator<MyContentModelSingle>() {
        @Override
        public MyContentModelSingle createFromParcel(Parcel source) {
            return new MyContentModelSingle(source);
        }

        @Override
        public MyContentModelSingle[] newArray(int size) {
            return new MyContentModelSingle[size];
        }
    };
}
