package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Educator implements Parcelable {
//    public UserInfoModel.School school;
    public List<Classroom> classrooms;
    public String id;
    public String branchId;
    public String schoolId;
    public String userId;
    public String userName;
    public String name;
    public String surname;
    public String emailAddress;
    public boolean isActive;
    public FileAttachment profilePicture;

    @Override
    public String toString() {
        return "Educator{" +
                "classrooms=" + classrooms +
                ", id='" + id + '\'' +
                ", branchId='" + branchId + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", isActive=" + isActive +
                ", profilePicture=" + profilePicture +
                '}';
    }


    public Educator() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.classrooms);
        dest.writeString(this.id);
        dest.writeString(this.branchId);
        dest.writeString(this.schoolId);
        dest.writeString(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.emailAddress);
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.profilePicture, flags);
    }

    protected Educator(Parcel in) {
        this.classrooms = in.createTypedArrayList(Classroom.CREATOR);
        this.id = in.readString();
        this.branchId = in.readString();
        this.schoolId = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.name = in.readString();
        this.surname = in.readString();
        this.emailAddress = in.readString();
        this.isActive = in.readByte() != 0;
        this.profilePicture = in.readParcelable(FileAttachment.class.getClassLoader());
    }

    public static final Creator<Educator> CREATOR = new Creator<Educator>() {
        @Override
        public Educator createFromParcel(Parcel source) {
            return new Educator(source);
        }

        @Override
        public Educator[] newArray(int size) {
            return new Educator[size];
        }
    };
}
