package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class TimelineComment implements Parcelable {
    public String timelineId;
    public String parentCommentId;
    public String contentScheduleId;
    public String commentText;
    public String creationTime;
    public Student user;
    public ContentSchedule contentSchedule;
    public Evaluation evaluation;
    public List<FileAttachment> attachments;
    public String id;

    @Override
    public String toString() {
        return "TimelineComment{" +
                "timelineId='" + timelineId + '\'' +
                ", parentCommentId='" + parentCommentId + '\'' +
                ", contentScheduleId='" + contentScheduleId + '\'' +
                ", commentText='" + commentText + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", user=" + user +
                ", contentSchedule=" + contentSchedule +
                ", evaluation=" + evaluation +
                ", attachments=" + attachments +
                ", id='" + id + '\'' +
                '}';
    }


    public TimelineComment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.timelineId);
        dest.writeString(this.parentCommentId);
        dest.writeString(this.contentScheduleId);
        dest.writeString(this.commentText);
        dest.writeString(this.creationTime);
        dest.writeParcelable(this.user, flags);
        dest.writeParcelable(this.contentSchedule, flags);
        dest.writeParcelable(this.evaluation, flags);
        dest.writeTypedList(this.attachments);
        dest.writeString(this.id);
    }

    protected TimelineComment(Parcel in) {
        this.timelineId = in.readString();
        this.parentCommentId = in.readString();
        this.contentScheduleId = in.readString();
        this.commentText = in.readString();
        this.creationTime = in.readString();
        this.user = in.readParcelable(Student.class.getClassLoader());
        this.contentSchedule = in.readParcelable(ContentSchedule.class.getClassLoader());
        this.evaluation = in.readParcelable(Evaluation.class.getClassLoader());
        this.attachments = in.createTypedArrayList(FileAttachment.CREATOR);
        this.id = in.readString();
    }

    public static final Creator<TimelineComment> CREATOR = new Creator<TimelineComment>() {
        @Override
        public TimelineComment createFromParcel(Parcel source) {
            return new TimelineComment(source);
        }

        @Override
        public TimelineComment[] newArray(int size) {
            return new TimelineComment[size];
        }
    };
}
