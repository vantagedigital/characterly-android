package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class FileAttachment implements Parcelable {
    public String entityId;
    public String fileName;
    public String path;
    public Integer category;
    public Integer type;
    public String id;
    public boolean isDeleted;
    public String creationTime;

    @Override
    public String toString() {
        return "FileAttachment{" +
                "entityId='" + entityId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", path='" + path + '\'' +
                ", category=" + category +
                ", type=" + type +
                ", id='" + id + '\'' +
                ", isDeleted=" + isDeleted +
                ", creationTime='" + creationTime + '\'' +
                '}';
    }


    public FileAttachment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.entityId);
        dest.writeString(this.fileName);
        dest.writeString(this.path);
        dest.writeValue(this.category);
        dest.writeValue(this.type);
        dest.writeString(this.id);
        dest.writeByte(this.isDeleted ? (byte) 1 : (byte) 0);
        dest.writeString(this.creationTime);
    }

    protected FileAttachment(Parcel in) {
        this.entityId = in.readString();
        this.fileName = in.readString();
        this.path = in.readString();
        this.category = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = in.readString();
        this.isDeleted = in.readByte() != 0;
        this.creationTime = in.readString();
    }

    public static final Creator<FileAttachment> CREATOR = new Creator<FileAttachment>() {
        @Override
        public FileAttachment createFromParcel(Parcel source) {
            return new FileAttachment(source);
        }

        @Override
        public FileAttachment[] newArray(int size) {
            return new FileAttachment[size];
        }
    };
}
