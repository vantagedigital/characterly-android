package com.mucaroo.models;

/**
 * Created by ahsan on 10/8/2017.
 */

public class AuthenticationModel {

    public Result result;
    public class Result {
        public String encryptedAccessToken;
        public String accessToken;
        public String userId;

    }

}

