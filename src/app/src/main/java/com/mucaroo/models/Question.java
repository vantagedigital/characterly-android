package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Question implements Parcelable {
    public String evaluationId;
    public String fileAttachmentId;
    public int questionType;
    public String description;
    public int value;
    public int position;
    public FileAttachment fileAttachment;
    public String id;
    public List<QuestionOption> questionOptions;

    @Override
    public String toString() {
        return "Question{" +
                "evaluationId='" + evaluationId + '\'' +
                ", fileAttachmentId='" + fileAttachmentId + '\'' +
                ", questionType=" + questionType +
                ", description='" + description + '\'' +
                ", value=" + value +
                ", position=" + position +
                ", fileAttachment=" + fileAttachment +
                ", id='" + id + '\'' +
                ", questionOptions=" + questionOptions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Question question = (Question) o;

        if (questionType != question.questionType) return false;
        if (value != question.value) return false;
        if (position != question.position) return false;
        if (evaluationId != null ? !evaluationId.equals(question.evaluationId) : question.evaluationId != null)
            return false;
        if (fileAttachmentId != null ? !fileAttachmentId.equals(question.fileAttachmentId) : question.fileAttachmentId != null)
            return false;
        if (description != null ? !description.equals(question.description) : question.description != null)
            return false;
        if (fileAttachment != null ? !fileAttachment.equals(question.fileAttachment) : question.fileAttachment != null)
            return false;
        if (id != null ? !id.equals(question.id) : question.id != null) return false;
        return questionOptions != null ? questionOptions.equals(question.questionOptions) : question.questionOptions == null;
    }

    @Override
    public int hashCode() {
        int result = evaluationId != null ? evaluationId.hashCode() : 0;
        result = 31 * result + (fileAttachmentId != null ? fileAttachmentId.hashCode() : 0);
        result = 31 * result + questionType;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + value;
        result = 31 * result + position;
        result = 31 * result + (fileAttachment != null ? fileAttachment.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (questionOptions != null ? questionOptions.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.evaluationId);
        dest.writeString(this.fileAttachmentId);
        dest.writeInt(this.questionType);
        dest.writeString(this.description);
        dest.writeInt(this.value);
        dest.writeInt(this.position);
        dest.writeParcelable(this.fileAttachment, flags);
        dest.writeString(this.id);
        dest.writeList(this.questionOptions);
    }

    public Question() {
    }

    protected Question(Parcel in) {
        this.evaluationId = in.readString();
        this.fileAttachmentId = in.readString();
        this.questionType = in.readInt();
        this.description = in.readString();
        this.value = in.readInt();
        this.position = in.readInt();
        this.fileAttachment = in.readParcelable(FileAttachment.class.getClassLoader());
        this.id = in.readString();
        this.questionOptions = new ArrayList<QuestionOption>();
        in.readList(this.questionOptions, QuestionOption.class.getClassLoader());
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
