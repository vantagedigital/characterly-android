package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Pillar implements Parcelable {
    public String branchId;
    public String name;
    public String description;
    public String colorCode;
    public List<MyContentModel.Image> images;
    public String id;

    @Override
    public String toString() {
        return "Pillar{" +
                "branchId='" + branchId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", colorCode='" + colorCode + '\'' +
                ", images=" + images +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.colorCode);
        dest.writeList(this.images);
        dest.writeString(this.id);
    }

    public Pillar() {
    }

    protected Pillar(Parcel in) {
        this.branchId = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.colorCode = in.readString();
        this.images = new ArrayList<MyContentModel.Image>();
        in.readList(this.images, MyContentModel.Image.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Pillar> CREATOR = new Parcelable.Creator<Pillar>() {
        @Override
        public Pillar createFromParcel(Parcel source) {
            return new Pillar(source);
        }

        @Override
        public Pillar[] newArray(int size) {
            return new Pillar[size];
        }
    };
}
