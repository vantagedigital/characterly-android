package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MyContentResult implements Parcelable {
    public String contentScheduleId;
    public String userId;
    public Integer status;
    public Double progressPercentage;
    public ContentSchedule contentSchedule;
    public User user;
    public String id;

    @Override
    public String toString() {
        return "MyContentResult{" +
                "contentScheduleId='" + contentScheduleId + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", progressPercentage=" + progressPercentage +
                ", contentSchedule=" + contentSchedule +
                ", user=" + user +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contentScheduleId);
        dest.writeString(this.userId);
        dest.writeValue(this.status);
        dest.writeValue(this.progressPercentage);
        dest.writeParcelable(this.contentSchedule, flags);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.id);
    }

    public MyContentResult() {
    }

    protected MyContentResult(Parcel in) {
        this.contentScheduleId = in.readString();
        this.userId = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.progressPercentage = (Double) in.readValue(Double.class.getClassLoader());
        this.contentSchedule = in.readParcelable(ContentSchedule.class.getClassLoader());
        this.user = in.readParcelable(User.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<MyContentResult> CREATOR = new Parcelable.Creator<MyContentResult>() {
        @Override
        public MyContentResult createFromParcel(Parcel source) {
            return new MyContentResult(source);
        }

        @Override
        public MyContentResult[] newArray(int size) {
            return new MyContentResult[size];
        }
    };
}
