package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    public String name;
    public String surname;
    public String userName;
    public String fullName;
    public String lastLoginTime;
    public String creationTime;
    public String emailAddress;
    public String branchId;
    public Boolean isActive;
    public Student student;
    public Educator educator;
    public String id;
    // extra field not in response
    public String imagePath;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", userName='" + userName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", lastLoginTime='" + lastLoginTime + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", branchId='" + branchId + '\'' +
                ", isActive=" + isActive +
                ", student=" + student +
                ", educator=" + educator +
                ", id='" + id + '\'' +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }


    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.userName);
        dest.writeString(this.fullName);
        dest.writeString(this.lastLoginTime);
        dest.writeString(this.creationTime);
        dest.writeString(this.emailAddress);
        dest.writeString(this.branchId);
        dest.writeValue(this.isActive);
        dest.writeParcelable(this.student, flags);
        dest.writeParcelable(this.educator, flags);
        dest.writeString(this.id);
        dest.writeString(this.imagePath);
    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.surname = in.readString();
        this.userName = in.readString();
        this.fullName = in.readString();
        this.lastLoginTime = in.readString();
        this.creationTime = in.readString();
        this.emailAddress = in.readString();
        this.branchId = in.readString();
        this.isActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.student = in.readParcelable(Student.class.getClassLoader());
        this.educator = in.readParcelable(Educator.class.getClassLoader());
        this.id = in.readString();
        this.imagePath = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
