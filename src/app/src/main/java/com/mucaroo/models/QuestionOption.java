package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionOption implements Parcelable {
    public String evaluationQuestionId;
    public String fileAttachmentId;
    public String description;
    public int position;
    public FileAttachment fileAttachment;
    public String id;

    @Override
    public String toString() {
        return "QuestionOption{" +
                "evaluationQuestionId='" + evaluationQuestionId + '\'' +
                ", fileAttachmentId='" + fileAttachmentId + '\'' +
                ", description='" + description + '\'' +
                ", position=" + position +
                ", fileAttachment=" + fileAttachment +
                ", id='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionOption that = (QuestionOption) o;

        if (position != that.position) return false;
        if (evaluationQuestionId != null ? !evaluationQuestionId.equals(that.evaluationQuestionId) : that.evaluationQuestionId != null)
            return false;
        if (fileAttachmentId != null ? !fileAttachmentId.equals(that.fileAttachmentId) : that.fileAttachmentId != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null)
            return false;
        if (fileAttachment != null ? !fileAttachment.equals(that.fileAttachment) : that.fileAttachment != null)
            return false;
        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        int result = evaluationQuestionId != null ? evaluationQuestionId.hashCode() : 0;
        result = 31 * result + (fileAttachmentId != null ? fileAttachmentId.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + position;
        result = 31 * result + (fileAttachment != null ? fileAttachment.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.evaluationQuestionId);
        dest.writeString(this.fileAttachmentId);
        dest.writeString(this.description);
        dest.writeInt(this.position);
//        dest.writeParcelable(this.fileAttachment, flags);
        dest.writeString(this.id);
    }

    public QuestionOption() {
    }

    protected QuestionOption(Parcel in) {
        this.evaluationQuestionId = in.readString();
        this.fileAttachmentId = in.readString();
        this.description = in.readString();
        this.position = in.readInt();
//        this.fileAttachment = in.readParcelable(Question.FileAttachment.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<QuestionOption> CREATOR = new Parcelable.Creator<QuestionOption>() {
        @Override
        public QuestionOption createFromParcel(Parcel source) {
            return new QuestionOption(source);
        }

        @Override
        public QuestionOption[] newArray(int size) {
            return new QuestionOption[size];
        }
    };
}
