package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Badge implements Parcelable {
    public String branchId;
    public String evaluationId;
    public User user;
    public Pillar pillar;
    public String id;

    @Override
    public String toString() {
        return "Badge{" +
                "branchId='" + branchId + '\'' +
                ", evaluationId='" + evaluationId + '\'' +
                ", user=" + user +
                ", pillar=" + pillar +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.evaluationId);
        dest.writeParcelable(this.user, flags);
        dest.writeParcelable(this.pillar, flags);
        dest.writeString(this.id);
    }

    public Badge() {
    }

    protected Badge(Parcel in) {
        this.branchId = in.readString();
        this.evaluationId = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.pillar = in.readParcelable(Pillar.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Badge> CREATOR = new Parcelable.Creator<Badge>() {
        @Override
        public Badge createFromParcel(Parcel source) {
            return new Badge(source);
        }

        @Override
        public Badge[] newArray(int size) {
            return new Badge[size];
        }
    };
}
