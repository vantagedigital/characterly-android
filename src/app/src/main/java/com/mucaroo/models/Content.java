package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Content implements Parcelable {
    public String branchId;
    public String language;
    public String countryId;
    public Integer status;
    public String title;
    public String description;
    public String contentText;
    public String footprint;
    public Integer contentType;
    public List<ContentTag> contentTags;
    public List<ContentPage> contentPages;
    public List<Domain> domains;
    public List<Pillar> pillars;
    public List<Subject> subjects;
    public List<Grade> grades;
    public List<FileAttachment> fileAttachments;
    public List<FileAttachment> attachments;
//    public List<Evaluation> evaluations;
    public String id;

    @Override
    public String toString() {
        return "Content{" +
                "branchId='" + branchId + '\'' +
                ", language='" + language + '\'' +
                ", countryId='" + countryId + '\'' +
                ", status=" + status +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", contentText='" + contentText + '\'' +
                ", footprint='" + footprint + '\'' +
                ", contentType=" + contentType +
                ", contentTags=" + contentTags +
                ", contentPages=" + contentPages +
                ", domains=" + domains +
                ", pillars=" + pillars +
                ", subjects=" + subjects +
                ", grades=" + grades +
                ", fileAttachments=" + fileAttachments +
                ", attachments=" + attachments +
                ", id='" + id + '\'' +
                '}';
    }


    public Content() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.language);
        dest.writeString(this.countryId);
        dest.writeValue(this.status);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.contentText);
        dest.writeString(this.footprint);
        dest.writeValue(this.contentType);
        dest.writeTypedList(this.contentTags);
        dest.writeTypedList(this.contentPages);
        dest.writeTypedList(this.domains);
        dest.writeTypedList(this.pillars);
        dest.writeTypedList(this.subjects);
        dest.writeTypedList(this.grades);
        dest.writeTypedList(this.fileAttachments);
        dest.writeTypedList(this.attachments);
        dest.writeString(this.id);
    }

    protected Content(Parcel in) {
        this.branchId = in.readString();
        this.language = in.readString();
        this.countryId = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.description = in.readString();
        this.contentText = in.readString();
        this.footprint = in.readString();
        this.contentType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.contentTags = in.createTypedArrayList(ContentTag.CREATOR);
        this.contentPages = in.createTypedArrayList(ContentPage.CREATOR);
        this.domains = in.createTypedArrayList(Domain.CREATOR);
        this.pillars = in.createTypedArrayList(Pillar.CREATOR);
        this.subjects = in.createTypedArrayList(Subject.CREATOR);
        this.grades = in.createTypedArrayList(Grade.CREATOR);
        this.fileAttachments = in.createTypedArrayList(FileAttachment.CREATOR);
        this.attachments = in.createTypedArrayList(FileAttachment.CREATOR);
        this.id = in.readString();
    }

    public static final Creator<Content> CREATOR = new Creator<Content>() {
        @Override
        public Content createFromParcel(Parcel source) {
            return new Content(source);
        }

        @Override
        public Content[] newArray(int size) {
            return new Content[size];
        }
    };
}
