package com.mucaroo.models;

import java.util.List;

public class Answer {
    public String evaluationQuestionId;
    public String answerText;
    public List<SelectedQuestionOption> selectedOptions;
}
