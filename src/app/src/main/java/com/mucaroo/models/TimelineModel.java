package com.mucaroo.models;

import java.util.List;

public class TimelineModel {

    public int totalCount;
    public List<TimelineComment> items;
}
