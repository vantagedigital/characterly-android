package com.mucaroo.models;

public class UserInfoModel {
    public Result result;

    @Override
    public String toString() {
        return "UserInfoModel{" +
                "result=" + result +
                '}';
    }

    public class Result {
        public User user;

        @Override
        public String toString() {
            return "Result{" +
                    "user=" + user +
                    '}';
        }
    }

    public class School {

    }

    public class Parent {
        public String branchId;
        public String userName;
        public String fullName;
        public String emailAddress;
        public String surname;
        public String name;
        public String id;
        public String creationTime;
        public boolean isActive;
        public int userId;

        @Override
        public String toString() {
            return "Parent{" +
                    "branchId='" + branchId + '\'' +
                    ", userName='" + userName + '\'' +
                    ", fullName='" + fullName + '\'' +
                    ", emailAddress='" + emailAddress + '\'' +
                    ", surname='" + surname + '\'' +
                    ", name='" + name + '\'' +
                    ", id='" + id + '\'' +
                    ", creationTime='" + creationTime + '\'' +
                    ", isActive=" + isActive +
                    ", userId=" + userId +
                    '}';
        }
    }
}
