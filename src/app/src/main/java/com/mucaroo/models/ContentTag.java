package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ContentTag implements Parcelable {
    public String contentId;
    public String name;
    public String id;

    @Override
    public String toString() {
        return "ContentTag{" +
                "contentId='" + contentId + '\'' +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contentId);
        dest.writeString(this.name);
        dest.writeString(this.id);
    }

    public ContentTag() {
    }

    protected ContentTag(Parcel in) {
        this.contentId = in.readString();
        this.name = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<ContentTag> CREATOR = new Parcelable.Creator<ContentTag>() {
        @Override
        public ContentTag createFromParcel(Parcel source) {
            return new ContentTag(source);
        }

        @Override
        public ContentTag[] newArray(int size) {
            return new ContentTag[size];
        }
    };
}
