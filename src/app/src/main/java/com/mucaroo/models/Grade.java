package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Grade implements Parcelable {
    public String branchId;
    public String name;
    public String description;
    public String id;

    @Override
    public String toString() {
        return "Grade{" +
                "branchId='" + branchId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.id);
    }

    public Grade() {
    }

    protected Grade(Parcel in) {
        this.branchId = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Grade> CREATOR = new Parcelable.Creator<Grade>() {
        @Override
        public Grade createFromParcel(Parcel source) {
            return new Grade(source);
        }

        @Override
        public Grade[] newArray(int size) {
            return new Grade[size];
        }
    };
}
