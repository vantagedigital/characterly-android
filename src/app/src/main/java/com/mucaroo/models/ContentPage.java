package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ContentPage implements Parcelable {
    public String contentText;
    public Integer pageNumber;
    public Integer contentType;
    public List<FileAttachment> attachmentIds;

    @Override
    public String toString() {
        return "ContentPage{" +
                "contentText='" + contentText + '\'' +
                ", pageNumber=" + pageNumber +
                ", contentType=" + contentType +
                ", attachmentIds=" + attachmentIds +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contentText);
        dest.writeValue(this.pageNumber);
        dest.writeValue(this.contentType);
        dest.writeTypedList(this.attachmentIds);
    }

    public ContentPage() {
    }

    protected ContentPage(Parcel in) {
        this.contentText = in.readString();
        this.pageNumber = (Integer) in.readValue(Integer.class.getClassLoader());
        this.contentType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.attachmentIds = in.createTypedArrayList(FileAttachment.CREATOR);
    }

    public static final Parcelable.Creator<ContentPage> CREATOR = new Parcelable.Creator<ContentPage>() {
        @Override
        public ContentPage createFromParcel(Parcel source) {
            return new ContentPage(source);
        }

        @Override
        public ContentPage[] newArray(int size) {
            return new ContentPage[size];
        }
    };


}
