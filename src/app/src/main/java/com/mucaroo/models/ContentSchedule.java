package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ContentSchedule implements Parcelable {
    public String branchId;
    public String contentId;
    public String language;
    public String countryId;
    public Integer status;
    public String title;
    public String description;
    public String startDate;
    public String endDate;
    public List<Classroom> classrooms;
    public Content content;
    public String id;

    @Override
    public String toString() {
        return "ContentSchedule{" +
                "branchId='" + branchId + '\'' +
                ", contentId='" + contentId + '\'' +
                ", language='" + language + '\'' +
                ", countryId='" + countryId + '\'' +
                ", status=" + status +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", classrooms=" + classrooms +
                ", content=" + content +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.contentId);
        dest.writeString(this.language);
        dest.writeString(this.countryId);
        dest.writeValue(this.status);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.startDate);
        dest.writeString(this.endDate);
        dest.writeList(this.classrooms);
        dest.writeParcelable(this.content, flags);
        dest.writeString(this.id);
    }

    public ContentSchedule() {
    }

    protected ContentSchedule(Parcel in) {
        this.branchId = in.readString();
        this.contentId = in.readString();
        this.language = in.readString();
        this.countryId = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.description = in.readString();
        this.startDate = in.readString();
        this.endDate = in.readString();
        this.classrooms = new ArrayList<Classroom>();
        in.readList(this.classrooms, Classroom.class.getClassLoader());
        this.content = in.readParcelable(Content.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<ContentSchedule> CREATOR = new Parcelable.Creator<ContentSchedule>() {
        @Override
        public ContentSchedule createFromParcel(Parcel source) {
            return new ContentSchedule(source);
        }

        @Override
        public ContentSchedule[] newArray(int size) {
            return new ContentSchedule[size];
        }
    };
}