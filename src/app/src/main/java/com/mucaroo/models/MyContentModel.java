package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MyContentModel implements Parcelable {

    public List<MyContentResult> result;

    @Override
    public String toString() {
        return "UserInfoModel{" +
                "result=" + result +
                '}';
    }

    public class Image {

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.result);
    }

    public MyContentModel() {
    }

    protected MyContentModel(Parcel in) {
        this.result = in.createTypedArrayList(MyContentResult.CREATOR);
    }

    public static final Parcelable.Creator<MyContentModel> CREATOR = new Parcelable.Creator<MyContentModel>() {
        @Override
        public MyContentModel createFromParcel(Parcel source) {
            return new MyContentModel(source);
        }

        @Override
        public MyContentModel[] newArray(int size) {
            return new MyContentModel[size];
        }
    };
}
