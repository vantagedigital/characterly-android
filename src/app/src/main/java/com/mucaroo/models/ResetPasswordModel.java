//
//	ResetPasswordModel.java
//
//	Create by ali khan on 5/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONEx

package com.mucaroo.models;

import org.json.*;
import java.util.*;


public class ResetPasswordModel{

	public boolean abp;
	public Object error;
	public result result;
	public boolean success;
	public Object targetUrl;
	public boolean unAuthorizedRequest;

	public class result {

		public ArrayList<errors> errors;
		public String message;
	}

	public class errors{
		public String code;
		public String description;
	}
}