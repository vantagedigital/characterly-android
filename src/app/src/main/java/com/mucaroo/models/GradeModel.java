package com.mucaroo.models;

import java.util.ArrayList;

/**
 * Created by ahsan on 10/15/2017.
 */

public class GradeModel {

    public Result result;

    public class Result {
        public int totalCount;
        public ArrayList<Items> items;

    }

    public class Items {
        public String branchId;
        public String name;
        public String description;
        public String id;
    }
}
