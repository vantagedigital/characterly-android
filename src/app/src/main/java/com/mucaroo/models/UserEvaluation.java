package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class UserEvaluation implements Parcelable {
    public String userId;
    public String evaluationId;
    public String id;
    public int status;
    public Evaluation evaluation;
    public List<Answer> answers;

    @Override
    public String toString() {
        return "UserEvaluation{" +
                "userId='" + userId + '\'' +
                ", evaluationId='" + evaluationId + '\'' +
                ", id='" + id + '\'' +
                ", status=" + status +
                ", evaluation=" + evaluation +
                ", answers=" + answers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEvaluation that = (UserEvaluation) o;

        if (status != that.status) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (evaluationId != null ? !evaluationId.equals(that.evaluationId) : that.evaluationId != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (evaluation != null ? !evaluation.equals(that.evaluation) : that.evaluation != null)
            return false;
        return answers != null ? answers.equals(that.answers) : that.answers == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (evaluationId != null ? evaluationId.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (evaluation != null ? evaluation.hashCode() : 0);
        result = 31 * result + (answers != null ? answers.hashCode() : 0);
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.evaluationId);
        dest.writeString(this.id);
        dest.writeInt(this.status);
        dest.writeParcelable(this.evaluation, flags);
        dest.writeList(this.answers);
    }

    public UserEvaluation() {
    }

    protected UserEvaluation(Parcel in) {
        this.userId = in.readString();
        this.evaluationId = in.readString();
        this.id = in.readString();
        this.status = in.readInt();
        this.evaluation = in.readParcelable(Evaluation.class.getClassLoader());
        this.answers = new ArrayList<Answer>();
        in.readList(this.answers, Answer.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserEvaluation> CREATOR = new Parcelable.Creator<UserEvaluation>() {
        @Override
        public UserEvaluation createFromParcel(Parcel source) {
            return new UserEvaluation(source);
        }

        @Override
        public UserEvaluation[] newArray(int size) {
            return new UserEvaluation[size];
        }
    };
}
