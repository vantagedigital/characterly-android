package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Timeline implements Parcelable {
    public String branchId;
    public String entityId;
    public Integer timelineType;
    public List<TimelineComment> comments;
    public String id;

    @Override
    public String toString() {
        return "TImeline{" +
                "branchId='" + branchId + '\'' +
                ", entityId='" + entityId + '\'' +
                ", timelineType=" + timelineType +
                ", comments=" + comments +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.entityId);
        dest.writeValue(this.timelineType);
        dest.writeTypedList(this.comments);
        dest.writeString(this.id);
    }

    public Timeline() {
    }

    protected Timeline(Parcel in) {
        this.branchId = in.readString();
        this.entityId = in.readString();
        this.timelineType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.comments = in.createTypedArrayList(TimelineComment.CREATOR);
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Timeline> CREATOR = new Parcelable.Creator<Timeline>() {
        @Override
        public Timeline createFromParcel(Parcel source) {
            return new Timeline(source);
        }

        @Override
        public Timeline[] newArray(int size) {
            return new Timeline[size];
        }
    };
}
