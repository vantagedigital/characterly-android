package com.mucaroo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Subject implements Parcelable {
    public String branchId;
    public String name;
    public String description;
    public String id;

    @Override
    public String toString() {
        return "Subject{" +
                "branchId='" + branchId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", id='" + id + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.branchId);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.id);
    }

    public Subject() {
    }

    protected Subject(Parcel in) {
        this.branchId = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Subject> CREATOR = new Parcelable.Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel source) {
            return new Subject(source);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };
}
