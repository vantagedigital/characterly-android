package com.mucaroo.models;

public class Notification {
    public String branchId;
    public Integer userId;
    public Integer type;
    public String typeId;
    public String title;
    public String message;
    public boolean isRead;
    public boolean isDeleted;
    public String creationTime;
    public Integer creatorUserId;
    public String id;

    @Override
    public String toString() {
        return "Notification{" +
                "branchId='" + branchId + '\'' +
                ", userId=" + userId +
                ", type=" + type +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", isRead=" + isRead +
                ", isDeleted=" + isDeleted +
                ", creationTime='" + creationTime + '\'' +
                ", creatorUserId=" + creatorUserId +
                ", id='" + id + '\'' +
                '}';
    }
}
